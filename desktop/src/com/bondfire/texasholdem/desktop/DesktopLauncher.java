package com.bondfire.texasholdem.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.bondfire.texasholdem.TexasHoldem;

import java.util.Calendar;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = TexasHoldem.WIDTH;
		config.height = TexasHoldem.HEIGHT;
		config.title = TexasHoldem.TITLE;

		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		new LwjglApplication(new TexasHoldem(hour * 60 * 60 + minute * 60 + second,null), config);
	}
}
