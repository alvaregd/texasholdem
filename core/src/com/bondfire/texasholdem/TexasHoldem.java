package com.bondfire.texasholdem;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.bondfire.app.bfUtils.BlurrableSpriteBatch;
import com.bondfire.app.callbacks.LibgdxLifeCycleCallback;
import com.bondfire.app.handler.Content;
import com.bondfire.app.services.TurnBasedMultiplayerService;
import com.bondfire.texasholdem.events.EventReceiver;
import com.bondfire.texasholdem.states.BackgroundState;
import com.bondfire.texasholdem.states.GSM;
import com.bondfire.texasholdem.states.TitleState;
import com.bondfire.texasholdem.test.NetworkStateTest;

public class TexasHoldem extends ApplicationAdapter {

    /** test*/
    public static NetworkStateTest tester;

    /** game info*/
    public static final String TITLE= "Texas Holdem";
    public static final int WIDTH  = 480;
    public static final int HEIGHT = 800;

    public static  int DEVICE_WIDTH;
    public static  int DEVICE_HEIGHT;

    /** class which we can use to get assets*/
    public static Content res;

    /** access to parent application type */
    public static Application.ApplicationType appType;

    /** Turn based game service ***/
    public static TurnBasedMultiplayerService mTurnBasedMultiplayerService;

    /** determine the amount of blur */
    public static float blurAmount; //For blurring our cards

    /** game state manager */
    private GSM gsm; //our gsm here

    /** so we can save the user's score */
    public static Preferences preferences;

    /** so we can change the background colour in real time*/
    public static int timeInSeconds;

    /** allows us to make network calls */
    private LibgdxLifeCycleCallback parentContainer;

    /** class to hold receive, hold events*/
    public static EventReceiver events;

    /*** Blurring objects **/
    private BlurrableSpriteBatch sbBlur;
    public static float MAX_BLUR = 3.2f;
    public static ShaderProgram blurShader;
    public static ShaderProgram circleShader;

    public TexasHoldem(int time, LibgdxLifeCycleCallback callback){
        timeInSeconds  = time;
        this.parentContainer = callback;
    }

    public void injectTurnBasedMultiplayerListener(TurnBasedMultiplayerService controller){
        mTurnBasedMultiplayerService = controller;
        if(controller == null){
            System.out.println("+================ Controller null");
        }

        if(events ==null){
            System.out.println("+================ Receiver Listener null");
        }

        try{
            controller.injectReceiver(events.getmReceiverListener());
        }catch (NullPointerException e) {
            System.out.println("Tried to fetch from null gsm, adding  to event bus");
        }
    }

    @Override
    public void create () {
        /** get our application type */
        appType = Gdx.app.getType();

        /** get device dimensions **/
        DEVICE_WIDTH = Gdx.graphics.getWidth();
        DEVICE_HEIGHT = Gdx.graphics.getHeight();

        /** set the clear color */
        Gdx.gl.glClearColor(0.2f,0.2f,0.2f,1);

        /** loead preferences */
        preferences = Gdx.app.getPreferences(TITLE);

        /** create our game state manager */
        gsm = new GSM();

        events = new EventReceiver(gsm);

        /** Load our assets */
        res = new Content();
        res.LoadAtlas("graphics/texasholdem.pack", "textures"); //our path to the file and the key
        res.LoadShaders("shaders/blurVertex.glsl", "shaders/blurFragment.glsl", "blur");
        res.LoadFont("open_sans.ttf");

        /** Load profile pictures */
        res.LoadProfilePictures(
                new String[]{
                        "http://lh5.googleusercontent.com/-K6KLPCMjjxw/AAAAAAAAAAI/AAAAAAAAABM/MiaIIUvEabg/s96-ns/",
                        "http://lh4.googleusercontent.com/-0wBUhn8Gy7c/AAAAAAAAAAI/AAAAAAAAABM/YeSh_8ILUvw/s96-ns/",
                        "http://lh5.googleusercontent.com/-8gSLDl3l_9A/AAAAAAAAAAI/AAAAAAAAABE/y5ZF_VIsB7M/s96-ns/",
                        "http://lh3.googleusercontent.com/-d1K6i1-f0zo/AAAAAAAAAAI/AAAAAAAAABM/JE32-Z_SuaI/s96-ns/",
                        "http://lh6.googleusercontent.com/-MTCLBITLbKM/AAAAAAAAAAI/AAAAAAAAABM/JBFnDzvk3PQ/s96-ns/",
                        "http://lh6.googleusercontent.com/-e5-VNShoC2o/AAAAAAAAAAI/AAAAAAAAABM/QQjEtDswLqA/s96-ns/",
                        "http://lh6.googleusercontent.com/-PJEPRbeaPe4/AAAAAAAAAAI/AAAAAAAAABE/o3o2RsoNLf8/s96-ns/",
                        "http://lh5.googleusercontent.com/-aMPI59mpfnk/AAAAAAAAAAI/AAAAAAAAABM/SZ-M57xjfIk/s96-ns/"
                },
                new String[]{
                        "http://lh5.googleusercontent.com/-K6KLPCMjjxw/AAAAAAAAAAI/AAAAAAAAABM/MiaIIUvEabg/s96-ns-s376/",
                        "http://lh4.googleusercontent.com/-0wBUhn8Gy7c/AAAAAAAAAAI/AAAAAAAAABM/YeSh_8ILUvw/s96-ns-s376/",
                        "http://lh5.googleusercontent.com/-8gSLDl3l_9A/AAAAAAAAAAI/AAAAAAAAABE/y5ZF_VIsB7M/s96-ns-s376/",
                        "http://lh3.googleusercontent.com/-d1K6i1-f0zo/AAAAAAAAAAI/AAAAAAAAABM/JE32-Z_SuaI/s96-ns-s376/",
                        "http://lh6.googleusercontent.com/-MTCLBITLbKM/AAAAAAAAAAI/AAAAAAAAABM/JBFnDzvk3PQ/s96-ns-s376/",
                        "http://lh6.googleusercontent.com/-e5-VNShoC2o/AAAAAAAAAAI/AAAAAAAAABM/QQjEtDswLqA/s96-ns-s376/",
                        "http://lh6.googleusercontent.com/-PJEPRbeaPe4/AAAAAAAAAAI/AAAAAAAAABE/o3o2RsoNLf8/s96-ns-s376/",
                        "http://lh5.googleusercontent.com/-aMPI59mpfnk/AAAAAAAAAAI/AAAAAAAAABM/SZ-M57xjfIk/s96-ns-s376/"
                }
        );

        /** load our blurrable sprite batch */
        sbBlur = new BlurrableSpriteBatch();

        /** prepare out blur blurShader */
        FileHandle[] blur = res.getShaders("blur");
        blurShader = new ShaderProgram(blur[0], blur[1]);
        if (!blurShader.isCompiled()) {
            Gdx.app.log("ShaderLessons", "Could not compile shaders: "+ blurShader.getLog());
            Gdx.app.exit();
        }
        sbBlur = new BlurrableSpriteBatch();
        sbBlur.setShader(blurShader);
        blurShader.begin();
        blurShader.setUniformf("bias", 0f);
        blurShader.end();

        /**Prepare the circle blurShader just in case */
        res.LoadShaders("shaders/circleVertex.glsl", "shaders/circleFragment.glsl", "circle");
        FileHandle[] circle = res.getShaders("circle");
        circleShader = new ShaderProgram(circle[0],circle[1]);
        if (!circleShader.isCompiled()) {
            Gdx.app.log("ShaderLessons", "Could not compile Circle shaders: " + circleShader.getLog());
            Gdx.app.exit();
        }
        ShaderProgram.pedantic = false;

        MAX_BLUR = (appType == Application.ApplicationType.Android) ? 3f:3.2f;

        if(parentContainer != null){
            parentContainer.onCreate();
        }

        tester = new NetworkStateTest(gsm);
        /*** push our states */
        gsm.push(new BackgroundState(gsm, timeInSeconds)); //load the background
        gsm.push(tester);
        gsm.push(new TitleState(gsm));
    }

    @Override
    public void render () {
	/*	if(appType == Application.ApplicationType.Desktop){
			blurAmount =  (Gdx.input.getX() / ((float)Gdx.graphics.getWidth()) );
			setBlur(blurAmount);
		}*/

        /** CLear the screen */
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gsm.update(Gdx.graphics.getDeltaTime());
        gsm.render(sbBlur);
    }

    public void setBlur(float blurPercent){
        blurAmount = blurPercent;
        if(this.sbBlur != null){
            this.sbBlur.setBlurAmount(blurAmount);
        }
    }

}