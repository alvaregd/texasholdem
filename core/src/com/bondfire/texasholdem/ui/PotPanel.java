package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;
import com.bondfire.texasholdem.data.PotPanelCoordinates;

/**
 * Created by alvaregd on 14/08/15.
 */

public class PotPanel extends Box{

    private BlurrableTextureAtlas atlas;
    private TextureRegion potIcon;

    private float y_bg_relative_device;
    private float x_bg_relative_device;
    private float width_bg_relative_device;
    private float height_bg_relative_device;

    private float y_icon;
    private float x_icon;
    private float width_icon;
    private float height_icon;

    private TextColumn orderColumn;
    private DashBoardColumn dbColumn;
    private TextColumn amountColumn;

    private boolean isShowing;

    public PotPanel(float x, float y, float width, float height){

        this.width  = TexasHoldem.WIDTH  * width;
        this.height = TexasHoldem.HEIGHT * height;
        this.x      = TexasHoldem.WIDTH  * x - this.width/2;
        this.y      = TexasHoldem.HEIGHT * y - this.height/2;

        y_bg_relative_device = this.y / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;
        x_bg_relative_device = this.x / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;
        width_bg_relative_device = this.width / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;
        height_bg_relative_device = this.height / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;

        width_icon =  this.width * PotPanelCoordinates.WIDTH_ICON_R_PANEL;
        height_icon = this.height* PotPanelCoordinates.HEIGHT_ICON_R_PANEL;
        x_icon = this.x + this.width * PotPanelCoordinates.X_ICON_R_PANEL - width_icon/2;
        y_icon = this.y + this.height * PotPanelCoordinates.Y_ICON_R_PANEL - height_icon/2;

        this.atlas = (BlurrableTextureAtlas) TexasHoldem.res.getAtlas("textures");
        potIcon = atlas.findRegion("pot");

        orderColumn = new TextColumn(
                this.x + this.width  * PotPanelCoordinates.X_ORDER_COLUMN_R_PANEL,
                this.y + this.height * PotPanelCoordinates.Y_ORDER_COLUMN_R_PANEL,
                this.width * PotPanelCoordinates.WIDTH_ORDER_COLUMN_R_PANEL,
                this.height * PotPanelCoordinates.HEIGHT_ORDER_COLUMN_R_PANEL
        );

        dbColumn = new DashBoardColumn(
                this.x + this.width  * PotPanelCoordinates.X_DASHBOARD_COLUMN_R_PANEL,
                this.y + this.height * PotPanelCoordinates.Y_DASHBOARD_COLUMN_R_PANEL,
                this.width * PotPanelCoordinates.WIDTH_DASHBOARD_COLUMN_R_PANEL,
                this.height * PotPanelCoordinates.HEIGHT_DASHBOARD_COLUMN_R_PANEL
        );

        amountColumn = new TextColumn(
                this.x + this.width  * PotPanelCoordinates.X_NUMBER_COLUMN_R_PANEL,
                this.y + this.height * PotPanelCoordinates.Y_NUMBER_COLUMN_R_PANEL,
                this.width * PotPanelCoordinates.WIDTH_NUMBER_COLUMN_R_PANEL,
                this.height * PotPanelCoordinates.HEIGHT_NUMBER_COLUMN_R_PANEL
        );
    }

    public void render(ShapeRenderer sr, SpriteBatch sb) {

        if(isShowing){
            sb.end();
            //Render the backgorund
            sr.begin(ShapeRenderer.ShapeType.Filled);
            sr.setColor(0.3f, 0.3f, 0.3f, 1f);
            sr.rect(x_bg_relative_device, y_bg_relative_device, width_bg_relative_device, height_bg_relative_device);
            sr.end();
            sb.begin();

            //draw the Icon
            sb.draw(atlas.tex,
                    x_icon,
                    y_icon,
                    this.width / 2,
                    this.height / 2,
                    width_icon,
                    height_icon,
                    1,
                    1,
                    0,// angle
                    potIcon.getRegionX(),
                    potIcon.getRegionY(),
                    potIcon.getRegionWidth(),
                    potIcon.getRegionHeight(),
                    false,
                    false);

            orderColumn.render(sb);
            amountColumn.render(sb);
            dbColumn.render(sb);

            //draw three columns
           /* sb.end();
            sr.begin();
            orderColumn.debugRender(sr);
            dbColumn.debugRender(sr);
            amountColumn.debugRender(sr);
            sr.end();
            sb.begin();*/
        }
    }

    public void setIsShowing(boolean show){
        this.isShowing = show;
    }

    public void setData(int[] cashpiles, int[][] potparticipants, int workingPotIndex ){

        orderColumn.clear();
        amountColumn.clear();
        dbColumn.clear();

        //Check the cashpiles, if they are -1
        for(int i = 0; i <= workingPotIndex; i ++){
            orderColumn.addTextRow("" + (i + 1));
            amountColumn.addTextRow(""+cashpiles[i]);
            dbColumn.addParticipantRow(potparticipants[i]);
        }
    }

    public void setMainPotParticipants(int[] participants){
        dbColumn.setMainPotParticipants(participants);
    }

}
