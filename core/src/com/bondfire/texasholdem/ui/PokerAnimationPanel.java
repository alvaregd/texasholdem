package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;
import static com.bondfire.texasholdem.data.AnimationPanelCoordinates.*;
import com.bondfire.texasholdem.data.DataPacket;
import com.bondfire.texasholdem.utils.CardUtils;

import java.util.ArrayList;



/**
 * Created by alvaregd on 28/07/15.
 * This class is used to display cards which are usually held by the player.
 * This class particularly works for poker.
 */
public class PokerAnimationPanel extends Box {

    private boolean isShown = false; //whether we are seeing the back or front of the card;
    private boolean isBig = false; //whether the cards are tiny or big
    private boolean isPlayerHandRevealing = true;
    private boolean isPlayerBig1 = false;
    private boolean isPlayerBig2 = false;
    private boolean isPlayerBig3 = false;
    private boolean isPlayerBig4 = false;
    private boolean isPlayerBig5 = false;
    private boolean isPlayerBig6 = false;
    private boolean isPlayerBig7 = false;
    private boolean isPlayerBig8 = false;

    private TextureRegion cardOne;
    private TextureRegion cardTwo;
    private TextureRegion back;

    private TextureRegion[][] playerCards;

    //final X coordinate of hand card one and two, when small
    private float x_card_hand_one_small_final;
    private float x_card_hand_two_small_final;
    private float y_card_hand_one_small_final;
    private float y_card_hand_two_small_final;

    //final y coordinates of the hand card one and two when big
    private float x_card_hand_one_big_final;
    private float x_card_hand_two_big_final;
    private float y_card_hand_one_big_final;
    private float y_card_hand_two_big_final;

    //final dimension of player cards while small;
    private float width_player_card_small_final;
    private float height_player_card_small_final;

    //final dimensions of hand card
    private float width_card_hand_small;
    private float height_card_hand_small;
    private float width_card_hand_big;
    private float height_card_hand_big;

    //current corrdinates of cards one and two
    private float x_card_one;
    private float x_card_two;
    private float y_card_one;
    private float y_card_two;

    //current dimension of the hand curds
    private float width_card_hand;
    private float height_card_hand;

    //deck coordinates and dimensions
    private float x_deck_final;  //final position of deck
    private float y_deck_final;  //final position of deck
    private float width_deck_final; //final width of  deck
    private float height_deck_final; //final height of deck

    //burn coordinates
    private float y_burn_final;
    private float x_burnOne_final; //final position of burn 1
    private float x_burnTwo_final; //final position of burn 2
    private float x_burnThree_final; //final position of burn 3

    //animating coordinates
    private float y_animating; //position of current moving card
    private float x_animating; //position of current moving card

    //final dimensions
    private float x_player_final;
    private float y_player_1_final;
    private float y_player_2_final;
    private float y_player_3_final;
    private float y_player_4_final;
    private float y_player_5_final;
    private float y_player_6_final;
    private float y_player_7_final;
    private float y_player_8_final;

    // PLAYER CARDS
    private float x_player_card_one_1;
    private float x_player_card_one_2;
    private float x_player_card_one_3;
    private float x_player_card_one_4;
    private float x_player_card_one_5;
    private float x_player_card_one_6;
    private float x_player_card_one_7;
    private float x_player_card_one_8;
    private float y_player_card_one_1;
    private float y_player_card_one_2;
    private float y_player_card_one_3;
    private float y_player_card_one_4;
    private float y_player_card_one_5;
    private float y_player_card_one_6;
    private float y_player_card_one_7;
    private float y_player_card_one_8;
    private float x_player_card_two_1;
    private float x_player_card_two_2;
    private float x_player_card_two_3;
    private float x_player_card_two_4;
    private float x_player_card_two_5;
    private float x_player_card_two_6;
    private float x_player_card_two_7;
    private float x_player_card_two_8;
    private float y_player_card_two_1;
    private float y_player_card_two_2;
    private float y_player_card_two_3;
    private float y_player_card_two_4;
    private float y_player_card_two_5;
    private float y_player_card_two_6;
    private float y_player_card_two_7;
    private float y_player_card_two_8;

    private float width_player_card_one_1;
    private float width_player_card_one_2;
    private float width_player_card_one_3;
    private float width_player_card_one_4;
    private float width_player_card_one_5;
    private float width_player_card_one_6;
    private float width_player_card_one_7;
    private float width_player_card_one_8;
    private float height_player_card_one_1;
    private float height_player_card_one_2;
    private float height_player_card_one_3;
    private float height_player_card_one_4;
    private float height_player_card_one_5;
    private float height_player_card_one_6;
    private float height_player_card_one_7;
    private float height_player_card_one_8;
    private float width_player_card_two_1;
    private float width_player_card_two_2;
    private float width_player_card_two_3;
    private float width_player_card_two_4;
    private float width_player_card_two_5;
    private float width_player_card_two_6;
    private float width_player_card_two_7;
    private float width_player_card_two_8;
    private float height_player_card_two_1;
    private float height_player_card_two_2;
    private float height_player_card_two_3;
    private float height_player_card_two_4;
    private float height_player_card_two_5;
    private float height_player_card_two_6;
    private float height_player_card_two_7;
    private float height_player_card_two_8;

    //player cards final
    private float x_player_card_1_small_final;
    private float x_player_card_2_small_final;
    private float y_player_card_1_small_final;
    private float y_player_card_2_small_final;
    private float y_player_card_3_small_final;
    private float y_player_card_4_small_final;
    private float y_player_card_5_small_final;
    private float y_player_card_6_small_final;
    private float y_player_card_7_small_final;
    private float y_player_card_8_small_final;

    //Hand transition timer
    private float transititionTime = 0;
    private final static float MAX_TRANSITION_TIME = 0.15f;
    private float transitionPercent =0f;

    //transition for player's cards
    private float t_player_card_transition_1;
    private float t_player_card_transition_2;
    private float t_player_card_transition_3;
    private float t_player_card_transition_4;
    private float t_player_card_transition_5;
    private float t_player_card_transition_6;
    private float t_player_card_transition_7;
    private float t_player_card_transition_8;

    private float transition_1_percent;
    private float transition_2_percent;
    private float transition_3_percent;
    private float transition_4_percent;
    private float transition_5_percent;
    private float transition_6_percent;
    private float transition_7_percent;
    private float transition_8_percent;

    //other transition timer
    private float transitionTimeDeck = 0;
    private final static float MAX_TRANSITION_TIME_DECK= 0.15f;
    private float transititionPercentDeck = 0f;

    private ArrayList<Integer> animations;
    private boolean isAnimating;
    private int animationType ;

    private int gameState;
    private boolean[] revealingPlayers;
    private int[][] cards;
    boolean isVisible = false;

    BlurrableTextureAtlas atlas;

    public PokerAnimationPanel(){

        //Panel height and width;
        this.width  = TexasHoldem.WIDTH  * WIDTH_R_PARENT;
        this.height = TexasHoldem.HEIGHT * HEIGHT_R_PARENT;
        this.x      = TexasHoldem.WIDTH  * X_R_PARENT_LEFT;
        this.y      = TexasHoldem.HEIGHT * Y_R_PARENT_BOTTOM;

        atlas  = (BlurrableTextureAtlas) TexasHoldem.res.getAtlas("textures");
        cardOne = atlas.findRegion("q_h");
        cardTwo = atlas.findRegion("k_h");
        back = atlas.findRegion("card_back_dark");

        animations = new ArrayList<Integer>();

        //measure new layout coordinates
        onMeasure();
    }

    public void onMeasure(){

        //calculate final coordindates of small hand cards
        x_card_hand_one_small_final =  width * X_R_PANEL_LEFT_SMALL_1;
        x_card_hand_two_small_final =  width * X_R_PANEL_LEFT_SMALL_2;
        y_card_hand_one_small_final = height * Y_R_PANEL_BOTTOM_SMALL_1;
        y_card_hand_two_small_final = height * Y_R_PANEL_BOTTOM_SMALL_2;

        //calculate final coordinates of big hand cards
         x_card_hand_one_big_final =width * X_R_PANEL_LEFT_BIG_1;
         x_card_hand_two_big_final =width * X_R_PANEL_LEFT_BIG_2;
         y_card_hand_one_big_final =height * Y_R_PANEL_BOTTOM_BIG_1;
         y_card_hand_two_big_final =height * Y_R_PANEL_BOTTOM_BIG_2;

        //calculate final dimensions of small and big sizes of hand cards;
        width_card_hand_small  =  width *WIDTH_R_PANEL_LEFT_SMALL;
        height_card_hand_small = height *HEIGHT_R_PANEL_BOTTOM_SMALL;
        width_card_hand_big    = width  * WIDTH_R_PANEL_LEFT_BIG;
        height_card_hand_big   = height *HEIGHT_R_PANEL_BOTTOM_BIG;

        //calculate the coordinates of the final burn cards
        y_burn_final      = height    * Y_BURN_R_PANEL_BOTTOM_DECK;
        x_burnOne_final   = width  * X_BURN_1_R_PANEL_LEFT;
        x_burnTwo_final   = width  * X_BURN_2_R_PANEL_LEFT;
        x_burnThree_final = width * X_BURN_3_R_PANEL_LEFT;

        //calculate the coordinates of the final deck card
        x_deck_final      = width  * X_R_PANEL_LEFT_DECK;
        y_deck_final      = height * Y_R_PANEL_BOTTOM_DECK;

        //calculate the dimensions of the deck card
        width_deck_final  = width  * WIDTH_DECK_R_PANEL;
        height_deck_final = height * HEIGHT_DECK_R_PANEL;


        //calcualte the coordinates of our players;
        x_player_final = width * X_PLAYER_R_PANEL_LEFT;
        y_player_1_final = height * Y_PLAYER_1_R_PANEL_BOTTOM;
        y_player_2_final = height * Y_PLAYER_2_R_PANEL_BOTTOM;
        y_player_3_final = height * Y_PLAYER_3_R_PANEL_BOTTOM;
        y_player_4_final = height * Y_PLAYER_4_R_PANEL_BOTTOM;
        y_player_5_final = height * Y_PLAYER_5_R_PANEL_BOTTOM;
        y_player_6_final = height * Y_PLAYER_6_R_PANEL_BOTTOM;
        y_player_7_final = height * Y_PLAYER_7_R_PANEL_BOTTOM;
        y_player_8_final = height * Y_PLAYER_8_R_PANEL_BOTTOM;

        //calculate coordinates our of player's cards;
        x_player_card_1_small_final = width *  X_PLAYERCARD_1_SMALL_R_PANEL;
        x_player_card_2_small_final = width *  X_PLAYERCARD_2_SMALL_R_PANEL;
        y_player_card_1_small_final = height * Y_PLAYER_1_R_PANEL_BOTTOM;
        y_player_card_2_small_final = height * Y_PLAYER_2_R_PANEL_BOTTOM;
        y_player_card_3_small_final = height * Y_PLAYER_3_R_PANEL_BOTTOM;
        y_player_card_4_small_final = height * Y_PLAYER_4_R_PANEL_BOTTOM;
        y_player_card_5_small_final = height * Y_PLAYER_5_R_PANEL_BOTTOM;
        y_player_card_6_small_final = height * Y_PLAYER_6_R_PANEL_BOTTOM;
        y_player_card_7_small_final = height * Y_PLAYER_7_R_PANEL_BOTTOM;
        y_player_card_8_small_final = height * Y_PLAYER_8_R_PANEL_BOTTOM;

        width_player_card_small_final = width *WIDTH_PLAYER_CARD_SMALL_R_PANEL_LEFT;
        height_player_card_small_final = height * HEIGHT_PLAYER_CARD_SMALL_R_PANEL_BOTTOM;

    }

    public void update(float dt) {

        if (isBig) transititionTime += dt;
        else transititionTime -= dt;
        transititionTime = MathUtils.clamp(transititionTime, 0, MAX_TRANSITION_TIME);
        transitionPercent = transititionTime / MAX_TRANSITION_TIME;

        x_card_one = Interpolation.linear.apply(x_card_hand_one_small_final, x_card_hand_one_big_final, transitionPercent);
        y_card_one = Interpolation.linear.apply(y_card_hand_one_small_final, y_card_hand_one_big_final, transitionPercent);

        x_card_two = Interpolation.linear.apply(x_card_hand_two_small_final, x_card_hand_two_big_final, transitionPercent);
        y_card_two = Interpolation.linear.apply(y_card_hand_two_small_final, y_card_hand_two_big_final, transitionPercent);

        height_card_hand = Interpolation.linear.apply(height_card_hand_small, height_card_hand_big, transitionPercent);

        //change the width to make it seem like the card is flipping
        if (transitionPercent < 0.5f) {
            width_card_hand = Interpolation.linear.apply(width_card_hand_small, 0, transitionPercent);
        } else {
            width_card_hand = Interpolation.linear.apply(0, width_card_hand_big, transitionPercent);
        }

        //Animate the moving cards
        if (isAnimating) {

            transitionTimeDeck += dt;
            transititionPercentDeck = transitionTimeDeck / MAX_TRANSITION_TIME_DECK;

            switch (this.animations.get(0)) {
                case ANIMATE_BURN_ONE:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_burn_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_burnOne_final, transititionPercentDeck);
                    break;
                case ANIMATE_BURN_TWO:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_burn_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_burnTwo_final, transititionPercentDeck);
                    break;
                case ANIMATE_BURN_THREE:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_burn_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_burnThree_final, transititionPercentDeck);
                    break;
                case ANIMATE_PLAYER_ONE:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_player_1_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_player_final, transititionPercentDeck);
                    break;
                case ANIMATE_PLAYER_TWO:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_player_2_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_player_final, transititionPercentDeck);
                    break;
                case ANIMATE_PLAYER_THREE:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_player_3_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_player_final, transititionPercentDeck);
                    break;
                case ANIMATE_PLAYER_FOUR:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_player_4_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_player_final, transititionPercentDeck);
                    break;
                case ANIMATE_PLAYER_FIVE:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_player_5_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_player_final, transititionPercentDeck);
                    break;

                case ANIMATE_PLAYER_SIX:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_player_6_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_player_final, transititionPercentDeck);
                    break;
                case ANIMATE_PLAYER_SEVEN:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_player_7_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_player_final, transititionPercentDeck);
                    break;
                case ANIMATE_PLAYER_EIGHT:
                    y_animating = Interpolation.linear.apply(y_deck_final, y_player_8_final, transititionPercentDeck);
                    x_animating = Interpolation.linear.apply(x_deck_final, x_player_final, transititionPercentDeck);
                    break;
            }

            if (transitionTimeDeck > MAX_TRANSITION_TIME_DECK) {
                transitionTimeDeck = 0;
                animations.remove(0);
                if (animations.size() == 0) {
                    isAnimating = false;
                }
            }
        }

        ////// PLAYERS's CARDS
        if(isPlayerHandRevealing){

            if (isPlayerBig1)t_player_card_transition_1 += dt;
            else t_player_card_transition_1 -= dt;
            t_player_card_transition_1 = MathUtils.clamp(t_player_card_transition_1, 0, MAX_TRANSITION_TIME);
            transition_1_percent = t_player_card_transition_1 / MAX_TRANSITION_TIME;

            if (isPlayerBig2) t_player_card_transition_2 += dt;
            else t_player_card_transition_2 -= dt;
            t_player_card_transition_2 = MathUtils.clamp(t_player_card_transition_2, 0, MAX_TRANSITION_TIME);
            transition_2_percent = t_player_card_transition_2 / MAX_TRANSITION_TIME;

            if (isPlayerBig3) t_player_card_transition_3 += dt;
            else t_player_card_transition_3 -= dt;
            t_player_card_transition_3 = MathUtils.clamp(t_player_card_transition_3, 0, MAX_TRANSITION_TIME);
            transition_3_percent = t_player_card_transition_3 / MAX_TRANSITION_TIME;

            if (isPlayerBig4) t_player_card_transition_4 += dt;
            else t_player_card_transition_4 -= dt;
            t_player_card_transition_4 = MathUtils.clamp(t_player_card_transition_4, 0, MAX_TRANSITION_TIME);
            transition_4_percent = t_player_card_transition_4 / MAX_TRANSITION_TIME;

            if (isPlayerBig5) t_player_card_transition_5 += dt;
            else t_player_card_transition_5 -= dt;
            t_player_card_transition_5 = MathUtils.clamp(t_player_card_transition_5, 0, MAX_TRANSITION_TIME);
            transition_5_percent = t_player_card_transition_5 / MAX_TRANSITION_TIME;

            if (isPlayerBig6) t_player_card_transition_6 += dt;
            else t_player_card_transition_6 -= dt;
            t_player_card_transition_6 = MathUtils.clamp(t_player_card_transition_6, 0, MAX_TRANSITION_TIME);
            transition_6_percent = t_player_card_transition_6 / MAX_TRANSITION_TIME;

            if (isPlayerBig7) t_player_card_transition_7 += dt;
            else t_player_card_transition_7 -= dt;
            t_player_card_transition_7 = MathUtils.clamp(t_player_card_transition_7, 0, MAX_TRANSITION_TIME);
            transition_7_percent = t_player_card_transition_7 / MAX_TRANSITION_TIME;

            if (isPlayerBig8) t_player_card_transition_8 += dt;
            else t_player_card_transition_8 -= dt;
            t_player_card_transition_8 = MathUtils.clamp(t_player_card_transition_8, 0, MAX_TRANSITION_TIME);
            transition_8_percent = t_player_card_transition_8 / MAX_TRANSITION_TIME;

        x_player_card_one_1 = Interpolation.linear.apply(x_player_card_1_small_final, x_card_hand_one_big_final, transition_1_percent);
        x_player_card_one_2 = Interpolation.linear.apply(x_player_card_1_small_final, x_card_hand_one_big_final, transition_2_percent);
        x_player_card_one_3 = Interpolation.linear.apply(x_player_card_1_small_final, x_card_hand_one_big_final, transition_3_percent);
        x_player_card_one_4 = Interpolation.linear.apply(x_player_card_1_small_final, x_card_hand_one_big_final, transition_4_percent);
        x_player_card_one_5 = Interpolation.linear.apply(x_player_card_1_small_final, x_card_hand_one_big_final, transition_5_percent);
        x_player_card_one_6 = Interpolation.linear.apply(x_player_card_1_small_final, x_card_hand_one_big_final, transition_6_percent);
        x_player_card_one_7 = Interpolation.linear.apply(x_player_card_1_small_final, x_card_hand_one_big_final, transition_7_percent);
        x_player_card_one_8 = Interpolation.linear.apply(x_player_card_1_small_final, x_card_hand_one_big_final, transition_8_percent);
        y_player_card_one_1 = Interpolation.linear.apply(y_player_card_1_small_final, y_card_hand_one_big_final, transition_1_percent);
        y_player_card_one_2 = Interpolation.linear.apply(y_player_card_2_small_final, y_card_hand_one_big_final, transition_2_percent);
        y_player_card_one_3 = Interpolation.linear.apply(y_player_card_3_small_final, y_card_hand_one_big_final, transition_3_percent);
        y_player_card_one_4 = Interpolation.linear.apply(y_player_card_4_small_final, y_card_hand_one_big_final, transition_4_percent);
        y_player_card_one_5 = Interpolation.linear.apply(y_player_card_5_small_final, y_card_hand_one_big_final, transition_5_percent);
        y_player_card_one_6 = Interpolation.linear.apply(y_player_card_6_small_final, y_card_hand_one_big_final, transition_6_percent);
        y_player_card_one_7 = Interpolation.linear.apply(y_player_card_7_small_final, y_card_hand_one_big_final, transition_7_percent);
        y_player_card_one_8 = Interpolation.linear.apply(y_player_card_8_small_final, y_card_hand_one_big_final, transition_8_percent);
        x_player_card_two_1 = Interpolation.linear.apply(x_player_card_2_small_final, x_card_hand_two_big_final, transition_1_percent);
        x_player_card_two_2 = Interpolation.linear.apply(x_player_card_2_small_final, x_card_hand_two_big_final, transition_2_percent);
        x_player_card_two_3 = Interpolation.linear.apply(x_player_card_2_small_final, x_card_hand_two_big_final, transition_3_percent);
        x_player_card_two_4 = Interpolation.linear.apply(x_player_card_2_small_final, x_card_hand_two_big_final, transition_4_percent);
        x_player_card_two_5 = Interpolation.linear.apply(x_player_card_2_small_final, x_card_hand_two_big_final, transition_5_percent);
        x_player_card_two_6 = Interpolation.linear.apply(x_player_card_2_small_final, x_card_hand_two_big_final, transition_6_percent);
        x_player_card_two_7 = Interpolation.linear.apply(x_player_card_2_small_final, x_card_hand_two_big_final, transition_7_percent);
        x_player_card_two_8 = Interpolation.linear.apply(x_player_card_2_small_final, x_card_hand_two_big_final, transition_8_percent);
        y_player_card_two_1 = Interpolation.linear.apply(y_player_card_1_small_final, y_card_hand_two_big_final, transition_1_percent);
        y_player_card_two_2 = Interpolation.linear.apply(y_player_card_2_small_final, y_card_hand_two_big_final, transition_2_percent);
        y_player_card_two_3 = Interpolation.linear.apply(y_player_card_3_small_final, y_card_hand_two_big_final, transition_3_percent);
        y_player_card_two_4 = Interpolation.linear.apply(y_player_card_4_small_final, y_card_hand_two_big_final, transition_4_percent);
        y_player_card_two_5 = Interpolation.linear.apply(y_player_card_5_small_final, y_card_hand_two_big_final, transition_5_percent);
        y_player_card_two_6 = Interpolation.linear.apply(y_player_card_6_small_final, y_card_hand_two_big_final, transition_6_percent);
        y_player_card_two_7 = Interpolation.linear.apply(y_player_card_7_small_final, y_card_hand_two_big_final, transition_7_percent);
        y_player_card_two_8 = Interpolation.linear.apply(y_player_card_8_small_final, y_card_hand_two_big_final, transition_8_percent);

        width_player_card_one_1 = Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_1_percent);
        width_player_card_one_2 = Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_2_percent);
        width_player_card_one_3 = Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_3_percent);
        width_player_card_one_4 = Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_4_percent);
        width_player_card_one_5 = Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_5_percent);
        width_player_card_one_6 = Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_6_percent);
        width_player_card_one_7 = Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_7_percent);
        width_player_card_one_8 = Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_8_percent);
        height_player_card_one_1 = Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_1_percent);
        height_player_card_one_2 = Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_2_percent);
        height_player_card_one_3 = Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_3_percent);
        height_player_card_one_4 = Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_4_percent);
        height_player_card_one_5 = Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_5_percent);
        height_player_card_one_6 = Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_6_percent);
        height_player_card_one_7 = Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_7_percent);
        height_player_card_one_8 = Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_8_percent);
        width_player_card_two_1 =Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_1_percent);
        width_player_card_two_2 =Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_2_percent);
        width_player_card_two_3 =Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_3_percent);
        width_player_card_two_4 =Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_4_percent);
        width_player_card_two_5 =Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_5_percent);
        width_player_card_two_6 =Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_6_percent);
        width_player_card_two_7 =Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_7_percent);
        width_player_card_two_8 =Interpolation.linear.apply(width_player_card_small_final, width_card_hand_big, transition_8_percent);
        height_player_card_two_1= Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_1_percent);
        height_player_card_two_2= Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_2_percent);
        height_player_card_two_3= Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_3_percent);
        height_player_card_two_4= Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_4_percent);
        height_player_card_two_5= Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_5_percent);
        height_player_card_two_6= Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_6_percent);
        height_player_card_two_7= Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_7_percent);
        height_player_card_two_8= Interpolation.linear.apply(height_player_card_small_final, height_card_hand_big, transition_8_percent);
        }
    }

    public void render(SpriteBatch sb) {

        if (isVisible) {


            if (!atlas.isBlurrable()) {
                System.out.println("Blurring from Hold BUtton");
                cardOne.getTexture().getTextureData().prepare();
                atlas.PrepareBlur(cardOne.getTexture().getTextureData().consumePixmap());
            }
            atlas.bind();


            //HAND first card
            sb.draw(atlas.tex,
                    x_card_one - width_card_hand / 2,
                    y_card_one - height_card_hand / 2,
                    width_card_hand / 2,
                    height_card_hand / 2,
                    width_card_hand,
                    height_card_hand,
                    1,
                    1,
                    +10f,// angle
                    transitionPercent > 0.5f ? cardOne.getRegionX() : back.getRegionX(),
                    transitionPercent > 0.5f ? cardOne.getRegionY() : back.getRegionY(),
                    transitionPercent > 0.5f ? cardOne.getRegionWidth() : back.getRegionWidth(),
                    transitionPercent > 0.5f ? cardOne.getRegionHeight() : back.getRegionHeight(),
                    false,
                    false);

            //HAND second card
            sb.draw(atlas.tex,
                    x_card_two - width_card_hand / 2,
                    y_card_two - height_card_hand / 2,
                    width_card_hand / 2,
                    height_card_hand / 2,
                    width_card_hand,
                    height_card_hand,
                    1,
                    1,
                    -10f,// angle
                    transitionPercent > 0.5f ? cardTwo.getRegionX() : back.getRegionX(),
                    transitionPercent > 0.5f ? cardTwo.getRegionY() : back.getRegionY(),
                    transitionPercent > 0.5f ? cardTwo.getRegionWidth() : back.getRegionWidth(),
                    transitionPercent > 0.5f ? cardTwo.getRegionHeight() : back.getRegionHeight(),
                    false,
                    false);

            //deck
            if(gameState != DataPacket.STATE_BLINDS){
                sb.draw(atlas.tex,
                        x_deck_final - width_deck_final / 2,
                        y_deck_final - height_deck_final / 2,
                        width_deck_final / 2,
                        height_deck_final / 2,
                        width_deck_final,
                        height_deck_final,
                        1,
                        1,
                        0,// angle
                        back.getRegionX(),
                        back.getRegionY(),
                        back.getRegionWidth(),
                        back.getRegionHeight(),
                        false,
                        false);
            }


            if(gameState >= DataPacket.STATE_FLOP){
                sb.draw(atlas.tex,
                        x_burnOne_final - width_deck_final / 2,
                        y_burn_final - height_deck_final / 2,
                        width_deck_final / 2,
                        height_deck_final / 2,
                        width_deck_final,
                        height_deck_final,
                        1,
                        1,
                        0,// angle
                        back.getRegionX(),
                        back.getRegionY(),
                        back.getRegionWidth(),
                        back.getRegionHeight(),
                        false,
                        false);
            }
            if(gameState >= DataPacket.STATE_TURN){
                sb.draw(atlas.tex,
                        x_burnTwo_final - width_deck_final / 2,
                        y_burn_final - height_deck_final / 2,
                        width_deck_final / 2,
                        height_deck_final / 2,
                        width_deck_final,
                        height_deck_final,
                        1,
                        1,
                        0,// angle
                        back.getRegionX(),
                        back.getRegionY(),
                        back.getRegionWidth(),
                        back.getRegionHeight(),
                        false,
                        false);
            }
            if(gameState >= DataPacket.STATE_RIVER){
                sb.draw(atlas.tex,
                        x_burnThree_final - width_deck_final / 2,
                        y_burn_final - height_deck_final / 2,
                        width_deck_final / 2,
                        height_deck_final / 2,
                        width_deck_final,
                        height_deck_final,
                        1,
                        1,
                        0,// angle
                        back.getRegionX(),
                        back.getRegionY(),
                        back.getRegionWidth(),
                        back.getRegionHeight(),
                        false,
                        false);
            }

              ///MOVING CARD
            if (isAnimating) {
                sb.draw(atlas.tex,
                        x_animating - width_deck_final / 2,
                        y_animating - height_deck_final / 2,
                        width_deck_final / 2,
                        height_deck_final / 2,
                        width_deck_final,
                        height_deck_final,
                        1,
                        1,
                        0,// angle
                        back.getRegionX(),
                        back.getRegionY(),
                        back.getRegionWidth(),
                        back.getRegionHeight(),
                        false,
                        false);
            }

            if (isPlayerHandRevealing) {

                for(int i = 0; i < revealingPlayers.length;i ++){
                    sb.draw(atlas.tex,
                            getPlayerCardX(i,1) - getPlayerCardWidth(i,1) / 2,
                            getPlayerCardY(i,1) - getPlayerCardHeight(i,1) / 2,
                            getPlayerCardWidth(i,1) / 2,
                            getPlayerCardHeight(i,1) / 2,
                            getPlayerCardWidth(i,1),
                            getPlayerCardHeight(i,1),
                            1,
                            1,
                            +10f,// angle
                            revealingPlayers[i] ? playerCards[i][0].getRegionX(): back.getRegionX(),
                            revealingPlayers[i] ? playerCards[i][0].getRegionY(): back.getRegionY(),
                            revealingPlayers[i] ? playerCards[i][0].getRegionWidth(): back.getRegionWidth(),
                            revealingPlayers[i] ? playerCards[i][0].getRegionHeight(): back.getRegionHeight(),
                            false,
                            false);
                    sb.draw(atlas.tex,
                            getPlayerCardX(i,2) - getPlayerCardWidth(i,2) / 2,
                            getPlayerCardY(i,2) - getPlayerCardHeight(i,2) / 2,
                            getPlayerCardWidth(i,2) / 2,
                            getPlayerCardHeight(i,2) / 2,
                            getPlayerCardWidth(i,2),
                            getPlayerCardHeight(i,2),
                            1,
                            1,
                            -10f,// angle
                            revealingPlayers[i] ? playerCards[i][1].getRegionX(): back.getRegionX(),
                            revealingPlayers[i] ? playerCards[i][1].getRegionY(): back.getRegionY(),
                            revealingPlayers[i] ? playerCards[i][1].getRegionWidth(): back.getRegionWidth(),
                            revealingPlayers[i] ? playerCards[i][1].getRegionHeight(): back.getRegionHeight(),
                            false,
                            false);
                }
            }
        }
    }


    public void distributeCards(){
        Animate(ANIMATE_PLAYER_ONE);
        Animate(ANIMATE_PLAYER_TWO);
        Animate(ANIMATE_PLAYER_THREE);
        Animate(ANIMATE_PLAYER_FOUR);
        Animate(ANIMATE_PLAYER_FIVE);
        Animate(ANIMATE_PLAYER_SIX);
        Animate(ANIMATE_PLAYER_SEVEN);
        Animate(ANIMATE_PLAYER_EIGHT);
        Animate(ANIMATE_PLAYER_ONE);
        Animate(ANIMATE_PLAYER_TWO);
        Animate(ANIMATE_PLAYER_THREE);
        Animate(ANIMATE_PLAYER_FOUR);
        Animate(ANIMATE_PLAYER_FIVE);
        Animate(ANIMATE_PLAYER_SIX);
        Animate(ANIMATE_PLAYER_SEVEN);
        Animate(ANIMATE_PLAYER_EIGHT);
    }

    public void Animate(int animationType){

        this.animations.add(animationType);
        this.isAnimating = true;
    }

    public void setIsMyHandBig(boolean isBig) {
        this.isBig = isBig;
    }

    public void setIsPlayerBig1(boolean isPlayerBig1) {
        this.isPlayerBig1 = isPlayerBig1;
    }

    public void setIsPlayerBig2(boolean isPlayerBig2) {
        this.isPlayerBig2 = isPlayerBig2;
    }

    public void setIsPlayerBig3(boolean isPlayerBig3) {
        this.isPlayerBig3 = isPlayerBig3;
    }

    public void setIsPlayerBig4(boolean isPlayerBig4) {
        this.isPlayerBig4 = isPlayerBig4;
    }

    public void setIsPlayerBig5(boolean isPlayerBig5) {
        this.isPlayerBig5 = isPlayerBig5;
    }

    public void setIsPlayerBig6(boolean isPlayerBig6) {
        this.isPlayerBig6 = isPlayerBig6;
    }

    public void setIsPlayerBig7(boolean isPlayerBig7) {
        this.isPlayerBig7 = isPlayerBig7;
    }

    public void setIsPlayerBig8(boolean isPlayerBig8) {
        this.isPlayerBig8 = isPlayerBig8;
    }

    public void setVisibility(boolean visibility){
        isVisible = visibility;
    }

    public void setPlayerHandRevealing(boolean revealHands){
        isPlayerHandRevealing = revealHands;
    }


    public void setPlayerData(boolean[] revealing, int [][] cards){
        this.revealingPlayers = revealing;
        if(this.cards == null || cards[0][0] != this.cards[0][0]){
            this.cards = cards;
            playerCards = new TextureRegion[revealing.length][2];

            for(int i =0;i<revealing.length; i++){
                playerCards[i][0] = atlas.findRegion(CardUtils.getCardString(cards[i][0]));
                playerCards[i][1] = atlas.findRegion(CardUtils.getCardString(cards[i][1]));
            }
        }
    }

    public void setState(int state ){
        this.gameState = state;
    }

    public void setMyCards(int one,int two){
        this.cardOne = atlas.findRegion(CardUtils.getCardString(one));
        this.cardTwo = atlas.findRegion(CardUtils.getCardString(two));
    }


    private float getPlayerCardX(int i, int card ){
        if(card == 1){
            switch(i){
                case 0: return x_player_card_one_1;
                case 1: return x_player_card_one_2;
                case 2: return x_player_card_one_3;
                case 3: return x_player_card_one_4;
                case 4: return x_player_card_one_5;
                case 5: return x_player_card_one_6;
                case 6: return x_player_card_one_7;
                case 7: return x_player_card_one_8;
            }
        }else if (card ==2){
            switch(i){
                case 0: return x_player_card_two_1;
                case 1: return x_player_card_two_2;
                case 2: return x_player_card_two_3;
                case 3: return x_player_card_two_4;
                case 4: return x_player_card_two_5;
                case 5: return x_player_card_two_6;
                case 6: return x_player_card_two_7;
                case 7: return x_player_card_two_8;
            }
        }
        return 0.0f;
    }

    private float getPlayerCardY(int i, int card){
        if(card == 1){
            switch(i){
                case 0: return y_player_card_one_1;
                case 1: return y_player_card_one_2;
                case 2: return y_player_card_one_3;
                case 3: return y_player_card_one_4;
                case 4: return y_player_card_one_5;
                case 5: return y_player_card_one_6;
                case 6: return y_player_card_one_7;
                case 7: return y_player_card_one_8;
            }
        }else if (card ==2){
            switch(i){
                case 0: return y_player_card_two_1;
                case 1: return y_player_card_two_2;
                case 2: return y_player_card_two_3;
                case 3: return y_player_card_two_4;
                case 4: return y_player_card_two_5;
                case 5: return y_player_card_two_6;
                case 6: return y_player_card_two_7;
                case 7: return y_player_card_two_8;
            }
        }
        return 0.0f;
    }
    private float getPlayerCardWidth(int i, int card){
        if(card == 1){
            switch(i){
                case 0: return width_player_card_one_1;
                case 1: return width_player_card_one_2;
                case 2: return width_player_card_one_3;
                case 3: return width_player_card_one_4;
                case 4: return width_player_card_one_5;
                case 5: return width_player_card_one_6;
                case 6: return width_player_card_one_7;
                case 7: return width_player_card_one_8;
            }
        }else if (card ==2){
            switch(i){
                case 0: return width_player_card_two_1;
                case 1: return width_player_card_two_2;
                case 2: return width_player_card_two_3;
                case 3: return width_player_card_two_4;
                case 4: return width_player_card_two_5;
                case 5: return width_player_card_two_6;
                case 6: return width_player_card_two_7;
                case 7: return width_player_card_two_8;
            }
        }
        return 0.0f;
    }
    private float getPlayerCardHeight(int i,int card){
        if(card == 1){
            switch(i){
                case 0: return height_player_card_one_1;
                case 1: return height_player_card_one_2;
                case 2: return height_player_card_one_3;
                case 3: return height_player_card_one_4;
                case 4: return height_player_card_one_5;
                case 5: return height_player_card_one_6;
                case 6: return height_player_card_one_7;
                case 7: return height_player_card_one_8;
            }
        }else if (card ==2){
            switch(i){
                case 0: return height_player_card_two_1;
                case 1: return height_player_card_two_2;
                case 2: return height_player_card_two_3;
                case 3: return height_player_card_two_4;
                case 4: return height_player_card_two_5;
                case 5: return height_player_card_two_6;
                case 6: return height_player_card_two_7;
                case 7: return height_player_card_two_8;
            }
        }
        return 0.0f;
    }







}
