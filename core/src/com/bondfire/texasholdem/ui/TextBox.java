package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 25/07/15.
 * A box to write text on
 */

public class TextBox extends Box {

    private String text;
    private BitmapFont bitmapFont;
    private GlyphLayout layout;
    private TextureRegion background;

    private final static float padding = 30f;

    private float backWidth;
    private float backHeight;

    private float textAlpha;

    public TextBox(BitmapFont bitmapFont, String text, float x, float y){

        setText(text);
        setCoordinates(x,y);
        this.bitmapFont = bitmapFont;
        layout = new GlyphLayout();
        layout.setText(bitmapFont,text);

        this.width = layout.width;
        this.height = layout.height;

        backWidth = width;
        backHeight = height;
    }

    public void render(SpriteBatch sb){
        textAlpha = TexasHoldem.blurAmount;
        bitmapFont.setColor(1.0f, 1.0f, 1.0f, 1 - (textAlpha));
        bitmapFont.draw(sb, text, x, y + backHeight);
    }

    public void setCoordinates(float x, float y){
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean contains(float x, float y) {

        boolean ret;
        this.width   = backWidth + 100;
        this.height = backHeight + 20;

        ret =  super.contains(x, y);

        this.width = layout.width;
        this.height = layout.height;

        return ret;
    }

    public void setText(String text){
        this.text = text;
    }
}