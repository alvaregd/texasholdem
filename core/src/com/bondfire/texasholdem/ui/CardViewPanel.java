package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 07/08/15.
 */
public class CardViewPanel extends Box{

    private BlurrableTextureAtlas atlas;
    private TextureRegion cardView;

    private boolean isShowing;

    public CardViewPanel(float x, float y, float width, float height){

        this.width  = TexasHoldem.WIDTH  * width;
        this.height = TexasHoldem.HEIGHT * height;
        this.x      = TexasHoldem.WIDTH  * x - this.width/2;
        this.y      = TexasHoldem.HEIGHT * y - this.height/2;

        this.atlas = (BlurrableTextureAtlas) TexasHoldem.res.getAtlas("textures");
        cardView = atlas.findRegion("card_back_dark");
    }

    public void render(SpriteBatch sb){
        if(isShowing){
            sb.draw(atlas.tex,
                    x,
                    y,
                    width / 2,
                    height / 2,
                    width,
                    height,
                    1,
                    1,
                    0,// angle
                    cardView.getRegionX(),
                    cardView.getRegionY(),
                    cardView.getRegionWidth(),
                    cardView.getRegionHeight(),
                    false,
                    false);
        }
    }

    public void isShowing(String pic, boolean show){
        isShowing = show;
        cardView = atlas.findRegion(pic);
    }

    public void setIsShowing(boolean show){
        this.isShowing = show;
    }
}
