package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bondfire.app.networkUtils.ImageDownloader;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 03/08/15.
 */
public class PlayerProfileInformation {

    private final static String Tag = PlayerProfileInformation.class.getName();
    private final static boolean d_main = false;

    /** Profile Pic */
    private String imageURL;
    private TextureRegion profileImage;
    private boolean isImageLoaded;

    /** Name **/
    public String name;
    /** wallet*/
    public int wallet;
    /** betting amount */
    public int bet;
    /** state*/
    public int state;
    /** rank **/
    public int rank;

    public boolean isRevealing;

    public int cardOne;
    public int cardTwo;

    public PlayerProfileInformation(String imageURL, int wallet, int bet, int playerState, int rank, int one, int two, boolean isRevealing){

        this.imageURL = imageURL;
        this.wallet = wallet;
        this.bet    = bet;
        this.state  = playerState;
        this.rank   = rank;
        this.cardOne = one;
        this.cardTwo = two;
        this.isRevealing = isRevealing;

        ImageDownloader.ImageDownloadCallBack callback = new ImageDownloader.ImageDownloadCallBack() {
            @Override
            public void TextureImageReceived(TextureRegion textureRegion) {
                profileImage = textureRegion;
                isImageLoaded = true;
            }
        };
        TexasHoldem.res.getLargeProfilePicture(imageURL, callback);
    }


    public TextureRegion getProfileImage() {
        return profileImage;
    }

    public boolean isImageLoaded() {
        return isImageLoaded;
    }


}
