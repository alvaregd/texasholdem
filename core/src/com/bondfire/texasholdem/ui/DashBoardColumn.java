package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by alvaregd on 16/08/15.
 */

public class DashBoardColumn  extends Column{

    public DashBoardColumn(float x, float y, float width, float height) {
        super(x, y, width, height);
//        addRow(new DashBoardRow(0f,0f,0f,0f));
//        addRow(new DashBoardRow(0f,0f,0f,0f));
//        addRow(new DashBoardRow(0f,0f,0f,0f));
//        addRow(new DashBoardRow(0f,0f,0f,0f));
//        addRow(new DashBoardRow(0f,0f,0f,0f));
    }

    @Override
    public void render(SpriteBatch sb) {
        super.render(sb);
    }


    @Override
    public void debugRender(ShapeRenderer sr) {
        super.debugRender(sr);
    }


    @Override
    public void addRow(Row row) {
        super.addRow(row);
        for(int i = 0; i < this.rows.size(); i++){
            ((DashBoardRow)rows.get(i)).applyCoordinates();
        }
    }

    public void setMainPotParticipants(int[] participants){
        if(rows.size() != 0){
            ((DashBoardRow)rows.get(rows.size()-1)).setParticipants(participants);
        }else{
//            System.out.println("New Entry on order column");
            addRow(new DashBoardRow(participants,0f,0f,0f,0f));
        }
    }

    public void addParticipantRow(int[] participants){
        addRow(new DashBoardRow(participants,0f,0f,0f,0f));
    }

    public void clear(){
        this.rows.clear();
    }


}
