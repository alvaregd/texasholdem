package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 28/07/15.
 */

public class InvisibleHoldButton extends Box {

    private boolean isHeldDown = false;
    BlurrableTextureAtlas atlas;

    public InvisibleHoldButton(float x, float y, float width, float height){

        this.width  = TexasHoldem.WIDTH  * width;
        this.height = TexasHoldem.WIDTH  * height;
        this.x      = TexasHoldem.WIDTH  * x  - this.width/2;
        this.y      = TexasHoldem.HEIGHT * y  - this.height/2;
    }

    public InvisibleHoldButton(float x, float y, float width, float height, boolean k){
        this.width  = width;
        this.height = height;
        this.x      = x;
        this.y      = y;
    }

    public void update(float dt){

    }

    public void render(ShapeRenderer sr){

//        sr.rect(158, 422, 95   , 118);

//        System.out.println("Render:" + this.x + " Y: " + this.y + " Width " +this.width + " height " + this.height);
        sr.circle(x,y, 10);
        sr.rect(x - this.width/2, y - this.height/2, width   , height);
//        sr.rect(x - width / 2, y - height / 2, width, height);
    }

    public void setIsHeldDown(boolean isHeldDown) {
        this.isHeldDown = isHeldDown;
    }

    public boolean isHeldDown() {
        return isHeldDown;
    }

}
