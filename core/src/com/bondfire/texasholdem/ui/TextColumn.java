package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 16/08/15.
 */
public class TextColumn extends Column {

    BitmapFont font;

    public TextColumn(float x, float y, float width, float height) {
        super(x, y, width, height);

        font = TexasHoldem.res.getBmpFont();
//        addRow(new TextRow(font, "1",0f,0f,0f,0f));
//        addRow(new TextRow(font, "2",0f,0f,0f,0f));
//        addRow(new TextRow(font, "3",0f,0f,0f,0f));
//        addRow(new TextRow(font, "4",0f,0f,0f,0f));
//        addRow(new TextRow(font, "5",0f,0f,0f,0f));
    }

    @Override
    public void addRow(Row row) {
        super.addRow(row);
        for(int i = 0; i < this.rows.size(); i++){
            ((TextRow)rows.get(i)).applyCoordinates();
        }
    }

    public void addPile(int amount ){

    }

    public void addTextRow(String text){
        addRow(new TextRow(font, text,0f,0f,0f,0f));
    }

    public void setMainText(String text){
        if(rows.size() != 0){
            ((TextRow)rows.get(rows.size()-1)).setText(text);
        }else{
            System.out.println("New Entry on order column");

        }
    }

    public void setSidePotText(int position, String text){
        if(rows.size() > 1){
            ((TextRow)rows.get(rows.size()-1)).setText(text);
        }else{
            System.out.println("New Entry on order column");
            addRow(new TextRow(font, text,0f,0f,0f,0f));
        }
    }

    public void clear(){
        this.rows.clear();
    }

}
