package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 30/07/15.
 */
public class TextButton extends Box {

    private final static boolean wireFrame = false;

    private String text;
    private BitmapFont bitmapFont;
    private GlyphLayout layout;
    private TextureRegion up;
    private TextureRegion down;

    BlurrableTextureAtlas atlas;

    private final static float padding = 30f;

    private boolean isHeldDown = false;

    private float textAlpha;

    private float graphicWidth = 0f;
    private float graphicHeight = 0f;

    ShapeRenderer sr;

    public TextButton(BlurrableTextureAtlas atlas, BitmapFont bitmapFont, String text, float x, float y){

        if(wireFrame) {
            sr = new ShapeRenderer();
            sr.setAutoShapeType(true);
        }
        this.atlas = atlas;
        this.bitmapFont = bitmapFont;

        up = atlas.findRegion("button_down");
        down = atlas.findRegion("button_up");

        this.text = text;

        this.x = x ;
        this.y = y ;

        calculateDimensions();
    }

    public void render(SpriteBatch sb){

        if(!atlas.isBlurrable()){
            System.out.println("Blurring from Turn Tracks");
            up.getTexture().getTextureData().prepare();
            atlas.PrepareBlur(up.getTexture().getTextureData().consumePixmap());
        }
        //Draw the background
        atlas.bind();

        sb.draw(atlas.tex,
                x - graphicWidth/2,
                y - graphicHeight/2,
                graphicWidth / 2,
                graphicHeight / 2,
                graphicWidth,
                graphicHeight,
                1,
                1,
                0,// angle
                isHeldDown ? down.getRegionX() : up.getRegionX(),
                isHeldDown ? down.getRegionY() : up.getRegionY(),
                isHeldDown ? down.getRegionWidth() : up.getRegionWidth(),
                isHeldDown ? down.getRegionHeight():up.getRegionHeight(),
                false,
                false);

        textAlpha = TexasHoldem.blurAmount;
        bitmapFont.setColor(0.0f, 0.0f, 0.0f, 1 - (textAlpha));
        bitmapFont.draw(sb, text, this.x - this.width/3, this.y + this.height/4);

        if(wireFrame){
            sb.end();

            sr.begin();
            sr.setColor(1, 1, 0, 1);
            sr.rect(this.x - this.width/2, this.y - this.height/2, width, height);
            sr.setColor(1, 0, 1, 1);
            sr.circle(this.x,this.y,2);
            sr.end();

            sb.begin();
        }
    }

    public void setHorizontalGraphicCoordinates(float x, float width) {
        this.graphicWidth = width;
    }

    public void setVerticalGraphicCoordinates(float y, float height) {
        this.y = y;
        this.graphicHeight = height;
    }

    public void setTextCoordinates(float x, float y){
        this.x= x;
        this.y= y;
    }

    public void setText(String text){
        this.text = text;
        calculateDimensions();
    }

    private void calculateDimensions(){
        layout = new GlyphLayout();
        layout.setText(bitmapFont,text);

        this.width = layout.width + padding;
        this.height = layout.height + padding;

        this.graphicWidth = this.width ;
        this.graphicHeight = this.height ;
    }
}
