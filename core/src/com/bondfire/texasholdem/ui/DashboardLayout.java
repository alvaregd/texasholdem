package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector3;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;
import com.bondfire.texasholdem.data.ProfilePanelCoordinates;
import com.bondfire.texasholdem.utils.CardUtils;

import java.util.ArrayList;

/*** Created by alvaregd on 30/07/15.
 * This will arrange the cards in a dashboard-like layout.*/

public class DashboardLayout extends Box {

    private final static String Tag = DashboardLayout.class.getName();
    private final static boolean d_handleInput = false;

    private final static float WIDTH_R_PARENT = 0.66f;
    private final static float HEIGHT_R_PARENT = 0.33f;

    private final static float X_R_PARENT_LEFT = 0.64f;
    private final static float Y_R_PARENT_BOTTOM = 0.68f;

    private final static float WIDTH_R_PARENT_CARD = 0.3f;
    private final static float HEIGHT_R_PARENT_CARD = 0.45f;

    BlurrableTextureAtlas atlas;

    private ArrayList<Card> cards;
    private ArrayList<InvisibleHoldButton> buttons;

    private float horizontal_space = 10f;
    private float vertical_space = 10f;

    private CardViewPanel cardPanel;
    private boolean isTouching = false;

    //Debug stuff
    private ShapeRenderer sr;

    boolean isVisible = false;

    public DashboardLayout(){

        sr          = new ShapeRenderer();
        cards       = new ArrayList<Card>();
        buttons     = new ArrayList<InvisibleHoldButton>();
        atlas       = (BlurrableTextureAtlas) TexasHoldem.res.getAtlas("textures");

        this.width  = TexasHoldem.WIDTH  * WIDTH_R_PARENT;
        this.height = TexasHoldem.HEIGHT * HEIGHT_R_PARENT;
        this.x      = TexasHoldem.WIDTH  * X_R_PARENT_LEFT - width/2;
        this.y      = TexasHoldem.HEIGHT * Y_R_PARENT_BOTTOM - height/2;

        cardPanel = new CardViewPanel(
                ProfilePanelCoordinates.X_PROFILE_PANEL_R_PARENT,
                ProfilePanelCoordinates.Y_PROFILE_PANEL_R_PARENT,
                ProfilePanelCoordinates.WIDTH_PROFILE_PANEL_R_PARENT,
                ProfilePanelCoordinates.HEIGHT_PROFILE_PANEL_R_PARENT
        );
    }

    public void update(float dt){

    }

    public void render(SpriteBatch sb){
        if(isVisible){
            for(Card c: cards){
                c.render(sb);
            }
        }
        cardPanel.render(sb);


        //debuging the button
      /*  sb.end();
        renderDebug();
        sb.begin();*/
    }

    public void renderDebug(){
        sr.setAutoShapeType(true);
        sr.begin();
//        sr.rect(x,y,width,height);

        for(int i = 0; i < buttons.size() ;i++){
            InvisibleHoldButton butt = buttons.get(i);
            butt.render(sr);
        }
        sr.end();
    }

    public void addView(String cardname){
        Card card = new Card(atlas,cardname,width *WIDTH_R_PARENT_CARD, height * HEIGHT_R_PARENT_CARD);
        cards.add(card);

        float w = 0;
        float h = 0;
        float rw = this.width;
        float rh = this.height;
        float h1 = 0;
        float w1 = 0;

        int numberOfChildren = cards.size();
        for(int i = 0; i < numberOfChildren; i++){

            Card view = cards.get(i);
            float vw = view.width;
            float vh = view.height;

            if(w1 + vw > rw){
                w = w > w1 ? w: w1;
                w1 = 0;
                h1 = h1 + vh + horizontal_space;
            }

            float w2 =0, h2 = 0;

            w2 = w1 + vw + horizontal_space;
            h2 = h1;

            h = (h > (h1 + vh)) ? h : (h1 + vh);

            view.x = this.x  +  w1 + horizontal_space;
            view.y = this.y  +  h1 + vertical_space;

            w1 = w2;
            h1 = h2;
        }
    }

    public void setButtonCoordinates(){
        buttons.clear();

        for(int i = 0; i < cards.size(); i++){
            //for each card, make a button with the cards's position and dimensions
            Card card = cards.get(i);
            InvisibleHoldButton button = new InvisibleHoldButton(
                    card.x + card.width/2,
                    card.y + card.height/2,
                    card.width,
                    card.height,
                    false
            );

//            System.out.println("Added button: X:" + card.x + " Y: " + card.y + " Width " +card.width + " height " + card.height);
            buttons.add(button);
        }
    }

    public void setVisibility(boolean visibility){
        this.isVisible= visibility;
    }

    public void setCards(int[] tableCards){
        cards.clear();
        if(tableCards[0] != -1)addView(CardUtils.getCardString(tableCards[0]));
        if(tableCards[1] != -1)addView(CardUtils.getCardString(tableCards[1]));
        if(tableCards[2] != -1)addView(CardUtils.getCardString(tableCards[2]));
        if(tableCards[3] != -1)addView(CardUtils.getCardString(tableCards[3]));
        if(tableCards[4] != -1)addView(CardUtils.getCardString(tableCards[4]));
        setButtonCoordinates();
    }

    /** returns true if this event is consumed */
    public boolean handleInput(Vector3 mouse){
            for(int i = 0; i < buttons.size() ;i++){
                InvisibleHoldButton butt = buttons.get(i);

                if (butt.contains(mouse.x, mouse.y)) {
                    butt.setIsHeldDown(true);
                    cardPanel.isShowing(cards.get(i).getCardName(), true);
                    if(d_handleInput)System.out.println("Returning true");
                    isTouching = true;
                    return  true;
                }
            }
            if(d_handleInput)System.out.println("Touched, ending false");
            return false;
    }

    public void setShowingfaslse(){
        for(int i = 0; i < cards.size() ;i++){
            InvisibleHoldButton butt = buttons.get(i);
            butt.setIsHeldDown(false);
        }
        cardPanel.setIsShowing(false);
        isTouching = false;
    }

    public boolean isTouching() {
        return isTouching;
    }
}
