package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;
import com.bondfire.texasholdem.data.DataPacket;

/**
 * Created by alvaregd on 30/07/15.
 */
public class ChoiceInterface extends Box {
    private final static String Tag = ChoiceInterface.class.getName();
    private final static boolean d_setMaxBetCount = true;

    private final static float WIDTH_R_PARENT = 0.66f;
    private final static float HEIGHT_R_PARENT = 0.21f;

    private final static float X_R_PARENT_LEFT = 0.71f;
    private final static float Y_R_PARENT_BOTTOM = 0.40f;

    private final static float Y_R_PANEL_CHOICE_1 = 0.07f;
    private final static float Y_R_PANEL_CHOICE_2 = 0.50f;
    private final static float Y_R_PANEL_CHOICE_3 = 0.92f;
 
    private final static float H_padding = 20f;
    private final static float V_padding = 10f;

    private final static float INCDEC_RADIUS_R_PARENT = 0.08f;
    private final static float X_INC_R_PANEL = 0.70f;
    private final static float X_DEC_R_PANEL = 0.85f;
    private final static float Y_INC_R_PANEL = 0.50f;
    private final static float Y_DEC_R_PANEL = 0.50f;

    private final static String RAISE = "Raise";
    private final static String CALL =  "Call";
    private final static String FOLD =  "Fold";
    private final static String BLIND = "Put Blinds";
    private final static String PASS = "Pass";

    private BitmapFont bitmapFont;

    public TextButton raiseButton;
    public TextButton callButton;
    public TextButton foldButton;

    private TextBox callAmount;
    private TextBox raiseAmount;

    public ImageButton increase;
    public ImageButton decrease;

    private final static int FLOP_MODE = 0;
    private final static int BLIND_MODE = 1;
    private final static int SHOW_MODE =2;

    private int mode =0;
    private boolean isVisible = true;
    private int  maxBetAmount;





    public int getRaiseCount() {
        return raiseCount;
    }

    public void ResetRaiseCount(){
        this.raiseCount = 0;
        raiseAmount.setText("" + raiseCount);
    }

    //GAME LOGIC
    private int raiseCount = 0;


    //Debug stuff
//    private ShapeRenderer sr;

    public ChoiceInterface(){

//        sr = new ShapeRenderer();

        bitmapFont = TexasHoldem.res.getBmpFont();

        BlurrableTextureAtlas atlas = (BlurrableTextureAtlas)TexasHoldem.res.getAtlas("textures");

        this.width = TexasHoldem.WIDTH * WIDTH_R_PARENT;
        this.height = TexasHoldem.HEIGHT * HEIGHT_R_PARENT;
        this.x = TexasHoldem.WIDTH * X_R_PARENT_LEFT - width/2;
        this.y = TexasHoldem.HEIGHT * Y_R_PARENT_BOTTOM - height/2;

        raiseButton = new TextButton(atlas,bitmapFont, RAISE,  x + H_padding, y + height  *Y_R_PANEL_CHOICE_2 );
        callButton  = new TextButton(atlas,bitmapFont,  CALL,  x + H_padding, y + height  *Y_R_PANEL_CHOICE_3 - V_padding);
        foldButton  = new TextButton(atlas,bitmapFont,  FOLD,  x + H_padding, y + height  *Y_R_PANEL_CHOICE_1 + V_padding);

        raiseButton.setHorizontalGraphicCoordinates(raiseButton.x,raiseButton.width);
        callButton.setHorizontalGraphicCoordinates(raiseButton.x,raiseButton.width);
        foldButton.setHorizontalGraphicCoordinates(raiseButton.x,raiseButton.width);


        //prepare the textboxes
        callAmount = new TextBox(bitmapFont, "0",x + raiseButton.width + 10, y + height  *(Y_R_PANEL_CHOICE_3 - 0.05f) - V_padding);
        raiseAmount = new TextBox(bitmapFont, "0",x + raiseButton.width +10, y + height  *(Y_R_PANEL_CHOICE_2 - 0.05f));

        //the other buttons
        increase = new ImageButton(
                "increase_up",
                "increase_down",
                x + width  * X_INC_R_PANEL,
                y + height * Y_INC_R_PANEL,
                INCDEC_RADIUS_R_PARENT* TexasHoldem.WIDTH,
                INCDEC_RADIUS_R_PARENT* TexasHoldem.WIDTH);
        decrease = new ImageButton(
                "decrease_up",
                "decrease_down",
                x + width  * X_DEC_R_PANEL,
                y + height * Y_DEC_R_PANEL,
                INCDEC_RADIUS_R_PARENT* TexasHoldem.WIDTH,
                INCDEC_RADIUS_R_PARENT* TexasHoldem.WIDTH);
    }

    public int handleInput(float x, float y){

        if (raiseButton.contains(x, y) && mode == FLOP_MODE) {
           return DataPacket.PLAYER_STATE_RAISED;
        }

        if (foldButton.contains(x, y) && (mode == FLOP_MODE || mode == BLIND_MODE) ) {
           return DataPacket.PLAYER_STATE_FOLDED;
        }
        if (callButton.contains(x, y)) {
            return DataPacket.PLAYER_STATE_CALLED;
        }

        if (increase.contains(x,y) && mode == FLOP_MODE) {
           incrementRaise();
        }

        if (decrease.contains(x,y ) && mode == FLOP_MODE) {
            decrementRaise();
        }
        return -1;
    }

    public void update(float dt){

    }

    public void render(SpriteBatch sb){
        if(isVisible){
            if(mode == BLIND_MODE){
                callButton.render(sb);
                foldButton.render(sb);
                callAmount.render(sb);
            }else if (mode == SHOW_MODE){
                callButton.render(sb);
            }else{
                raiseButton.render(sb);
                callButton.render(sb);
                foldButton.render(sb);

                callAmount.render(sb);
                raiseAmount.render(sb);

                increase.render(sb);
                decrease.render(sb);
            }
        }
    }

    public void renderDebug(){
      /*  sr.setAutoShapeType(true);
        sr.begin();
        sr.rect(this.x,this.y,this.width,this.height);
        sr.end();*/
    }

    public void setCallAmount(int amount){
        callAmount.setText("" + amount);
    }

    public void incrementRaise(){
        raiseCount += 100;
        raiseCount = MathUtils.clamp(raiseCount, 0, maxBetAmount);
        raiseAmount.setText("" + raiseCount);
    }
    public void decrementRaise(){
        raiseCount -= 100;
        raiseCount = MathUtils.clamp(raiseCount, 0, maxBetAmount);
        raiseAmount.setText(""+raiseCount);
    }

    public void setBlindsMode(){
        mode = BLIND_MODE;
        callButton.setText(BLIND);
        foldButton.setText(PASS);

        callAmount.setCoordinates(x + raiseButton.width + 10, y + height  *(Y_R_PANEL_CHOICE_3 - 0.05f) - V_padding);
        raiseAmount.setCoordinates(x + raiseButton.width +10, y + height  *(Y_R_PANEL_CHOICE_2 - 0.05f));

//        callButton.setHorizontalGraphicCoordinates(raiseButton.x, raiseButton.width + 75);
//        foldButton.setHorizontalGraphicCoordinates(raiseButton.x, raiseButton.width + 75);
//        foldButton.setVerticalGraphicCoordinates(y + height * Y_R_PANEL_CHOICE_1 + V_padding + 45, foldButton.height);
//        callAmount.setCoordinates(x + x_raise_amount, y + height * Y_R_PANEL_CHOICE_3 - V_padding);
    }

    public void setFlopMode() {
        mode = FLOP_MODE;
        callButton.setText(callText);
        foldButton.setText(FOLD);

        callAmount.setCoordinates(x + raiseButton.width + 10, y + height  *(Y_R_PANEL_CHOICE_3 - 0.05f) - V_padding);
        raiseAmount.setCoordinates(x + raiseButton.width +10, y + height  *(Y_R_PANEL_CHOICE_2 - 0.05f));
//        callButton.setHorizontalGraphicCoordinates(raiseButton.x, raiseButton.width);
//        foldButton.setHorizontalGraphicCoordinates(raiseButton.x, raiseButton.width);
//        foldButton.setVerticalGraphicCoordinates(y + height * Y_R_PANEL_CHOICE_1 + V_padding, foldButton.height);
//        callAmount.setCoordinates(x + x_raise_amount, y + height * Y_R_PANEL_CHOICE_3 - V_padding);
    }

    public void setShowMode(){
        mode = SHOW_MODE;
        callButton.setText("Show");
    }

    public void setVisibility(boolean visibility){
        isVisible = visibility;
    }

    String callText;
    public void setCheckedText(){
        this.callText = "Check";
    }
    public void setCallText(){
        this.callText = "Call";
    }

    public void setMaxBetAmount(int max){
        if(d_setMaxBetCount)System.out.println(Tag +" Max Bet count: " + max);
        maxBetAmount = max;
    }

}
