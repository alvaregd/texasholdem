package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by alvaregd on 16/08/15.
 */

public class Row extends Box {

    public Row(float x, float y, float width, float height) {

        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
    }

    public void render(SpriteBatch sb) {

    }

    public void debugRender(ShapeRenderer sr) {
        sr.setColor(Color.ORANGE);
        sr.circle(x, y, 10);
        sr.rect(x - this.width / 2, y - this.height / 2, width, height);
    }


}
