package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.bondfire.texasholdem.TexasHoldem;

import java.util.ArrayList;

/**
 * Created by alvaregd on 27/07/15.
 */
public class StatusIconBar extends Box{

    public final static float WIDTH_R_PARENT = 0.3f;
    public final static float HEIGHT_R_PARENT = 0.1f;
    public final static float X_R_PARENT_LEFT = 0.7f;
    public final static float Y_R_PARENT_BOTTOM = 0.85f;

    public final static int DEALER = 1;
    public final static int SMALLBLIND = 2;
    public final static int BIGBLIND = 3;

    private ArrayList<StatusIcon> icons;

    public StatusIconBar(){
        icons = new ArrayList<StatusIcon>();

        this.width = WIDTH_R_PARENT * TexasHoldem.WIDTH;
        this.height = HEIGHT_R_PARENT * TexasHoldem.HEIGHT;
        this.x = TexasHoldem.WIDTH*X_R_PARENT_LEFT;
        this.y = TexasHoldem.HEIGHT*Y_R_PARENT_BOTTOM;
    }

    public void empty(){
        icons.clear();
    }

    public void render(SpriteBatch sb){
        for(StatusIcon icon: icons){   icon.render(sb); }
    }

    public void setIcon(int icon){

        float x = this.x + width *WIDTH_R_PARENT* (icons.size());
        icons.add(new StatusIcon(icon, width, width,x,y));
    }
}
