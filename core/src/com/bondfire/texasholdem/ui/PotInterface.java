package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 30/07/15.
 */
public class PotInterface extends Box {

    private final static String Tag= PotInterface.class.getName();
    private final static boolean d_render = false;

    private final static float WIDTH_R_PARENT = 0.42f; //width of this panel
    private final static float HEIGHT_R_PARENT = 0.1f; //height of this panel
    private final static float X_R_PARENT_LEFT = 0.54f; //x pos of this panel
    private final static float Y_R_PARENT_BOTTOM = 0.21f; // y post of this panels

    private final static float POT_X_R_PANEL = 0.01f;
    private final static float POT_Y_R_PANEL = 0.55f;
    private final static float POT_WIDTH_R_PANEL = 0.2f;
    private final static float POT_HEIGHT_R_PANEL = 0.45f;

    private final static float AMOUNT_X_R_PANEL = 0.3f;
    private final static float AMOUNT_Y_R_PANEL = 0.60f;

    private final static float MINIPOT_X_R_PANEL = 0.01f;
    private final static float MINIPOT_Y_R_PANEL = 0.15f;

    private BitmapFont bitmapFont;

    private TextureRegion pot;
    private TextBox amount;
    private TextBox minipot;

    //Debug stuff
    private ShapeRenderer sr;

    private boolean hasSidePot = false;
    BlurrableTextureAtlas atlas;

    public PotInterface(){

        sr = new ShapeRenderer();
        bitmapFont = TexasHoldem.res.getBmpFont();

        if(bitmapFont == null)System.out.println("bitmap font is null");
        atlas = (BlurrableTextureAtlas)TexasHoldem.res.getAtlas("textures");

        this.width = TexasHoldem.WIDTH * WIDTH_R_PARENT;
        this.height = TexasHoldem.HEIGHT * HEIGHT_R_PARENT;
        this.x = TexasHoldem.WIDTH * X_R_PARENT_LEFT - width/2;
        this.y = TexasHoldem.HEIGHT * Y_R_PARENT_BOTTOM - height/2;

//        label = new TextBox(bitmapFont, "Pot:", x + width * POT_X_R_PANEL, y + height*POT_Y_R_PANEL);
        amount = new TextBox(bitmapFont, "1000", x + width * AMOUNT_X_R_PANEL, y + height*AMOUNT_Y_R_PANEL);
        pot = atlas.findRegion("pot");
        minipot = new TextBox(bitmapFont, "+ Side Pots", x + width * MINIPOT_X_R_PANEL, y + height*MINIPOT_Y_R_PANEL);
    }

    public void render(SpriteBatch sb){
        if(d_render)System.out.println(Tag +" render enter");
//        label.render(sb);
        if(!atlas.isBlurrable()){
            System.out.println("Blurring from Pot interface");
            pot.getTexture().getTextureData().prepare();
            atlas.PrepareBlur(pot.getTexture().getTextureData().consumePixmap());
        }
        //Draw the background
        atlas.bind();

        sb.draw(atlas.tex,
                x + width * POT_X_R_PANEL,
                y + height * POT_Y_R_PANEL,
                width / 2,
                height / 2,
                width * POT_WIDTH_R_PANEL,
                height * POT_HEIGHT_R_PANEL,
                1,
                1,
                0,// angle
                pot.getRegionX(),
                pot.getRegionY(),
                pot.getRegionWidth(),
                pot.getRegionHeight(),
                false,
                false);

        amount.render(sb);
        if(hasSidePot)minipot.render(sb);

        if(d_render)System.out.println(Tag +" render exit");
    }

    public void setSidePot(boolean sidePot){
        this.hasSidePot = sidePot;

    }

    public void setPotAmount(int pot){
        amount.setText(""+pot);
    }

    public void renderDebug(){
        sr.setAutoShapeType(true);
        sr.begin();
        sr.rect(this.x,this.y,this.width,this.height);
        sr.end();
    }

}
