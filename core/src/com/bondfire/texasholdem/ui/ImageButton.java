package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 30/07/15.
 */
public class ImageButton extends Box {

    private final static float WIDTH_R_PARENT = 0.3f;
    private final static float HEIGHT_R_PARENT = 0.3f;
    private final static float X_R_PARENT_LEFT = 0.9f;
    private final static float Y_R_PARENT_BUTTOM = 0.23f;

    private TextureRegion up;
    private TextureRegion down;

    private boolean isHeldDown = false;

    BlurrableTextureAtlas atlas;

    public ImageButton(String up_text, String down_text,float x, float y, float width,float height){

        atlas  = (BlurrableTextureAtlas) TexasHoldem.res.getAtlas("textures");

        up =atlas.findRegion(up_text);
        down =atlas.findRegion(down_text);

        this.x = x;
        this.y = y;
        this.width = width;
        this.height =height;
    }

    public void update(float dt){
    }

    public void render(SpriteBatch sb){
        if(!atlas.isBlurrable()){
            System.out.println("Blurring from Hold BUtton");
            up.getTexture().getTextureData().prepare();
            atlas.PrepareBlur(up.getTexture().getTextureData().consumePixmap());
        }
        //Draw the background
        atlas.bind();

        sb.draw(atlas.tex,
                x - width/2,
                y - height/2,
                width / 2,
                height / 2,
                width,
                height,
                1,
                1,
                0,// angle
                isHeldDown ? down.getRegionX():     up.getRegionX(),
                isHeldDown ? down.getRegionY():     up.getRegionY(),
                isHeldDown ? down.getRegionWidth(): up.getRegionWidth(),
                isHeldDown ? down.getRegionHeight():up.getRegionHeight(),
                false,
                false);
    }


    public void setIsHeldDown(boolean isHeldDown) {
        this.isHeldDown = isHeldDown;
    }

    public boolean isHeldDown() {
        return isHeldDown;
    }

}
