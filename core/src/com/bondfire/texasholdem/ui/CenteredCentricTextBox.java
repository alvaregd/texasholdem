package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 03/08/15.
 */
public class CenteredCentricTextBox extends  Box {

    private String text;
    private BitmapFont bitmapFont;
    private GlyphLayout layout;

    private float textAlpha;

    public CenteredCentricTextBox(BitmapFont bitmapFont, String text, float x, float y){

        this.text = text;

        this.bitmapFont = bitmapFont;
        layout = new GlyphLayout();
        layout.setText(bitmapFont,text);

        this.width = layout.width;
        this.height = layout.height;

        this.x = x - width/2;
        this.y = y - height/2;
    }

    public void render(SpriteBatch sb){

        textAlpha = TexasHoldem.blurAmount;
        bitmapFont.setColor(1.0f, 1.0f, 1.0f, 1 - (textAlpha));
        bitmapFont.draw(sb, text, x, y);

    }

    @Override
    public boolean contains(float x, float y) {

        boolean ret;
        ret =  super.contains(x, y);

        this.width = layout.width;
        this.height = layout.height;

        return ret;
    }

    public void setText(String text){
        this.text = text;
    }
}
