package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.bondfire.texasholdem.TexasHoldem;

import java.util.ArrayList;

/**
 * Created by alvaregd on 16/08/15.
 */

public class DashBoardRow extends Row {

    private ArrayList<MiniIcons> icons;
    private float horizontal_space = 20f;
    private float vertical_space   = 20f;
    private float left_padding     = 20f;

    public DashBoardRow(float x, float y, float width, float height) {
        super(x, y, width, height);

        icons = new ArrayList<MiniIcons>();
//        addIcon(new FakeIcons(1,0f,0f,15f));
//        addIcon(new FakeIcons(1,0f,0f,15f));
//        addIcon(new FakeIcons(1,0f,0f,15f));
//        addIcon(new FakeIcons(0f,0f,15f));
//        addIcon(new FakeIcons(0f,0f,15f));
//        addIcon(new FakeIcons(0f,0f,15f));
//        addIcon(new FakeIcons(0f,0f,15f));
//        addIcon(new FakeIcons(0f,0f,15f));
    }

    public DashBoardRow(int[] particippants,float x, float y, float width, float height) {
        super(x, y, width, height);
        icons = new ArrayList<MiniIcons>();
        setParticipants(particippants);
    }

    @Override
    public void render(SpriteBatch sb){

        for(int i =0; i < icons.size();i++){
            icons.get(i).render(sb);
        }
    }

    @Override
    public void debugRender(ShapeRenderer sr) {
        super.debugRender(sr);

        for(int i =0; i < icons.size();i++){
            icons.get(i).debugRender(sr);
        }
    }

    public void applyCoordinates(){
        refreshCoordinates();
    }

    public void refreshCoordinates(){
        float w = 0;
        float h = 0;
        float rw = this.width;
        float rh = this.height;
        float h1 = 0;
        float w1 = 0;

        int numberOfChildren = icons.size();
        for(int i = 0; i < numberOfChildren; i++){

            MiniIcons view = icons.get(i);
            float vw = view.width;
            float vh = view.height;

            if(w1 + vw > rw){
                w = w > w1 ? w: w1;
                w1 = 0;
                h1 = h1 + vh + horizontal_space;
            }

            float w2 =0, h2 = 0;

            w2 = w1 + vw+ horizontal_space;
            h2 = h1;

            h = (h > (h1 + vh)) ? h : (h1 + vh);

            view.x = this.x - this.width /2 + left_padding +  w1;// + horizontal_space;
            view.y = this.y - h/2 +  h1 ;//+ vertical_space;

            w1 = w2;
            h1 = h2;

            view.applyCoordinates();
        }
    }

    public void addIcon(MiniIcons icon){
        icons.add(icon);
    }

    public void setParticipants(int[] participants){
        icons.clear();
        for(int player:participants){

            if(player != 0){
                addIcon(new MiniIcons(player,0f,0f,30f));
            }
        }
        refreshCoordinates();
    }

}
