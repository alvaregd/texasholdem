package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;


public class Card extends Box {

    private TextureRegion frontRegion;
    private TextureRegion backRegion;
    private BlurrableTextureAtlas atlas;


    private String cardName;

    private boolean isRevealed = true;

    public Card(BlurrableTextureAtlas atlas, String card, float width, float height){
        this.atlas = atlas;
        frontRegion = atlas.findRegion(card);

        cardName = card;

        this.width = width;
        this.height = height;
    }

    public void render(SpriteBatch sb){

        if(!atlas.isBlurrable()){
            System.out.println("Blurring from Turn Tracks");
            frontRegion.getTexture().getTextureData().prepare();
            atlas.PrepareBlur(frontRegion.getTexture().getTextureData().consumePixmap());
        }
        //Draw the background
        atlas.bind();

        sb.draw(atlas.tex,
                x ,
                y ,
                width / 2,
                height / 2,
                width,
                height,
                1,
                1,
                0,// angle
                isRevealed ? frontRegion.getRegionX():backRegion.getRegionX(),
                isRevealed ? frontRegion.getRegionY():backRegion.getRegionY(),
                isRevealed ? frontRegion.getRegionWidth():backRegion.getRegionWidth(),
                isRevealed ? frontRegion.getRegionHeight():backRegion.getRegionHeight(),
                false,
                false);
    }

    public String getCardName() {
        return cardName;
    }

    public void reveal(){
        this.isRevealed = true;
    }

    public void hide(){
        this.isRevealed = false;
    }
}
