package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.bondfire.app.networkUtils.ImageDownloader;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 16/08/15.
 */
public class MiniIcons extends Box {

    private  float radius;
    private int player;
    private TextureRegion pic = null;

    private float x_pic_relative_device;
    private float y_pic_relative_device;
    private float width_pic_relative_device;
    private float height_pic_relative_device;


    public MiniIcons(int player, float x, float y, float radius){
        this.player = player;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.width  = radius;
        this.height = radius;

        TexasHoldem.res.getSmallProfilePicture(
                TexasHoldem.res.getSmallIconUrl(player),
                new ImageDownloader.ImageDownloadCallBack() {
                    @Override
                    public void TextureImageReceived(TextureRegion textureRegion) {
                        pic = textureRegion;
                    }
                }
        );
    }

    public void applyCoordinates(){
        x_pic_relative_device = (this.x - width/2) / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;
        y_pic_relative_device = (this.y - height/2) / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;
        width_pic_relative_device = this.width / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;
        height_pic_relative_device = this.height / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;
    }

    public void debugRender(ShapeRenderer sr){
        sr.setColor(Color.YELLOW);
        sr.circle(x,y,radius);
    }

    public void render(SpriteBatch sb){

        sb.end();
        sb.setShader(TexasHoldem.circleShader);
        TexasHoldem.circleShader.begin();
        TexasHoldem.circleShader.setUniformf("u_position", x_pic_relative_device, y_pic_relative_device);
        TexasHoldem.circleShader.setUniformf("u_resolution", width_pic_relative_device, height_pic_relative_device);
        TexasHoldem.circleShader.end();
        sb.begin();
        if(pic != null){
            sb.draw(pic, x - width/2, y - height/2, width, height);
        }
        sb.setShader(TexasHoldem.blurShader);
    }

}
