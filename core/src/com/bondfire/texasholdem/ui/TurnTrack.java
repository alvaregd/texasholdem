package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.app.javaUtils.IndexedHashMap;
import com.bondfire.texasholdem.TexasHoldem;

import java.util.HashMap;
import java.util.Iterator;

public class TurnTrack extends Box {

    private final static String Tag = TurnTrack.class.getName();
    private final static boolean d_setMoney = false;

    private final static float MAX_WIDTH_R = 0.25f;
    private final static float MAX_HEIGHT_R = 0.55f;
    private final static float RELATIVE_Y_POS_R = 0.14f;
    private final static float RELATIVE_X_POS_R = 0.02f;

    private final static float INDICATOR_WIDTH_R = 0.1f;

    private final static float MAX_ROW_HEIGHT_RATIO = 0.09f;
    private final static float X_INDICATOR_R_PANEL = 0.4f;

    private static float x_indicator;

    /**Background **/
    private TextureRegion background;

    /**turn indicator */
    private TextureRegion indicator;

    /**Row entry*/
    private IndexedHashMap<String,TurnTrackEntry> players;
    private static HashMap<String,TurnTrackEntry> iteratingMap;
    private static Iterator iterator;
    private static java.util.Map.Entry pair;

    /** atlas */
    BlurrableTextureAtlas atlas;

    /** Logic **/
    private int turnIndex = 0;

    public TurnTrack() {

        this.x = TexasHoldem.WIDTH * RELATIVE_X_POS_R;
        this.y = TexasHoldem.HEIGHT * RELATIVE_Y_POS_R;
        this.height = TexasHoldem.HEIGHT * MAX_HEIGHT_R;
        this.width = TexasHoldem.WIDTH * MAX_WIDTH_R;

        x_indicator = x + width *X_INDICATOR_R_PANEL;

        players = new IndexedHashMap<String, TurnTrackEntry>();
        atlas = (BlurrableTextureAtlas) TexasHoldem.res.getAtlas("textures");
        background = atlas.findRegion("grey_background");
        indicator = atlas.findRegion("indicator");
    }

    public void render(SpriteBatch sb){
        if(!atlas.isBlurrable()){
            System.out.println("Blurring from Turn Tracks");
            background.getTexture().getTextureData().prepare();
            atlas.PrepareBlur(background.getTexture().getTextureData().consumePixmap());
        }

        //Draw the background
        atlas.bind();

        //Draw the players
        renderPlayers(players, sb);

        //Draw the indicator
        sb.draw(atlas.tex,
                x_indicator,
                MAX_ROW_HEIGHT_RATIO * TexasHoldem.HEIGHT + this.y + this.height - MAX_ROW_HEIGHT_RATIO * TexasHoldem.HEIGHT * (turnIndex),
                TexasHoldem.WIDTH / 2,
                TexasHoldem.HEIGHT / 2,
                TexasHoldem.WIDTH * INDICATOR_WIDTH_R,
                TexasHoldem.WIDTH * INDICATOR_WIDTH_R, //its a square image
                1,
                1,
                0,// angle
                indicator.getRegionX(),
                indicator.getRegionY(),
                indicator.getRegionWidth(),
                indicator.getRegionHeight(),
                false,
                false);
    }

    public void update(float dt){

    }

    public void addPlayerToTrack(String player, TurnTrackEntry entry){
        float rowY;
        //Find the positions of this new entry given the player count
        rowY = MAX_ROW_HEIGHT_RATIO * TexasHoldem.HEIGHT +  this.y + this.height -  MAX_ROW_HEIGHT_RATIO * TexasHoldem.HEIGHT * players.size();
        entry.setDimensions(
                atlas,
                x,
                rowY,
                width,
                MAX_ROW_HEIGHT_RATIO * TexasHoldem.HEIGHT );
        ((IndexedHashMap)players).put(player, entry);
    }

    public void setTurn(String turn){
        //get the index of the person's id
        turnIndex = players.getKeyIndex(turn);
    }

    public void setDealer(String dealer){
        //Clear the list
        for(int i = 1; i <= players.size(); i++){
            players.get("p_" + i).setIsDealer(false);
        }
        //set the new dealer
        players.get(dealer).setIsDealer(true);
    }

    public void setSmallBlind(String player){
        try{
            //Clear the list
            for(int i = 1; i <= players.size(); i++){
                players.get("p_" + i).setIsSmallBlind(false);
            }
            //set the new dealer
            players.get(player).setIsSmallBlind(true);
        }catch (NullPointerException e){
            System.out.println("Something is null");
        }
    }
    public void setBigBlind(String player){
        try{
            //Clear the list
            for(int i = 1; i <= players.size(); i++){
                players.get("p_" + i).setIsBigBlind(false);
            }
            //set the new dealer
            players.get(player).setIsBigBlind(true);
        }catch (NullPointerException e){
            System.out.println("Something is null");
        }
    }

    //Here we seperate the rendering of profile pic with the the rest of the UI
    //because profile pic uses a different blurShader
    public void renderPlayers(HashMap mp, SpriteBatch sb) {

        sb.end();
        iteratingMap = new HashMap<String, TurnTrackEntry>(mp);
        iterator = iteratingMap.entrySet().iterator();
        while (iterator.hasNext()) {
            pair = (java.util.Map.Entry)iterator.next();
            ((TurnTrackEntry)pair.getValue()).renderProfile(sb,TexasHoldem.circleShader);
            iterator.remove();
        }
        sb.begin();

        sb.setShader(TexasHoldem.blurShader);
        iteratingMap = new HashMap<String, TurnTrackEntry>(mp);
        iterator = iteratingMap.entrySet().iterator();
        while (iterator.hasNext()) {
            pair = (java.util.Map.Entry)iterator.next();
            ((TurnTrackEntry)pair.getValue()).render(sb);
            iterator.remove();
        }
    }

    public void clearPlayers(){
        players.clear();
    }

    public void setMoney(String player, int walletSize){
        if(d_setMoney)System.out.println(Tag + " setMoney()" + "player: "+ player + "  walletSize:" + walletSize);
        players.get(player).setScore(""+walletSize);
    }

    public TurnTrackEntry getPlayerEntry(String player){
        return players.get(player);
    }
}
