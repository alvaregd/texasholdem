package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Created by alvaregd on 16/08/15.
 */
public class TextRow extends Row {

    CenteredCentricTextBox rowEntryText;
    public TextRow(BitmapFont font, String text, float x, float y, float width, float height) {
        super(x, y, width, height);

        rowEntryText = new CenteredCentricTextBox(
              font,
              text,
              x,
              y
        );
    }

    public void applyCoordinates(){

        rowEntryText.x = x - rowEntryText.width/2;
        rowEntryText.y = y + rowEntryText.height/2;
    }

    public void setText(String text){
        rowEntryText.setText(text);
    }

    @Override
    public void render(SpriteBatch sb) {
        super.render(sb);
        rowEntryText.render(sb);
    }

    @Override
    public void debugRender(ShapeRenderer sr) {
        super.debugRender(sr);



    }
}
