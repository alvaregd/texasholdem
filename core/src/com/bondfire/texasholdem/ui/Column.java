package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.ArrayList;

/**
 * Created by alvaregd on 16/08/15.
 */
public class Column extends Box {

    protected ArrayList<Row> rows;

    public Column(float x, float y, float width, float height){

        this.width = width;
        this.height = height;

        this.x = x;
        this.y = y;

        rows = new ArrayList<Row>();
    }

    public void render(SpriteBatch sb){
        for(int i = 0;i< rows.size();i++){
            rows.get(i).render(sb);
        }
    }

    public void debugRender(ShapeRenderer sr){
        sr.setColor(Color.WHITE);
        sr.circle(x, y, 10);
        sr.rect(x - this.width/2, y - this.height/2, width   , height);

        for(int i = 0;i< rows.size();i++){
            rows.get(i).debugRender(sr);
        }
    }

    public void addRow(Row row){
        try{
            //new Row(0f,0f,0f,0f)
            rows.add(row);
            float availableHeight = this.height;
            int numberOfChildren = rows.size();
            float availableHeightPerEntry = availableHeight/numberOfChildren;
            float shiftAmount = availableHeight/2  - availableHeightPerEntry/2;

            for(int i = 0; i < numberOfChildren; i++){
                Row view = rows.get(i);
                view.x = this.x;
                view.y = this.y +  availableHeightPerEntry * (numberOfChildren -1 - i) - shiftAmount;
                view.height = availableHeightPerEntry;
                view.width = this.width - 10f;
            }
        }catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Something went wrong!");
            rows.clear();
        }
    }
}
