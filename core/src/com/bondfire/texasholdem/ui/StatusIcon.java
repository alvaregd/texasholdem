package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;

/**
 * Created by alvaregd on 27/07/15.
 * Display an Icon
 */
public class StatusIcon extends Box{

    private final static float WIDTH_R_PARENT = 0.3f;
    private final static float HEIGHT_R_PARENT = 0.3f;

    private TextureRegion icon_image;
    BlurrableTextureAtlas atlas;

    public StatusIcon(int icon,float parent_width, float paren_height, float x, float y){

        this.x = x;
        this.y = y;
        this.width = parent_width * WIDTH_R_PARENT;
        this.height = paren_height * HEIGHT_R_PARENT;

       atlas = (BlurrableTextureAtlas) TexasHoldem.res.getAtlas("textures");

        switch (icon){
            case StatusIconBar.DEALER:
                icon_image  = atlas.findRegion("dealer_icon");
                break;
            case StatusIconBar.BIGBLIND:
                icon_image = atlas.findRegion("bigblind_icon");
                break;
            case StatusIconBar.SMALLBLIND:
               icon_image =  atlas.findRegion("smallblind_icon");
                break;
        }
    }

    public void render(SpriteBatch sb){
        if(!atlas.isBlurrable()){
            System.out.println("Blurring from Turn Tracks");
            icon_image.getTexture().getTextureData().prepare();
            atlas.PrepareBlur(icon_image.getTexture().getTextureData().consumePixmap());
        }
        //Draw the background
        atlas.bind();

        sb.draw(atlas.tex,
                x ,
                y,
                width / 2,
                height / 2,
                width,
                height,
                1,
                1,
                0,// angle
                icon_image.getRegionX(),
                icon_image.getRegionY(),
                icon_image.getRegionWidth(),
                icon_image.getRegionHeight(),
                false,
                false);
    }


}
