package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;
import com.bondfire.texasholdem.data.DataPacket;
import com.bondfire.texasholdem.data.ProfilePanelCoordinates;
import com.bondfire.texasholdem.utils.CardUtils;

import java.util.HashMap;

/**
 * Created by alvaregd on 03/08/15.
 */
public class ProfilePanel extends Box {

    private HashMap<String,PlayerProfileInformation> players;

    /** profile picture*/
    private ShaderProgram circleShader;
    private TextureRegion picture;
    private TextureRegion placehodler;
    private TextureRegion cardOne;
    private TextureRegion cardTwo;

    private float x_profile_pic;
    private float y_profile_pic;
    private float width_profile_pic;
    private float height_profile_pic;

    private float y_profile_pic_relative_device;
    private float x_profile_pic_relative_device;
    private float width_profile_pic_relative_device;
    private float height_profile_pic_relative_device;

    private float y_bg_relative_device;
    private float x_bg_relative_device;
    private float width_bg_relative_device;
    private float height_bg_relative_device;

    private float x_card_one;
    private float x_card_two;
    private float y_card;
    private float width_card;
    private float height_card;

    private CenteredCentricTextBox name;
    private CenteredCentricTextBox label_wallet;
    private CenteredCentricTextBox label_bet;
    private CenteredCentricTextBox wallet_amount;
    private CenteredCentricTextBox bet_amount;
    private CenteredCentricTextBox state;
    private CenteredCentricTextBox rank;
    private CenteredCentricTextBox noCards;

    private BlurrableTextureAtlas atlas;
    private BitmapFont bitmapFont;
    private int playerShown;

    private int myId;



    private boolean isByStander;

    public ProfilePanel(float x, float y, float width, float height){

        this.width  = TexasHoldem.WIDTH  * width;
        this.height = TexasHoldem.HEIGHT * height;
        this.x      = TexasHoldem.WIDTH  * x - this.width/2;
        this.y      = TexasHoldem.HEIGHT * y - this.height/2;

        players = new HashMap<String, PlayerProfileInformation>();

        this.atlas = (BlurrableTextureAtlas) TexasHoldem.res.getAtlas("textures");
        bitmapFont = TexasHoldem.res.getBmpFont();
        circleShader = TexasHoldem.circleShader;
        picture = atlas.findRegion("picture_placeholder");
        placehodler = atlas.findRegion("picture_placeholder");
        cardOne = atlas.findRegion("card_back_dark");
        cardTwo = atlas.findRegion("card_back_dark");

        width_profile_pic  = this.width  * ProfilePanelCoordinates.WIDTH_PROFILE_PIC_R_PANEL;
        height_profile_pic = this.width  * ProfilePanelCoordinates.HEIGHT_PROFILE_PIC_R_PANEL;
        x_profile_pic = this.x +  this.width  * ProfilePanelCoordinates.X_PROFILE_PIC_R_PANEL - width_profile_pic /2;
        y_profile_pic = this.y +  this.height * ProfilePanelCoordinates.Y_PROFILE_PIC_R_PANEL - height_profile_pic/2;

        y_bg_relative_device = this.y / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;
        x_bg_relative_device = this.x / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;
        width_bg_relative_device = this.width / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;
        height_bg_relative_device = this.height / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;

        //Calculate device dimensions for the GSLS
        y_profile_pic_relative_device = y_profile_pic / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;
        x_profile_pic_relative_device = x_profile_pic / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;
        width_profile_pic_relative_device = width_profile_pic / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;
        height_profile_pic_relative_device = height_profile_pic / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;

        //calculate dimension and coordinates of our cards
        width_card  = this.width * ProfilePanelCoordinates.WIDTH_CARD_R_PANEL;
        height_card = this.height * ProfilePanelCoordinates.HEIGHT_CARD_R_PANEL;
        x_card_one  = this.x + this.width * ProfilePanelCoordinates.X_CARD_ONE_R_PANEL - width_card/2;
        x_card_two  = this.x + this.width * ProfilePanelCoordinates.X_CARD_TWO_R_PANEL - width_card/2;
        y_card      = this.y + this.height * ProfilePanelCoordinates.Y_CARD_R_PANEL    - height_card/2;

        name   = new CenteredCentricTextBox(bitmapFont,"Guillermo",
                this.x + this.width * ProfilePanelCoordinates.X_NAME_R_PANEL,
                this.y + this.height * ProfilePanelCoordinates.Y_NAME_R_PANEL);
        state  = new CenteredCentricTextBox(bitmapFont,"Passed",
                this.x + this.width * ProfilePanelCoordinates.X_STATE_R_PANEL,
                this.y + this.height * ProfilePanelCoordinates.Y_STATE_R_PANEL);
        label_wallet  = new CenteredCentricTextBox(bitmapFont, "Wallet:",
                this.x + this.width * ProfilePanelCoordinates.X_WALLET_LABEL_R_PANEL,
                this.y + this.height * ProfilePanelCoordinates.Y_WALLET_LABEL_R_PANEL);
        wallet_amount     = new CenteredCentricTextBox(bitmapFont, "30000",
                this.x + this.width * ProfilePanelCoordinates.X_WALLET_AMOUNT_R_PANEL,
                this.y + this.height * ProfilePanelCoordinates.Y_WALLET_AMOUNT_R_PANEL);
        label_bet = new CenteredCentricTextBox(bitmapFont, "Bet",
                this.x + this.width * ProfilePanelCoordinates.X_BET_LABEL_R_PANEL,
                this.y + this.height * ProfilePanelCoordinates.Y_BET_LABEL_R_PANEL);
        bet_amount    = new CenteredCentricTextBox(bitmapFont, "20032",
                this.x + this.width * ProfilePanelCoordinates.X_BET_AMOUNT_R_PANEL,
                this.y + this.height * ProfilePanelCoordinates.Y_BET_AMOUNT_R_PANEL);

        rank = new CenteredCentricTextBox(bitmapFont, "100",
                this.x + this.width * ProfilePanelCoordinates.X_RANK_R_PANEL,
                this.y + this.height * ProfilePanelCoordinates.Y_RANK_R_PANEL);

    }

    public void addPlayerProfileInfo(String key, PlayerProfileInformation value){
        players.put(key, value);
    }

    public void render(ShapeRenderer sr, SpriteBatch sb) {

        if(!atlas.isBlurrable()){
            System.out.println("Blurring from Profile Panels");
            placehodler.getTexture().getTextureData().prepare();
            atlas.PrepareBlur(placehodler.getTexture().getTextureData().consumePixmap());
        }
        //Draw the background
        atlas.bind();

        try {
            //Render player
            if (playerShown != 0) {
                sb.end();
                //Render the backgorund
                sr.begin(ShapeRenderer.ShapeType.Filled);
                sr.setColor(0.3f, 0.3f, 0.3f, 1f);
                sr.rect(x_bg_relative_device, y_bg_relative_device, width_bg_relative_device, height_bg_relative_device);
                sr.end();

                //render the profile image
                PlayerProfileInformation info =  players.get("p_" + playerShown);

                if(info.isImageLoaded()){
                    picture = info.getProfileImage();
                    sb.setShader(circleShader);
                    circleShader.begin();
                    TexasHoldem.circleShader.setUniformf("u_position", x_profile_pic_relative_device, y_profile_pic_relative_device);
                    TexasHoldem.circleShader.setUniformf("u_resolution", width_profile_pic_relative_device, height_profile_pic_relative_device);
                    circleShader.end();

                    sb.begin();
                    sb.draw(picture, x_profile_pic, y_profile_pic, width_profile_pic, height_profile_pic);
                    sb.end();
                }else{
                    sb.begin();
                    sb.draw(atlas.tex,
                            x_profile_pic,
                            y_profile_pic,
                            width_profile_pic / 2,
                            height_profile_pic / 2,
                            width_profile_pic,
                            height_profile_pic,
                            1,
                            1,
                            0,// angle
                            picture.getRegionX(),
                            picture.getRegionY(),
                            picture.getRegionWidth(),
                            picture.getRegionHeight(),
                            false,
                            false);
                    sb.end();
                }
                sb.setShader(TexasHoldem.blurShader);
                sb.begin();

                wallet_amount.setText("" + info.wallet);
                bet_amount.setText("" + info.bet);
                rank.setText("" + info.rank);
                state.setText(info.state == DataPacket.PLAYER_STATE_CALLED ? "Called" :
                                info.state == DataPacket.PLAYER_STATE_FOLDED ? "Folded" :
                                        info.state == DataPacket.PLAYER_STATE_RAISED ? "Raised" : "Waiting"
                );

//                System.out.println("Card one:" + info.cardOne);
//                System.out.println("Card two:" + info.cardTwo);

                if(info.cardOne != -1){
                    //Load card one

                    cardOne = (myId == playerShown || info.isRevealing || isByStander) ? atlas.findRegion(CardUtils.getCardString(info.cardOne)):
                                                    atlas.findRegion(CardUtils.getCardString(52));
                    //display Card one
                    sb.draw(atlas.tex,
                            x_card_one,
                            y_card,
                            width_card / 2,
                            height_card / 2,
                            width_card,
                            height_card,
                            1,
                            1,
                            10,// angle
                            cardOne.getRegionX(),
                            cardOne.getRegionY(),
                            cardOne.getRegionWidth(),
                            cardOne.getRegionHeight(),
                            false,
                            false);
                }

                if(info.cardTwo != -1){
                    //card two
                    cardTwo =( myId == playerShown || info.isRevealing || isByStander) ? atlas.findRegion(CardUtils.getCardString(info.cardTwo)):
                                                    atlas.findRegion(CardUtils.getCardString(52));
                    sb.draw(atlas.tex,
                            x_card_two,
                            y_card,
                            width_card / 2,
                            height_card / 2,
                            width_card,
                            height_card,
                            1,
                            1,
                            -10,// angle
                            cardTwo.getRegionX(),
                            cardTwo.getRegionY(),
                            cardTwo.getRegionWidth(),
                            cardTwo.getRegionHeight(),
                            false,
                            false);
                }

                name.render(sb);
                label_wallet.render(sb);
                label_bet.render(sb);
                wallet_amount.render(sb);
                bet_amount.render(sb);
                state.render(sb);
                if(isByStander)  rank.render(sb);
            }
        } catch (NullPointerException e) {
            System.out.println("Player information does not exist");
            sb.setShader(TexasHoldem.blurShader);
            sb.begin();
        }catch (IllegalStateException e) {
            System.out.println("Woops. not suppost to happen");
            sb.setShader(TexasHoldem.blurShader);
            sb.begin();
        } finally {
        }
    }

    public void showPanel(int player){
        playerShown = player;
    }

    public void setMyId(int myId) {
        this.myId = myId;
    }

    public boolean isByStander() {
        return isByStander;
    }

    public void setIsByStander(boolean isByStander) {
        this.isByStander = isByStander;
    }

    public PlayerProfileInformation getPlayerProfile(String player){
        return players.get(player);
    }
}
