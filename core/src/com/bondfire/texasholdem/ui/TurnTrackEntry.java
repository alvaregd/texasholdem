package com.bondfire.texasholdem.ui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Logger;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.app.networkUtils.ImageDownloader;
import com.bondfire.texasholdem.TexasHoldem;
import com.bondfire.texasholdem.data.DataPacket;

/**
 * Created by alvaregd on 26/07/15.
 */

public class TurnTrackEntry extends Box {

    private Logger logger = new Logger(TurnTrackEntry.class.getName());
    private final static boolean d_setImage = false;
    private final static boolean d_changeIcon = false;

    private final static float PICTURE_RELATIVE_RADIUS = 0.8f;
    private final static float SCORE_RELATIVE_HEIGHT = 0.8f;
    private final static float SCORE_RELATIVE_WIDTH = 0.8f;
    private final static float SCORE_RELATIVE_POS_X = 0.5f;
    private final static float SCORE_RELATIVE_POS_Y = 0.4f;

    private final static float ICON_RADIUS_RELATIVE_ENTRY = 0.3f;
    private final static float X_ACTIONICON_R_PANEL  = 0.4f;
    private final static float Y_ACTIONICON_R_PANEL = 0.3f;

    private TextureRegion border;
    private TextureRegion iconImage;

    private TextureRegion dealerIcon;
    private TextureRegion sbIcon;
    private TextureRegion bbIcon;
    private TextureRegion action;
    private int state;

    private boolean isImageLoaded =false;

    private TextBox entry_score;
    private BitmapFont bitmapFont;

    private BlurrableTextureAtlas atlas;

    private int action_state = 0;

    //picture dimensions (game)
    private float picHeight;
    private float picWidth;

    //status Icon dimensions and position
    private float statusIconWidth;
    private float statusIconHeight;
    private float x_status_icon;
    private float y_status_icon;

    //action icon dimensions
    private float x_action_icon;
    private float y_action_icon;

    private boolean isDealer =false;
    private boolean isBigBlind = false;
    private boolean isSmallBlind = false;

    private String iconUrl;

    //GLSL shaders work with respect to the device resolution,
    //so we me must translate game coordinates and dimensions into
    //device coordinates and dimensions;

    private float y_icon_relative_device;
    private float x_icon_relative_device;
    private float width_icon_relative_device;
    private float height_icon_relative_device;

    private float C_H;
    private float C_W;
    private float C_Y;
    private float C_X;



    private int iconCount = 0;

    public TurnTrackEntry(){

    }

    public void setDimensions(BlurrableTextureAtlas atlas, float x, float y, float width, float height){
        this.x = x;
        this.y = y;
        this.width =width;
        this.height = height;

        this.atlas = atlas;

        //Calculate device dimensions for the GSLS
        y_icon_relative_device = y / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;
        x_icon_relative_device = x / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;

        //Calculate positions of the action status icons
        x_action_icon = x + width * X_ACTIONICON_R_PANEL;
        y_action_icon = y + height * Y_ACTIONICON_R_PANEL;

        bitmapFont = TexasHoldem.res.getBmpFont();

        //find width and height of the display iconImage
        picHeight = height * PICTURE_RELATIVE_RADIUS;
        picWidth = picHeight;

        width_icon_relative_device = picWidth / TexasHoldem.WIDTH * TexasHoldem.DEVICE_WIDTH;
        height_icon_relative_device = picWidth / TexasHoldem.HEIGHT * TexasHoldem.DEVICE_HEIGHT;

        //width and height of the status icons;
        statusIconWidth = height *ICON_RADIUS_RELATIVE_ENTRY;
        statusIconHeight = statusIconWidth;
        x_status_icon = x;
        y_status_icon = y;

        border = atlas.findRegion("red_background");
        iconImage  = atlas.findRegion("picture_placeholder");
        dealerIcon = atlas.findRegion("dealer_icon");
        sbIcon     = atlas.findRegion("smallblind_icon");
        bbIcon     = atlas.findRegion("bigblind_icon");
    }

    public void setImage(String iconUrl) {
        this.iconUrl = iconUrl;

        ImageDownloader.ImageDownloadCallBack callback = new ImageDownloader.ImageDownloadCallBack() {
            @Override
            public void TextureImageReceived(TextureRegion textureRegion) {
                iconImage = textureRegion;
                isImageLoaded = true;
            }
        };

        TexasHoldem.res.getSmallProfilePicture(iconUrl, callback);
    }

    public void render(SpriteBatch sb){
        if(!atlas.isBlurrable()){
            System.out.println("Blurring from Turn Tracks");
            border.getTexture().getTextureData().prepare();
            atlas.PrepareBlur(border.getTexture().getTextureData().consumePixmap());
        }

        atlas.bind();

        //draw the state icon
                sb.draw(atlas.tex,
                        x_action_icon,
                        y_action_icon,
                        statusIconWidth / 2,
                        statusIconHeight / 2,
                        statusIconWidth,
                        statusIconHeight,
                        1,
                        1,
                        0,// angle
                        action.getRegionX(),
                        action.getRegionY(),
                        action.getRegionWidth(),
                        action.getRegionHeight(),
                        false,
                        false);


        iconCount = 0;
        if(isBigBlind){
            sb.draw(atlas.tex,
                    x_status_icon,
                    y_status_icon,
                    statusIconWidth / 2,
                    statusIconHeight / 2,
                    statusIconWidth,
                    statusIconHeight,
                    1,
                    1,
                    0,// angle
                    bbIcon.getRegionX(),
                    bbIcon.getRegionY(),
                    bbIcon.getRegionWidth(),
                    bbIcon.getRegionHeight(),
                    false,
                    false);
            iconCount++;
        }
        if(isSmallBlind){
            sb.draw(atlas.tex,
                    x_status_icon,
                    y,
                    statusIconWidth / 2,
                    statusIconHeight / 2,
                    statusIconWidth,
                    statusIconHeight,
                    1,
                    1,
                    0,// angle
                    sbIcon.getRegionX(),
                    sbIcon.getRegionY(),
                    sbIcon.getRegionWidth(),
                    sbIcon.getRegionHeight(),
                    false,
                    false);
            iconCount++;
        }
        if(isDealer){
            sb.draw(atlas.tex,
                    x_status_icon,
                    y,
                    statusIconWidth / 2,
                    statusIconHeight / 2,
                    statusIconWidth,
                    statusIconHeight,
                    1,
                    1,
                    0,// angle
                    dealerIcon.getRegionX(),
                    dealerIcon.getRegionY(),
                    dealerIcon.getRegionWidth(),
                    dealerIcon.getRegionHeight(),
                    false,
                    false);
        }
    }

    public void renderProfile(SpriteBatch sb, ShaderProgram shader){

        if(!isImageLoaded ){

            sb.begin();
            sb.setShader(TexasHoldem.blurShader);
            sb.draw(atlas.tex,
                    x,
                    y,
                    picWidth / 2,
                    picHeight / 2,
                    picWidth,
                    picHeight,
                    1,
                    1,
                    0,// angle
                    iconImage.getRegionX(),
                    iconImage.getRegionY(),
                    iconImage.getRegionWidth(),
                    iconImage.getRegionHeight(),
                    false,
                    false);
            sb.end();
        }else {
            //NOTE TO SELF GAME WIDTH != Screen width
            sb.setShader(TexasHoldem.circleShader);
            shader.begin();

            //Change the circle fragment coordinates depending on how much "blur there is"
            C_W = width_icon_relative_device * ( 1- TexasHoldem.blurAmount);
            C_H = height_icon_relative_device * ( 1- TexasHoldem.blurAmount);

            C_Y =  y_icon_relative_device + (height_icon_relative_device - C_H)/2;
            C_X = x_icon_relative_device + (width_icon_relative_device - C_W)/2;

            TexasHoldem.circleShader.setUniformf("u_position", C_X, C_Y);
            TexasHoldem.circleShader.setUniformf("u_resolution", C_W, C_H);
            shader.end();

            sb.begin();
            sb.draw(iconImage, x, y, picWidth, picHeight);
            sb.end();
        }
    }

    public void update(float dt){

    }

    public void setScore(String score){

        entry_score = new TextBox(
                bitmapFont,
                score,
                this.x + width  *SCORE_RELATIVE_POS_X ,
                this.y + height *SCORE_RELATIVE_POS_Y);
    }

    public void setIsSmallBlind(boolean isSmallBlind) {
        this.isSmallBlind = isSmallBlind;
    }

    public void setIsBigBlind(boolean isBigBlind) {
        this.isBigBlind = isBigBlind;
    }

    public void setIsDealer(boolean isDealer) {
        this.isDealer = isDealer;
    }

    public void setState(int state){
        this.state = state;
        changeIcon();
    }

    private void changeIcon(){
        switch (this.state){
            case DataPacket.PLAYER_STATE_WAITING:
                if(d_changeIcon)logger.info("State Entry WAITING");
                action = atlas.findRegion("waiting_icon");
                break;
            case DataPacket.PLAYER_STATE_FOLDED:
                if(d_changeIcon)System.out.println("State Entry FOLDED");
                action = atlas.findRegion("passed_icon");
                break;
            case DataPacket.PLAYER_STATE_CALLED:
                if(d_changeIcon)System.out.println("State Entry CALLED");
                action = atlas.findRegion("checked_icon");
                break;
            case DataPacket.PLAYER_STATE_RAISED:
               if(d_changeIcon) System.out.println("State Entry RAISED");
                action = atlas.findRegion("raised_icon");
                break;
            case DataPacket.PLAYER_STATE_ALLIN:
                if(d_changeIcon)logger.info("State Entry WAITING");
                action = atlas.findRegion("allin_icon");
                break;
            default:break;
        }
    }
}
