package com.bondfire.texasholdem.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Logger;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;
import com.bondfire.texasholdem.data.ButtonCoordinates;
import com.bondfire.texasholdem.data.DataPacket;
import com.bondfire.texasholdem.data.ProfilePanelCoordinates;
import com.bondfire.texasholdem.data.PotPanelCoordinates;
import com.bondfire.texasholdem.specialKEval.HandEval;
import com.bondfire.texasholdem.specialKEval.SevenEval;
import com.bondfire.texasholdem.ui.CardViewPanel;
import com.bondfire.texasholdem.ui.ChoiceInterface;
import com.bondfire.texasholdem.ui.DashboardLayout;

import com.bondfire.texasholdem.ui.PlayerProfileInformation;
import com.bondfire.texasholdem.ui.PokerAnimationPanel;
import com.bondfire.texasholdem.ui.InvisibleHoldButton;
import com.bondfire.texasholdem.ui.PotInterface;
import com.bondfire.texasholdem.ui.PotPanel;
import com.bondfire.texasholdem.ui.ProfilePanel;
import com.bondfire.texasholdem.ui.StatusIconBar;
import com.bondfire.texasholdem.ui.TurnTrack;
import com.bondfire.texasholdem.ui.TurnTrackEntry;
import com.bondfire.texasholdem.utils.ArrayUtils;
import com.bondfire.texasholdem.utils.CardUtils;
import com.bondfire.texasholdem.utils.FrenchDeck;

/*** Created by alvaregd on 25/07/15.
 * Here we show main game play UI */
public class PlayState extends State{
    private final static String Tag = PlayState.class.getName();
    Logger logger = new Logger(Tag);
    private final static boolean d_processData = false;
    private final static boolean d_render = false;
    private final static boolean d_advanceProgres = false;
    private final static boolean d_onFoldClicked = false;
    private final static boolean d_onCallClicked = false;
    private final static boolean d_dealCards  = false;
    private final static boolean d_sidePotDetail = true;
    private final static boolean d_collection = true;
    private final static boolean d_processTurnInfo = true;

    /** Data **/
    DataPacket newData;
    String[] iconUrls;
    String[] imageUrls;
    String myId;
    int myDataIndex;
    int callDifference =0;

    /** update*/
    boolean updatePlayers = false;

    /** turntrack **/
    private TurnTrack turnTrack;

    /** Timer**/

    /** actions UI **/
    private ChoiceInterface optionsUI;

    /** status Icons **/
    private StatusIconBar statusIconBar;
    /** match progress track */

    /** table deck */
    private DashboardLayout table;

    /** animationPanel deck **/
    private PokerAnimationPanel animationPanel;

    /** Burn deck */

    /** pot **/
    private PotInterface pot;
    private PotPanel potPanel;

    /** hold buttons */
    private InvisibleHoldButton handButton;
    private InvisibleHoldButton p1CardZoomButton;
    private InvisibleHoldButton p2CardZoomButton;
    private InvisibleHoldButton p3CardZoomButton;
    private InvisibleHoldButton p4CardZoomButton;
    private InvisibleHoldButton p5CardZoomButton;
    private InvisibleHoldButton p6CardZoomButton;
    private InvisibleHoldButton p7CardZoomButton;
    private InvisibleHoldButton p8CardZoomButton;

    private InvisibleHoldButton p1PokerProfileButton;
    private InvisibleHoldButton p2PokerProfileButton;
    private InvisibleHoldButton p3PokerProfileButton;
    private InvisibleHoldButton p4PokerProfileButton;
    private InvisibleHoldButton p5PokerProfileButton;
    private InvisibleHoldButton p6PokerProfileButton;
    private InvisibleHoldButton p7PokerProfileButton;
    private InvisibleHoldButton p8PokerProfileButton;

    private InvisibleHoldButton potPanelButton;

    /** view panels **/
    private ProfilePanel profilePanel;
    private CardViewPanel cardPanel;

    BlurrableTextureAtlas atlas;
    ShapeRenderer sr;

    /**Miscellanious**/
    private boolean touchLatch = false;

    public PlayState(GSM gsm, String myId) {
        super(gsm);

        logger.setLevel(Logger.INFO);
        System.out.println("LOADED Play State");
        atlas = (BlurrableTextureAtlas) TexasHoldem.res.getAtlas("textures");

        this.myId = myId;

        turnTrack = new TurnTrack();
       // * create a status icon bar *
        statusIconBar = new StatusIconBar();
        handButton = new InvisibleHoldButton(
                ButtonCoordinates.X_HAND_R_PARENT_LEFT,
                ButtonCoordinates.Y_HAND_R_PARENT_BUTTOM,
                ButtonCoordinates.WIDTH_HAND_R_PARENT,
                ButtonCoordinates.HEIGHT_HAND_R_PARENT
        );
        p1CardZoomButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_ZOOM_CARDS_1_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_ZOOM_CARDS_1_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_ZOOM_CARDS_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_ZOOM_CARDS_R_PANEL_BOTTOM_SMALL
        );
        p2CardZoomButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_ZOOM_CARDS_1_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_ZOOM_CARDS_2_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_ZOOM_CARDS_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_ZOOM_CARDS_R_PANEL_BOTTOM_SMALL
        );
        p3CardZoomButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_ZOOM_CARDS_1_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_ZOOM_CARDS_3_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_ZOOM_CARDS_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_ZOOM_CARDS_R_PANEL_BOTTOM_SMALL
        );
        p4CardZoomButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_ZOOM_CARDS_1_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_ZOOM_CARDS_4_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_ZOOM_CARDS_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_ZOOM_CARDS_R_PANEL_BOTTOM_SMALL
        );
        p5CardZoomButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_ZOOM_CARDS_1_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_ZOOM_CARDS_5_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_ZOOM_CARDS_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_ZOOM_CARDS_R_PANEL_BOTTOM_SMALL
        );
        p6CardZoomButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_ZOOM_CARDS_1_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_ZOOM_CARDS_6_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_ZOOM_CARDS_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_ZOOM_CARDS_R_PANEL_BOTTOM_SMALL
        );
        p7CardZoomButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_ZOOM_CARDS_1_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_ZOOM_CARDS_7_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_ZOOM_CARDS_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_ZOOM_CARDS_R_PANEL_BOTTOM_SMALL
        );
        p8CardZoomButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_ZOOM_CARDS_1_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_ZOOM_CARDS_8_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_ZOOM_CARDS_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_ZOOM_CARDS_R_PANEL_BOTTOM_SMALL
        );

        p1PokerProfileButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_PROFILE_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_PROFILE_1_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL
        );

        p2PokerProfileButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_PROFILE_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_PROFILE_2_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL
        );

        p3PokerProfileButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_PROFILE_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_PROFILE_3_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL
        );
        p4PokerProfileButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_PROFILE_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_PROFILE_4_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL
        );
        p5PokerProfileButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_PROFILE_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_PROFILE_5_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL
        );
        p6PokerProfileButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_PROFILE_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_PROFILE_6_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL
        );
        p6PokerProfileButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_PROFILE_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_PROFILE_6_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL
        );
        p7PokerProfileButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_PROFILE_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_PROFILE_7_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL
        );
        p8PokerProfileButton = new InvisibleHoldButton(
                ButtonCoordinates.X_PLAYER_PROFILE_SMALL_R_PANEL,
                ButtonCoordinates.Y_PLAYER_PROFILE_8_R_PANEL_BOTTOM,
                ButtonCoordinates.WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL,
                ButtonCoordinates.HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL
        );

        potPanelButton = new InvisibleHoldButton(
                ButtonCoordinates.X_POT_PANEL_BUTTON_R_PARENT,
                ButtonCoordinates.Y_POT_PANEL_BUTTON_R_PARENT,
                ButtonCoordinates.WIDTH_POT_PANEL_BUTTON_R_PARENT,
                ButtonCoordinates.HEIGHT_PPOT_PANEL_BUTTON_R_PARENT
        );

        profilePanel = new ProfilePanel(
                ProfilePanelCoordinates.X_PROFILE_PANEL_R_PARENT,
                ProfilePanelCoordinates.Y_PROFILE_PANEL_R_PARENT,
                ProfilePanelCoordinates.WIDTH_PROFILE_PANEL_R_PARENT,
                ProfilePanelCoordinates.HEIGHT_PROFILE_PANEL_R_PARENT
        );

        animationPanel = new PokerAnimationPanel();
        table = new DashboardLayout();
        optionsUI = new ChoiceInterface();
        pot = new PotInterface();
        potPanel =  new PotPanel(
                PotPanelCoordinates.X_POT_BACKGROUND_R_PARENT,
                PotPanelCoordinates.Y_POT_BACKGROUND_R_PARENT,
                PotPanelCoordinates.WIDTH_POT_BACKGROUND_R_PARENT,
                PotPanelCoordinates.HEIGHT_POT_BACKGROUND_R_PARENT
        );

        updatePlayers = true;
        sr = new ShapeRenderer();
        sr.setAutoShapeType(true);
    }

    @Override
    public void update(float dt) {
        animationPanel.update(dt);
        optionsUI.update(dt);
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        if (d_render) System.out.println(Tag + " render()");
        try {

            TexasHoldem.blurShader.begin();
            TexasHoldem.blurShader.setUniformf("bias", TexasHoldem.MAX_BLUR * TexasHoldem.blurAmount);
            TexasHoldem.blurShader.end();

            sb.setProjectionMatrix(cam.combined);
            sb.begin();

            statusIconBar.render(sb);

            optionsUI.render(sb);
            pot.render(sb);

            turnTrack.render(sb);

            if(table.isTouching()){
                animationPanel.render(sb);
                table.render(sb);
            }else{
                table.render(sb);
                animationPanel.render(sb);
            }

            profilePanel.render(sr, sb);
            potPanel.render(sr,sb);
            sb.end();

            sr.begin();
            potPanelButton.render(sr);
            sr.end();

            sb.begin();
            sb.flush();

        } catch (NullPointerException e) {
            logger.error("Render()", e);
        } catch (IllegalStateException e) {
            System.out.println("Illegal State");
        } finally {
            sb.end();
        }

        if (d_render) System.out.println(Tag + " render() exit");
    }

    @Override
    public void handleInput() {
        if (Gdx.input.isTouched()) {
            mouse.x = Gdx.input.getX();
            mouse.y = Gdx.input.getY();
            cam.unproject(mouse);

            if(table.handleInput(mouse))return;

            //reset all touches
            handButton.setIsHeldDown(false);
            animationPanel.setIsMyHandBig(false);
            p1CardZoomButton.setIsHeldDown(false);
            p2CardZoomButton.setIsHeldDown(false);
            animationPanel.setIsPlayerBig1(false);
            animationPanel.setIsPlayerBig2(false);
            animationPanel.setIsPlayerBig3(false);
            animationPanel.setIsPlayerBig4(false);
            animationPanel.setIsPlayerBig5(false);
            animationPanel.setIsPlayerBig6(false);
            animationPanel.setIsPlayerBig7(false);
            animationPanel.setIsPlayerBig8(false);
            potPanel.setIsShowing(false);

//            profilePanel.showPanel(0);
           /* if(!touchLatch){
                touchLatch = true;*/
                if (handButton.contains(mouse.x, mouse.y)) {
                    handButton.setIsHeldDown(true);
                    animationPanel.setIsMyHandBig(true);
                }
                if (p1CardZoomButton.contains(mouse.x, mouse.y)) {
                    p1CardZoomButton.setIsHeldDown(true);
                    animationPanel.setIsPlayerBig1(true);
                }
                else if (p2CardZoomButton.contains(mouse.x, mouse.y)) {
                    p2CardZoomButton.setIsHeldDown(true);
                    animationPanel.setIsPlayerBig2(true);
                }
                else if (p3CardZoomButton.contains(mouse.x, mouse.y)) {
                    p3CardZoomButton.setIsHeldDown(true);
                    animationPanel.setIsPlayerBig3(true);
                } else if (p4CardZoomButton.contains(mouse.x, mouse.y)) {
                    p4CardZoomButton.setIsHeldDown(true);
                    animationPanel.setIsPlayerBig4(true);
                }
                else if (p5CardZoomButton.contains(mouse.x, mouse.y)) {
                    p5CardZoomButton.setIsHeldDown(true);
                    animationPanel.setIsPlayerBig5(true);
                }
                else if (p6CardZoomButton.contains(mouse.x, mouse.y)) {
                    p6CardZoomButton.setIsHeldDown(true);
                    animationPanel.setIsPlayerBig6(true);
                }
                else if (p7CardZoomButton.contains(mouse.x, mouse.y)) {
                    p7CardZoomButton.setIsHeldDown(true);
                    animationPanel.setIsPlayerBig7(true);
                }
                else if (p8CardZoomButton.contains(mouse.x, mouse.y)) {
                    p8CardZoomButton.setIsHeldDown(true);
                    animationPanel.setIsPlayerBig8(true);
                }
                else if (p1PokerProfileButton.contains(mouse.x, mouse.y)) {
                    p1PokerProfileButton.setIsHeldDown(true);
                    profilePanel.showPanel(1);
                }
                else if (p2PokerProfileButton.contains(mouse.x, mouse.y)) {
                    p2PokerProfileButton.setIsHeldDown(true);
                    profilePanel.showPanel(2);
                }
                else if (p3PokerProfileButton.contains(mouse.x, mouse.y)) {
                    p3PokerProfileButton.setIsHeldDown(true);
                    profilePanel.showPanel(3);
                }
                else if (p4PokerProfileButton.contains(mouse.x, mouse.y)) {
                    p4PokerProfileButton.setIsHeldDown(true);
                    profilePanel.showPanel(4);
                }
                else if (p5PokerProfileButton.contains(mouse.x, mouse.y)) {
                    p5PokerProfileButton.setIsHeldDown(true);
                    profilePanel.showPanel(5);
                }
                else if (p6PokerProfileButton.contains(mouse.x, mouse.y)) {
                    p6PokerProfileButton.setIsHeldDown(true);
                    profilePanel.showPanel(6);
                }
                else if (p7PokerProfileButton.contains(mouse.x, mouse.y)) {
                    p7PokerProfileButton.setIsHeldDown(true);
                    profilePanel.showPanel(7);
                }
                else if (p8PokerProfileButton.contains(mouse.x, mouse.y)) {
                    p8PokerProfileButton.setIsHeldDown(true);
                    profilePanel.showPanel(8);
                } else if (potPanelButton.contains(mouse.x, mouse.y)) {
                    potPanel.setIsShowing(true);
                }
        }else{
            touchLatch = false;
            handButton.setIsHeldDown(false);
            animationPanel.setIsMyHandBig(false);
            p1CardZoomButton.setIsHeldDown(false);
            p2CardZoomButton.setIsHeldDown(false);
            animationPanel.setIsPlayerBig1(false);
            animationPanel.setIsPlayerBig2(false);
            animationPanel.setIsPlayerBig3(false);
            animationPanel.setIsPlayerBig4(false);
            animationPanel.setIsPlayerBig5(false);
            animationPanel.setIsPlayerBig6(false);
            animationPanel.setIsPlayerBig7(false);
            animationPanel.setIsPlayerBig8(false);
            profilePanel.showPanel(0);
            potPanel.setIsShowing(false);
            table.setShowingfaslse();
        }

        if (Gdx.input.justTouched()) {
            mouse.x = Gdx.input.getX();
            mouse.y = Gdx.input.getY();
            cam.unproject(mouse);

            switch(optionsUI.handleInput(mouse.x, mouse.y)){
                case DataPacket.PLAYER_STATE_CALLED: onCallClicked();break;
                case DataPacket.PLAYER_STATE_FOLDED: onFoldClicked();break;
                case DataPacket.PLAYER_STATE_RAISED: onRaisedClicked();break;
                default: break;
            }
        }
    }

    public void updateData(String playerid, DataPacket data, String[] imageUrls, String[] iconUrls){
        myId = playerid;

        if(newData == null){
            newData = data;
            updatePlayers = true;
        }else{
            updatePlayers = data.playerCount != newData.playerCount;
        }
        this.newData = data;
        this.imageUrls = imageUrls;
        this.iconUrls = iconUrls;
        ProcessData();
    }

    public void updateData(DataPacket data, String[] imageUrls, String[] iconUrls){
        if(newData == null){
            newData = data;
            updatePlayers = true;
        }else{
            updatePlayers = data.playerCount != newData.playerCount;
        }

        this.newData = data;
        this.imageUrls = imageUrls;
        this.iconUrls = iconUrls;
        ProcessData();
    }

    private void ProcessData(){
        if(d_processData)System.out.println("ProcessData()");
        if(d_processTurnInfo)System.out.println("Process Turn Data=============================================");
        int i;

        //We have a different number of players, update the entire list
        if(updatePlayers){
            try{
                turnTrack.clearPlayers();
                for(i = 0 ; i < newData.playerCount; i++){
                    TurnTrackEntry entry = new TurnTrackEntry();
//                    if(iconUrls != null)   entry.setImage( i + 1);
                    if(iconUrls != null)   entry.setImage(iconUrls[i]);
                    turnTrack.addPlayerToTrack("p_" +( i + 1),entry);
                    entry.setState(newData.playerState[i]);
                    profilePanel.addPlayerProfileInfo("p_" + (i+1),
                            new PlayerProfileInformation(
                                    imageUrls[i],
                                    newData.wallets[i],
                                    newData.bets[i],
                                    newData.playerState[i],
                                    newData.ranks[i],
                                    newData.hands[i][0],
                                    newData.hands[i][1],
                                    newData.isShowingCards[i]
                            ));
                }
            }catch (NullPointerException e) {
                logger.error("Error occured while setting players, Null",e);
            }catch (ArrayIndexOutOfBoundsException e){
                System.out.println("Image doesn't exit");
            }
        }else{
            //only update the required players
            for(i = 0 ; i < newData.playerCount; i++) {
                TurnTrackEntry entry = turnTrack.getPlayerEntry("p_" + (i + 1));
                entry.setState(newData.playerState[i]);
                PlayerProfileInformation info = profilePanel.getPlayerProfile("p_" + (i+1));
                info.state = newData.playerState[i];
                info.bet   = newData.bets[i];
                info.wallet = newData.wallets[i];
                info.rank = newData.ranks[i];
                info.cardOne = newData.hands[i][0];
                info.cardTwo = newData.hands[i][1];
                info.isRevealing = newData.isShowingCards[i];
            }
        }

        /** set the turn on the turn track */
        turnTrack.setTurn(      "p_" + newData.currentTurn);
        turnTrack.setDealer(    "p_" + newData.dealer);
        turnTrack.setSmallBlind("p_" + newData.smallBlind);
        turnTrack.setBigBlind(  "p_" + newData.bigBlind);

        /** see if we have any status icons*/
        statusIconBar.empty();
        if(myId.equals("p_" + newData.dealer    ))  statusIconBar.setIcon(StatusIconBar.DEALER);
        if(myId.equals("p_" + newData.smallBlind))  statusIconBar.setIcon(StatusIconBar.SMALLBLIND);
        if(myId.equals("p_" + newData.bigBlind  ))  statusIconBar.setIcon(StatusIconBar.BIGBLIND);

        pot.setPotAmount(ArrayUtils.sum(newData.cashPiles));
        if(newData.workingPotIndex > 0) pot.setSidePot(true);
        potPanel.setData(newData.cashPiles, newData.potParticipants, newData.workingPotIndex);

        /** animate a new card appearing */
        myDataIndex = Integer.valueOf(myId.substring(2)) -1;
        profilePanel.setMyId(myDataIndex + 1);
        if(newData.playerState[myDataIndex] == DataPacket.PLAYER_STATE_BYSTANDER) profilePanel.setIsByStander(true);


        /** Check the state and see what interfaces we must bring up**/
        if(newData.gameState == DataPacket.STATE_BLINDS){
            //set the the choice interface to blinds mode
            if(newData.roundProgress == 1 ){
                if(d_collection)System.out.println("Getting Ceiling");
                collectCeilings();
            }
            optionsUI.setCallAmount(newData.bigBlindAmount);
            optionsUI.setBlindsMode();
            optionsUI.setVisibility(isMyTurn(newData.currentTurn));
            animationPanel.setState(newData.gameState);
            animationPanel.setVisibility(false);
            table.setVisibility(false);
        }else if (newData.gameState == DataPacket.STATE_PREFLOP){

            //only collect ceiling information at the begining of a match
            callDifference = getBiggestBet(newData.bets) - newData.bets[myDataIndex]; //Find out how much we need to call

            if(callDifference == 0)  optionsUI.setCheckedText();
            else  optionsUI.setCallText();
            optionsUI.setMaxBetAmount(findMaxRaiseAmount(newData.wallets[myDataIndex], callDifference));
            optionsUI.ResetRaiseCount();
            optionsUI.setCallAmount(callDifference);
            optionsUI.setFlopMode();
            optionsUI.setVisibility(isMyTurn(newData.currentTurn));
            animationPanel.setMyCards(newData.hands[myDataIndex][0],newData.hands[myDataIndex][1]);
            animationPanel.setState(newData.gameState);
            animationPanel.setVisibility(true);
            animationPanel.setPlayerHandRevealing(false);
            animationPanel.setState(newData.gameState);
            if(newData.roundProgress ==1 ) animationPanel.distributeCards();
            table.setVisibility(false);

        }else if (newData.gameState == DataPacket.STATE_FLOP){
            callDifference = getBiggestBet(newData.bets) - newData.bets[myDataIndex];
            if(callDifference == 0)  optionsUI.setCheckedText();
            else  optionsUI.setCallText();
            optionsUI.setMaxBetAmount(findMaxRaiseAmount(newData.wallets[myDataIndex], callDifference));
            optionsUI.ResetRaiseCount();
            optionsUI.setCallAmount(callDifference);
            optionsUI.setFlopMode();
            optionsUI.setVisibility(isMyTurn(newData.currentTurn));
            animationPanel.setMyCards(newData.hands[myDataIndex][0], newData.hands[myDataIndex][1]);
            animationPanel.setState(newData.gameState);
            animationPanel.setVisibility(true);
            animationPanel.setPlayerHandRevealing(false);
            table.setCards(newData.table);
            table.setVisibility(true);

        }else if (newData.gameState == DataPacket.STATE_TURN){
            callDifference = getBiggestBet(newData.bets) - newData.bets[myDataIndex];
            if(callDifference == 0)  optionsUI.setCheckedText();
            else  optionsUI.setCallText();
            optionsUI.setMaxBetAmount(findMaxRaiseAmount(newData.wallets[myDataIndex], callDifference));
            optionsUI.ResetRaiseCount();
            optionsUI.setCallAmount(callDifference);
            optionsUI.setFlopMode();
            optionsUI.setVisibility(isMyTurn(newData.currentTurn));
            animationPanel.setMyCards(newData.hands[myDataIndex][0], newData.hands[myDataIndex][1]);
            animationPanel.setState(newData.gameState);
            animationPanel.setVisibility(true);
            animationPanel.setPlayerHandRevealing(false);
            table.setCards(newData.table);
            table.setVisibility(true);

        }else if (newData.gameState == DataPacket.STATE_RIVER){
            callDifference = getBiggestBet(newData.bets) - newData.bets[myDataIndex];
            if(callDifference == 0)  optionsUI.setCheckedText();
            else  optionsUI.setCallText();
            optionsUI.setMaxBetAmount(findMaxRaiseAmount(newData.wallets[myDataIndex], callDifference));
            optionsUI.ResetRaiseCount();
            optionsUI.setCallAmount(callDifference);
            optionsUI.setFlopMode();
            optionsUI.setVisibility(isMyTurn(newData.currentTurn));
            animationPanel.setMyCards(newData.hands[myDataIndex][0], newData.hands[myDataIndex][1]);
            animationPanel.setState(newData.gameState);
            animationPanel.setVisibility(true);
            animationPanel.setPlayerHandRevealing(false);
            table.setCards(newData.table);
            table.setVisibility(true);

        }else if (newData.gameState == DataPacket.STATE_SHOWDOWN){
            optionsUI.setShowMode();
            optionsUI.setVisibility(isMyTurn(newData.currentTurn));
            animationPanel.setMyCards(newData.hands[myDataIndex][0], newData.hands[myDataIndex][1]);
            animationPanel.setState(newData.gameState);
            animationPanel.setVisibility(true);
            animationPanel.setPlayerHandRevealing(true);
            animationPanel.setPlayerData(newData.isShowingCards,newData.hands);
            table.setCards(newData.table);
            table.setVisibility(true);
        }

        else if (newData.gameState == DataPacket.STATE_WINNER){
            optionsUI.setShowMode();
            optionsUI.setVisibility(isMyTurn(newData.currentTurn));
            animationPanel.setMyCards(newData.hands[myDataIndex][0], newData.hands[myDataIndex][1]);
            animationPanel.setState(newData.gameState);
            animationPanel.setVisibility(true);
            animationPanel.setPlayerHandRevealing(true);
            animationPanel.setPlayerData(newData.isShowingCards,newData.hands);
            table.setCards(newData.table);
            table.setVisibility(true);
        }
    }

    private boolean isMyTurn(int turn){
        return myId.equals("p_"+turn);
    }

    /*** BUTTON ACTIONS **/
    public void onFoldClicked(){
        if(d_onFoldClicked)System.out.println("Folded Clicked");
        newData.playerState[newData.currentTurn -1] = DataPacket.PLAYER_STATE_FOLDED;               //change the player's state to folded
        newData.roundLength --;                                                                     //because the player folded, the round's length is decreased by one
        newData.roundProgress--;                                                                    //the progress is also decreased by one

        advanceProgress();
        TexasHoldem.tester.takeTurn(newData);
    }

    public void onCallClicked(){
       if(d_onCallClicked) System.out.println("Called Clicked");

        if(newData.gameState == DataPacket.STATE_BLINDS){
            newData.cashPiles[newData.workingPotIndex] += newData.bigBlindAmount;//Are we in blinds mode?
            newData.bets[newData.currentTurn - 1 ]     +=newData.bigBlindAmount;                        //set the player's bet amount higher
            newData.wallets[newData.currentTurn -1]    -= newData.bigBlindAmount;                      //decrease the player's wallet amount
            newData.playerState[newData.currentTurn -1] = DataPacket.PLAYER_STATE_CALLED;           //set the player's status to called
            isAllIn();
        }

        //Are we in showdown state?
        else if(newData.gameState == DataPacket.STATE_SHOWDOWN){
            newData.isShowingCards[newData.currentTurn - 1] = true;                                 //Set the player's cards visible
            newData.playerState[newData.currentTurn -1]     = DataPacket.PLAYER_STATE_WAITING;          //set the player's state after this cards are shown
            computeRankOfSeven();                                                                   //calculate the rank of this player
        }

        //Are we in betting state?
        else{
           if(!isAllIn()){
               newData.playerState[newData.currentTurn -1] = DataPacket.PLAYER_STATE_CALLED;
               newData.cashPiles[newData.workingPotIndex] += callDifference;
               newData.bets[newData.currentTurn - 1 ]     += callDifference;                   //increase the player's bet amount
               newData.wallets[newData.currentTurn -1]    -= callDifference;
            }
        }
        advanceProgress();
        TexasHoldem.tester.takeTurn(newData);
    }

    public void onRaisedClicked(){
        System.out.println("Raised Clicked");

        if(optionsUI.getRaiseCount() == 0){
            //the player pressed raised, but raised 0, so treat it as a call/check
            onCallClicked();
            return;
        }

        newData.roundProgress = 1;         //If you raise, set progress down to 1,
        for(int i= 0; i < newData.playerCount; i++){
            if(newData.playerState[i] != DataPacket.PLAYER_STATE_FOLDED
                    && newData.playerState[i] != DataPacket.PLAYER_STATE_ALLIN
                    &&newData.playerState[i]  != DataPacket.PLAYER_STATE_BYSTANDER){
                newData.playerState[i]         = DataPacket.PLAYER_STATE_WAITING;
            }
        }

        newData.playerState[newData.currentTurn -1] = DataPacket.PLAYER_STATE_RAISED;               //set the player's state to Raised

        if(!isAllIn()){
            newData.cashPiles[newData.workingPotIndex]+= callDifference  + optionsUI.getRaiseCount();
            newData.bets[newData.currentTurn - 1 ]    += callDifference  + optionsUI.getRaiseCount();       //increase the player's bet amount
            newData.wallets[newData.currentTurn -1]   -= callDifference  +  optionsUI.getRaiseCount();      //decrease the player's wallet
        }
        advanceProgress();
        TexasHoldem.tester.takeTurn(newData);
    }

    /** advance the game, (set the next state, and turns etc **/
    public void advanceProgress(){

        //Did everyone fold except one person OR are we in WINNER STATE?
        if(newData.roundLength == 1 || (newData.gameState == DataPacket.STATE_WINNER )){
            //Have the remaining player collect the pile

            newData.gameState = 0;                                                                  //go back to collecting blinds
            newData.roundProgress = 0;                                                              //set the round progress to one
            newData.roundLength = newData.playerCount;                                              //everyone is playing again, so reset roundlengh
            resetGame(true);
        }

        //Did we go through all players this round?
        else if(newData.roundProgress == newData.roundLength ){ //yes,

            if(newData.gameState == DataPacket.STATE_BLINDS)dealCards();
            if(newData.gameState == DataPacket.STATE_PREFLOP)dealFlop();
            if(newData.gameState == DataPacket.STATE_FLOP)dealTurn();
            if(newData.gameState == DataPacket.STATE_TURN)dealRiver();

            newData.roundProgress = 1; //reset progress
            newData.gameState += 1; //increment the game state

            //need to check how many players are folded
            resetGame(false);

        }else{ //No, so increment round
            newData.roundProgress +=1;
        }

        //see who goes next
        do{
            newData.currentTurn++;
            //have we reached the max player?
            if(newData.currentTurn == (newData.playerCount + 1)) newData.currentTurn = 1;
        }

        while (newData.playerState[newData.currentTurn-1] == DataPacket.PLAYER_STATE_FOLDED ||  //skip this players turn if they have folded
                (newData.allIn[newData.currentTurn-1] != -1 && newData.gameState != DataPacket.STATE_SHOWDOWN ));

        if(d_advanceProgres)logger.info("advanceProgress() Enter");
        if(d_advanceProgres)logger.info("RoundLength:   " + newData.roundLength);
        if(d_advanceProgres)logger.info("RoundProgress: " + newData.roundProgress);
        if(d_advanceProgres)logger.info("Game State:    " + newData.gameState);
        if(d_advanceProgres)logger.info("Next Player:   " + newData.currentTurn);
    }

    private boolean outOfCash(){
        return callDifference > newData.wallets[newData.currentTurn -1];
    }

    private void collectCeilings(){
        newData.ceilings = new int[newData.wallets.length];

        for(int i=0; i < newData.ceilings.length; i++){
            newData.ceilings[i] = newData.wallets[i];
            System.out.println("Ceilings:" + newData.ceilings[i]);
        }
    }

    private boolean isAllIn(){
        System.out.println("Handle Ceilings");

        newData.potCeiling = getBiggestBet(newData.bets);

        if(newData.potCeiling > newData.ceilings[newData.currentTurn - 1] ){
            System.out.println("Player "+ newData.currentTurn + " is all in!");
            System.out.println("Because " + newData.potCeiling + " > " + newData.ceilings[newData.currentTurn -1 ]);

            newData.cashPiles[newData.workingPotIndex] += (newData.wallets[newData.currentTurn-1]);
            System.out.println("Amount contributed by allin player"  + (newData.wallets[newData.currentTurn-1]));
            System.out.println("Current Pile Amount "  + newData.cashPiles[newData.workingPotIndex]);

            int tempPot = newData.cashPiles[newData.workingPotIndex];

            //get total number of players checked and the person that raised
            int potcontributorsCount = 0;
            for(int i = 0; i < newData.playerState.length; i++){
                if(newData.playerState[i] == DataPacket.PLAYER_STATE_RAISED ||
                        newData.playerState[i] == DataPacket.PLAYER_STATE_CALLED   ){
                    potcontributorsCount ++;
                }
            }

            System.out.println("Pot Contributor Count: " +  potcontributorsCount);
            System.out.println("Ceiling difference: " + (newData.potCeiling - newData.ceilings[newData.currentTurn - 1]));
            System.out.println("Current Pile Amount "  + newData.cashPiles[newData.workingPotIndex]);
            System.out.println("Amount subtracted current pile: "+ (newData.potCeiling - newData.ceilings[newData.currentTurn - 1]) * (potcontributorsCount));
            newData.cashPiles[newData.workingPotIndex] -= (newData.potCeiling - newData.ceilings[newData.currentTurn - 1]) * (potcontributorsCount);

            //Add the player's remaining wallet to the old pile

            newData.workingPotIndex++;
            System.out.println("Amount given to new pile " + (tempPot -  newData.cashPiles[newData.workingPotIndex -1]));
            newData.cashPiles[newData.workingPotIndex] = tempPot -  newData.cashPiles[newData.workingPotIndex -1];
            System.out.println("New Pile Amount "  + newData.cashPiles[newData.workingPotIndex]);

            //Change the player's bet, and wallet amount
            newData.bets[newData.currentTurn-1] += (newData.wallets[newData.currentTurn-1]);
            newData.wallets[newData.currentTurn-1] -= (newData.wallets[newData.currentTurn-1]);

            //Indicate that we are all in
            newData.playerState[newData.currentTurn -1] = DataPacket.PLAYER_STATE_ALLIN;
            newData.allIn[newData.currentTurn -1]       = newData.workingPotIndex -1;
            newData.roundLength --;
            newData.roundProgress --;
            return true;
        }else{
            return false;
        }
    }

    //Checks the different piles to see if we have reached their ceilings
    //and if so, distribute the appropriate amount to that pile
    private void handleCeilings(int betAmount){


        //get sorted ceilings smallest to largest
        //but we also need to know which pile index that ceiling is under

        //for each ceiling, check if we are over or not already

        //if we are over, we don't need to put anything in there,
        //if we are not over, place a bet amount such that total bet amount for that pile == ceiling amount
        // repeat until we run out of the bet amount
        //add the player to the pot participant

    }

    /**** Resets the player's states
     * @param newGame If newGame false, it means we are not starting a new game and all players
     *                who are passed remain in folded state */
    private void resetGame(boolean newGame){
        if(newGame){
            for(int i = 0; i < newData.playerCount; i++){

                //if a player is STATE_BYSTANDER, don't change his sate
                if( newData.playerState[i] != DataPacket.PLAYER_STATE_BYSTANDER){
                    newData.playerState[i] =  DataPacket.PLAYER_STATE_WAITING; //reset all player states to waiting
                }

                newData.bets[i]  = 0;                                     //reset the amount they bet
                //GIVE cash to wining player

                //hide all cards
                newData.isShowingCards[i] = false;

                //remove all cards from the players
                newData.hands[i][0] = -1;
                newData.hands[i][1] = -1;

                //set a new dealer, and sb and bb

                //clear the ranks
                newData.ranks[i] = 0;

                //clear the all in state
                newData.allIn[i] = -1;

                newData.cashPiles[i] = 0;
            }

            //remove all cards from the table
            newData.table[0] = -1;
            newData.table[1] = -1;
            newData.table[2] = -1;
            newData.table[3] = -1;
            newData.table[4] = -1;
            newData.burns[0] = -1;
            newData.burns[1] = -1;
            newData.burns[2] = -1;
            newData.workingPotIndex = 0;

        }else{
            for(int i = 0; i < newData.playerCount; i++){
                if( newData.playerState[i] != DataPacket.PLAYER_STATE_FOLDED &&
                        newData.playerState[i] != DataPacket.PLAYER_STATE_ALLIN &&
                        newData.playerState[i] != DataPacket.PLAYER_STATE_BYSTANDER)
                newData.playerState[i] = DataPacket.PLAYER_STATE_WAITING; //reset all player states to waiting
            }
        }
    }

    private int getBiggestBet(int[] bets){
        int ret = 0;
        for(int i =0 ; i <bets.length; i++){
            if(bets[i] > ret) ret = bets[i];
        }
        return  ret;
    }

    private int findMaxRaiseAmount(int wallet, int calling){
        if(wallet < calling){
            return wallet;
        }else{
            return wallet - calling;
        }
    }

    private void dealCards(){
        if(d_dealCards )logger.info("DEALING CARDS");

        FrenchDeck deck = CardUtils.getNewFrenchDeckInstance();

        int playersRemaining = 0;
        for(int i =0; i < newData.playerCount; i++){
            if(newData.playerState[i] != DataPacket.PLAYER_STATE_FOLDED)playersRemaining ++;
        }
        for(int j = 0; j < playersRemaining; j++){
            newData.hands[j][0]= CardUtils.PickRandom(deck);
            newData.hands[j][1]= CardUtils.PickRandom(deck);
        }
    }

    private void dealFlop(){
        if(d_dealCards )logger.info("DEALING CARDS");

        int[] pile = new int[2*newData.playerCount];
        for(int i = 0;i < newData.playerCount; i++){
            pile[i] = newData.hands[i][0]; //
        }
        for(int i = 0;i < newData.playerCount; i++){
            pile[i+8] = newData.hands[i][1]; //
        }
        FrenchDeck deck = CardUtils.restoreDeck(pile);
        newData.burns[0] = CardUtils.PickRandom(deck);
        newData.table[0] = CardUtils.PickRandom(deck);
        newData.table[1] = CardUtils.PickRandom(deck);
        newData.table[2] = CardUtils.PickRandom(deck);
    }

    private void dealTurn(){
        if(d_dealCards )logger.info("DEALING CARDS");

        int[] pile = new int[2*newData.playerCount + 4 ];
        for(int i = 0;i < newData.playerCount; i++){
            pile[i] = newData.hands[i][0]; //
        }

        for(int i = 0;i < newData.playerCount; i++){
            pile[i+8] = newData.hands[i][1]; //
        }

        pile[2*newData.playerCount] = newData.table[0];
        pile[2*newData.playerCount + 1] = newData.table[1];
        pile[2*newData.playerCount + 2] = newData.table[2];
        pile[2*newData.playerCount + 3] = newData.burns[0];

        FrenchDeck deck = CardUtils.restoreDeck(pile);
        newData.burns[1] = CardUtils.PickRandom(deck);
        newData.table[3] = CardUtils.PickRandom(deck);
    }
    private void dealRiver(){
        if(d_dealCards )logger.info("DEALING CARDS");

        int[] pile = new int[2*newData.playerCount + 6];
        for(int i = 0;i < newData.playerCount; i++){
            pile[i] = newData.hands[i][0]; //
        }
        for(int i = 0;i < newData.playerCount; i++){
            pile[i+8] = newData.hands[i][1]; //
        }

        pile[2*newData.playerCount] = newData.table[0];
        pile[2*newData.playerCount + 1] = newData.table[1];
        pile[2*newData.playerCount + 2] = newData.table[2];
        pile[2*newData.playerCount + 3] = newData.table[3];
        pile[2*newData.playerCount + 4] = newData.burns[0];
        pile[2*newData.playerCount + 5] = newData.burns[1];

        FrenchDeck deck = CardUtils.restoreDeck(pile);
        newData.burns[2] = CardUtils.PickRandom(deck);
        newData.table[4] = CardUtils.PickRandom(deck);
    }

    private void computeHandWorth(){
        HandEval eval = new HandEval();

        int[] kards = new int[newData.playerCount*2];

        for(int i = 0; i < newData.playerCount;i++){
            System.out.println("Got " + newData.hands[i][0]);
            System.out.println("Got"  + newData.hands[i][1]);
            kards[i*2 ]  = newData.hands[i][0] ;  //0,2,4,6,8,10,12,14
            kards[i*2 +1 ] = newData.hands[i][1] ;  //1,3,5,7,9,11,13,15
        }
        System.out.print("RESULT" + eval.computePreFlopEquityForSpecificHoleCards(kards,8));
    }

    private void computeRankOfSeven(){
        SevenEval eval = new SevenEval();

        newData.ranks[myDataIndex] = eval.getRankOf(
                newData.hands[myDataIndex][0],
                newData.hands[myDataIndex][1],
                newData.table[0],
                newData.table[1],
                newData.table[2],
                newData.table[3],
                newData.table[4]);

        System.out.println(myId + "Rank: " + newData.ranks[myDataIndex]);
    }
}
