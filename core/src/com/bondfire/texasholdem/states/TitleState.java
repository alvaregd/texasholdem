package com.bondfire.texasholdem.states;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.texasholdem.TexasHoldem;
import com.bondfire.texasholdem.ui.CenteredCentricTextBox;
import com.bondfire.texasholdem.ui.TextBox;

public class TitleState extends State {

    private CenteredCentricTextBox title;
    private CenteredCentricTextBox instruction;

    BlurrableTextureAtlas atlas;

    private BitmapFont bitmapFont;

    public TitleState(GSM gsm){
        super(gsm);

        System.out.println("LOADED Title State");
        bitmapFont = TexasHoldem.res.getBmpFont();
        title = new CenteredCentricTextBox(bitmapFont,"Texas Holdem", TexasHoldem.WIDTH/2, TexasHoldem.HEIGHT/2);
        instruction = new CenteredCentricTextBox(bitmapFont,"Join a Match", TexasHoldem.WIDTH/2, TexasHoldem.HEIGHT/2 - 210);
    }

    @Override
    public void update(float dt) {
        this.handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(cam.combined);
        sb.begin();

        TexasHoldem.blurShader.setUniformf("bias", TexasHoldem.MAX_BLUR * TexasHoldem.blurAmount);

        title.render(sb);
//        instruction.render(sb);

        sb.flush();
        sb.end();
    }


    @Override
    public void handleInput() {

    }
}
