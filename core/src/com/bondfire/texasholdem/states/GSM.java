package com.bondfire.texasholdem.states;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.bondfire.texasholdem.TexasHoldem;

import java.util.Stack;


public class GSM {

    private GSM self = this;
    private Stack<State> states; //Our states stacks
    private final static int STATE_BACKGROUND = 0;

    public GSM(){
        states = new Stack<State>();
    }

    public void push(State s){
        states.push(s);
    }

    public void pop(){
        states.pop();
    }

    public State peek(){
        return states.peek();
    }

    /** replace top of the stack */
    public void set(State s){

        if(states.isEmpty()){
            states.push(new BackgroundState(this, TexasHoldem.timeInSeconds));
            states.push(s);
        }else{

            State state = states.pop();
            if(state instanceof TitleState)System.out.println("Removing TitleState");
            states.push(s);
        }
    }

    /** our GSM is only going to be updating the TOP of the stack */
    public void update(float dt){
        /** get the top of the stack */
        for(int i = 0; i < states.size();i++){
            states.get(i).update(dt);
        }
    }

    public void render(SpriteBatch sp){
        for(int i = 0; i < states.size();i++){
            states.get(i).render(sp);
        }
    }

    public State getBackground(){
        return states.get(STATE_BACKGROUND);
    }


}
