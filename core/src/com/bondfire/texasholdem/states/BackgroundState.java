package com.bondfire.texasholdem.states;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.bondfire.app.bfUtils.GradientBoard;

/**
 * Created by alvaregd on 25/07/15.
 * background
 */
public class BackgroundState extends State  {

    private GradientBoard backWall;
    private ShapeRenderer sr;

    public BackgroundState(GSM gsm, int time){
        super(gsm);
        System.out.println("LOADED BG");
        backWall = GradientBoard.newIntance(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(),time);
        sr = new ShapeRenderer();
    }

    @Override
    public void update(float dt) {
    }

    @Override
    public void render(SpriteBatch sb) {
        backWall.render(sr);
    }

    @Override
    public void handleInput() {

    }
}
