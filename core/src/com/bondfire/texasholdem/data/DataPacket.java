package com.bondfire.texasholdem.data;

/**
 * Created by alvaregd on 25/07/15.
 * Holds information about the game
 */
public class DataPacket {

    public final static int STATE_BLINDS   = 0;
    public final static int STATE_PREFLOP  = 1;
    public final static int STATE_FLOP     = 2;
    public final static int STATE_TURN     = 3;
    public final static int STATE_RIVER    = 4;
    public final static int STATE_SHOWDOWN = 5;
    public final static int STATE_WINNER   = 6;

    public final static int PLAYER_STATE_WAITING   = 0;
    public final static int PLAYER_STATE_RAISED    = 1;
    public final static int PLAYER_STATE_FOLDED    = 2;
    public final static int PLAYER_STATE_CALLED    = 3;
    public final static int PLAYER_STATE_BYSTANDER = 4;
    public final static int PLAYER_STATE_ALLIN     = 5;

    public int playerCount; // 1 to 8
    public int currentTurn; // 1 to MAX Player
    public int roundProgress; //the current progress of  the round
    public int roundLength;  // the current round length, varies with who's checked, passed, allin etc..

    public int dealer;   //1 to Max Player
    public int bigBlind; //1 to Max Player
    public int smallBlind; // 1 to Max Player
    public byte gameState; // 0 - 4, increments after currenTurn reaches MAX player

    public int  [][] hands;   //the player's hands
    public int  [] burns;    //which cards are burned
    public int  [] table;    // whats on the table

    public int  [] wallets;     //how much money the player has in the match
    public int  [] bets;        //How much the player has bet in the MATCH
    public int  [] playerState; //The player's state throughout the match
    public int  [] ranks;       //the player's score based on his cards
    public boolean[] isShowingCards; //wether the player is showing his cards or not

    public int smallBlindAmount;
    public int bigBlindAmount;

    public int workingPotIndex; //tells us which pot is the working pot

    public int potCeiling; //the biggest ceiling amongts the bets
    public int[] ceilings; //helps determine when a player is all-in
    public int[] cashPiles; //the different piles that get created due to all-in
    public int[][] potParticipants; //keep track of who belongs to which pile
    public int[] allIn; //keep track which player calls All in on which pile, index is player,

    public int[] ceilingOrder; //keep track when players call AllIn, value indicates which player, which we use to fetch the ceiling


}
