package com.bondfire.texasholdem.data;

/**
 * Created by alvaregd on 14/08/15.
 */

public final class PotPanelCoordinates {

    public final static float X_POT_BACKGROUND_R_PARENT = 0.5f;
    public final static float Y_POT_BACKGROUND_R_PARENT = 0.55f;
    public final static float WIDTH_POT_BACKGROUND_R_PARENT = 0.8f;
    public final static float HEIGHT_POT_BACKGROUND_R_PARENT= 0.7f;

    public final static float X_ICON_R_PANEL = 0.5f;
    public final static float Y_ICON_R_PANEL = 0.9f;
    public final static float WIDTH_ICON_R_PANEL = 0.2f;
    public final static float HEIGHT_ICON_R_PANEL = 0.1f;

    public final static float X_ORDER_COLUMN_R_PANEL = 0.07f;
    public final static float Y_ORDER_COLUMN_R_PANEL = 0.45f;
    public final static float WIDTH_ORDER_COLUMN_R_PANEL = 0.1f;
    public final static float HEIGHT_ORDER_COLUMN_R_PANEL = 0.75f;

    public final static float X_DASHBOARD_COLUMN_R_PANEL = 0.43f;
    public final static float Y_DASHBOARD_COLUMN_R_PANEL = 0.45f;
    public final static float WIDTH_DASHBOARD_COLUMN_R_PANEL = 0.55f;
    public final static float HEIGHT_DASHBOARD_COLUMN_R_PANEL = 0.75f;

    public final static float X_NUMBER_COLUMN_R_PANEL = 0.75f;
    public final static float Y_NUMBER_COLUMN_R_PANEL = 0.45f;
    public final static float WIDTH_NUMBER_COLUMN_R_PANEL = 0.26f;
    public final static float HEIGHT_NUMBER_COLUMN_R_PANEL = 0.75f;

}
