package com.bondfire.texasholdem.data;

/**
 * Created by alvaregd on 06/08/15.
 */
public class AnimationPanelCoordinates {

    public final static int ANIMATE_BURN_ONE     = 0;
    public final static int ANIMATE_BURN_TWO     = 1;
    public final static int ANIMATE_BURN_THREE   = 2;
    public final static int ANIMATE_PLAYER_ONE   = 5;
    public final static int ANIMATE_PLAYER_TWO   = 6;
    public final static int ANIMATE_PLAYER_THREE = 7;
    public final static int ANIMATE_PLAYER_FOUR  = 8;
    public final static int ANIMATE_PLAYER_FIVE  = 9;
    public final static int ANIMATE_PLAYER_SIX   = 10;
    public final static int ANIMATE_PLAYER_SEVEN = 11;
    public final static int ANIMATE_PLAYER_EIGHT = 12;

    //layout coordinates for the parent panel
    public final static float WIDTH_R_PARENT    = 1f;
    public final static float HEIGHT_R_PARENT   = 1f;
    public final static float X_R_PARENT_LEFT   = 0.5f;
    public final static float Y_R_PARENT_BOTTOM = 0.5f;

    //layout coordinates for the hand card 1 and 2
    public final static float X_R_PANEL_LEFT_BIG_1     = 0.46f;
    public final static float Y_R_PANEL_BOTTOM_BIG_1   = 0.45f;
    public final static float X_R_PANEL_LEFT_SMALL_1   = 0.87f;
    public final static float Y_R_PANEL_BOTTOM_SMALL_1 = 0.28f;
    public final static float X_R_PANEL_LEFT_BIG_2     = 0.78f;
    public final static float Y_R_PANEL_BOTTOM_BIG_2   = 0.45f;
    public final static float X_R_PANEL_LEFT_SMALL_2   = 0.92f;
    public final static float Y_R_PANEL_BOTTOM_SMALL_2 = 0.28f;

    //dimensions for for big and small hand cards
    public final static float WIDTH_R_PANEL_LEFT_BIG    = 0.3f;
    public final static float HEIGHT_R_PANEL_BOTTOM_BIG = 0.2432f;
    public final static float WIDTH_R_PANEL_LEFT_SMALL    = 0.12f;
    public final static float HEIGHT_R_PANEL_BOTTOM_SMALL = 0.1152f;

    //coordinates for player card 1 and 2
    public final static float X_PLAYERCARD_1_BIG_R_PANEL  = 0.46f;
    public final static float Y_PLAYERCARD_2_BIG_R_PANEL  = 0.45f;
    public final static float X_PLAYERCARD_1_SMALL_R_PANEL = 0.2f;
    public final static float X_PLAYERCARD_2_SMALL_R_PANEL = 0.25f;

    //dimensions for player card 1 and two small
    public final static float WIDTH_PLAYER_CARD_SMALL_R_PANEL_LEFT    = 0.0769f;
    public final static float HEIGHT_PLAYER_CARD_SMALL_R_PANEL_BOTTOM = 0.0738f;

    //layout coordinates for the deck pile
    public final static float X_R_PANEL_LEFT_DECK   = 0.5f;
    public final static float Y_R_PANEL_BOTTOM_DECK = 0.88f;
    public final static float WIDTH_DECK_R_PANEL    = 0.05f;
    public final static float HEIGHT_DECK_R_PANEL   = 0.05f;

    //layout coordinates for burn
    public final static float X_BURN_1_R_PANEL_LEFT      = 0.8f;
    public final static float X_BURN_2_R_PANEL_LEFT      = 0.84f;
    public final static float X_BURN_3_R_PANEL_LEFT      = 0.88f;
    public final static float Y_BURN_R_PANEL_BOTTOM_DECK = 0.797f;

    //Coordinate of Players
    public final static float X_PLAYER_R_PANEL_LEFT = 0.1f;
    public final static float Y_PLAYER_8_R_PANEL_BOTTOM = 0.20f;
    public final static float Y_PLAYER_7_R_PANEL_BOTTOM = 0.29f;
    public final static float Y_PLAYER_6_R_PANEL_BOTTOM = 0.37f;
    public final static float Y_PLAYER_5_R_PANEL_BOTTOM = 0.46f;
    public final static float Y_PLAYER_4_R_PANEL_BOTTOM = 0.55f;
    public final static float Y_PLAYER_3_R_PANEL_BOTTOM = 0.65f;
    public final static float Y_PLAYER_2_R_PANEL_BOTTOM = 0.73f;
    public final static float Y_PLAYER_1_R_PANEL_BOTTOM = 0.81f;
}
