package com.bondfire.texasholdem.data;

/**
 * Created by alvaregd on 03/08/15.
 */
public final class ProfilePanelCoordinates {

    public final static float X_PROFILE_PANEL_R_PARENT = 0.5f;
    public final static float Y_PROFILE_PANEL_R_PARENT = 0.5f;
    public final static float WIDTH_PROFILE_PANEL_R_PARENT = 0.8f;
    public final static float HEIGHT_PROFILE_PANEL_R_PARENT= 0.7f;

    //background coordinates
    public final static float X_PROFILE_BG_R_PANEL = 0.5f;
    public final static float Y_PROFILE_BG_R_PAREL = 0.5f;
    public final static float WIDTH_PROFILE_BG_R_PANEL  = 1f;
    public final static float HEIGHT_PROFILE_BG_R_PANEL = 1f;

    //profile picture coordinates
    public final static float X_PROFILE_PIC_R_PANEL = 0.5f;
    public final static float Y_PROFILE_PIC_R_PANEL = 0.8f;
    public final static float WIDTH_PROFILE_PIC_R_PANEL  = 0.5f;
    public final static float HEIGHT_PROFILE_PIC_R_PANEL = 0.5f;

    //name
    public final static float X_NAME_R_PANEL = 0.5f;
    public final static float Y_NAME_R_PANEL = 0.65f;
    //state
    public final static float X_STATE_R_PANEL = 0.5f;
    public final static float Y_STATE_R_PANEL = 0.58f;

    //bet field coordinates
    public final static float X_BET_LABEL_R_PANEL = 0.18f;
    public final static float Y_BET_LABEL_R_PANEL = 0.15f;
    public final static float X_BET_AMOUNT_R_PANEL = 0.75f;
    public final static float Y_BET_AMOUNT_R_PANEL = 0.15f;
    //wallet field coordinates
    public final static float X_WALLET_LABEL_R_PANEL = 0.18f;
    public final static float Y_WALLET_LABEL_R_PANEL = 0.10f;
    public final static float X_WALLET_AMOUNT_R_PANEL = 0.75f;
    public final static float Y_WALLET_AMOUNT_R_PANEL = 0.10f;

    public final static float X_RANK_R_PANEL = 0.5f;
    public final static float Y_RANK_R_PANEL = 0.22f;


    public final static float X_CARD_ONE_R_PANEL = 0.4f;
    public final static float X_CARD_TWO_R_PANEL = 0.6f;
    public final static float Y_CARD_R_PANEL = 0.35f;
    public final static float WIDTH_CARD_R_PANEL = 0.26f;
    public final static float HEIGHT_CARD_R_PANEL = 0.26f;


}
