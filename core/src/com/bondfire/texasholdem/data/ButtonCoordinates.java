package com.bondfire.texasholdem.data;

/**
 * Created by alvaregd on 02/08/15.
 */
public class ButtonCoordinates {

    //The hand cards
    public final static float WIDTH_HAND_R_PARENT = 0.23f;
    public final static float HEIGHT_HAND_R_PARENT = 0.23f;
    public final static float X_HAND_R_PARENT_LEFT = 1f;
    public final static float Y_HAND_R_PARENT_BUTTOM = 0.34f;

    //player card zoom positions
    public final static float Y_PLAYER_ZOOM_CARDS_8_R_PANEL_BOTTOM = 0.22f;
    public final static float Y_PLAYER_ZOOM_CARDS_7_R_PANEL_BOTTOM = 0.31f;
    public final static float Y_PLAYER_ZOOM_CARDS_6_R_PANEL_BOTTOM = 0.40f;
    public final static float Y_PLAYER_ZOOM_CARDS_5_R_PANEL_BOTTOM = 0.49f;
    public final static float Y_PLAYER_ZOOM_CARDS_4_R_PANEL_BOTTOM = 0.58f;
    public final static float Y_PLAYER_ZOOM_CARDS_3_R_PANEL_BOTTOM = 0.67f;
    public final static float Y_PLAYER_ZOOM_CARDS_2_R_PANEL_BOTTOM = 0.76f;
    public final static float Y_PLAYER_ZOOM_CARDS_1_R_PANEL_BOTTOM = 0.85f;
    public final static float X_PLAYER_ZOOM_CARDS_1_SMALL_R_PANEL = 0.31f;
    public final static float WIDTH_PLAYER_ZOOM_CARDS_R_PANEL_LEFT_SMALL    = 0.16f;
    public final static float HEIGHT_PLAYER_ZOOM_CARDS_R_PANEL_BOTTOM_SMALL = 0.1152f;

    //player poker profile info
    public final static float Y_PLAYER_PROFILE_1_R_PANEL_BOTTOM = 0.85f;
    public final static float Y_PLAYER_PROFILE_2_R_PANEL_BOTTOM =  0.76f;
    public final static float Y_PLAYER_PROFILE_3_R_PANEL_BOTTOM = 0.67f;
    public final static float Y_PLAYER_PROFILE_4_R_PANEL_BOTTOM =  0.58f;
    public final static float Y_PLAYER_PROFILE_5_R_PANEL_BOTTOM = 0.49f;
    public final static float Y_PLAYER_PROFILE_6_R_PANEL_BOTTOM =  0.40f;
    public final static float Y_PLAYER_PROFILE_7_R_PANEL_BOTTOM = 0.31f;
    public final static float Y_PLAYER_PROFILE_8_R_PANEL_BOTTOM = 0.22f;

    public final static float X_PLAYER_PROFILE_SMALL_R_PANEL = 0.145f;
    public final static float WIDTH_PLAYER_PROFILE_R_PANEL_LEFT_SMALL    = 0.14f;
    public final static float HEIGHT_PLAYER_PROFILE_R_PANEL_BOTTOM_SMALL = 0.1152f;


    public final static float X_POT_PANEL_BUTTON_R_PARENT        = 0.60f;
    public final static float Y_POT_PANEL_BUTTON_R_PARENT        = 0.28f;
    public final static float WIDTH_POT_PANEL_BUTTON_R_PARENT    = 0.30f;
    public final static float HEIGHT_PPOT_PANEL_BUTTON_R_PARENT  = 0.12f;

}
