package com.bondfire.texasholdem.events;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Json;
import com.bondfire.app.services.TurnBasedMultiplayerDataPacket;
import com.bondfire.app.services.TurnBasedMultiplayerReceiverListener;
import com.bondfire.texasholdem.data.DataPacket;
import com.bondfire.texasholdem.states.GSM;
import com.bondfire.texasholdem.states.PlayState;
import com.bondfire.texasholdem.states.TitleState;

/**
 * Created by alvaregd on 25/07/15.
 * Handles incoming event calls from the network
 */
public class EventReceiver {

    private final static String Tag = EventReceiver.class.getName();
    private final static boolean d_onTurnBasedMatchReceived = false;

    GSM gsm;
    Json json;

    public DataPacket data;

    public TurnBasedMultiplayerReceiverListener getmReceiverListener() {
        return mReceiverListener;
    }
    public TurnBasedMultiplayerReceiverListener mReceiverListener;

    public EventReceiver(GSM gsm){
        this.gsm = gsm;
        createReceiverListener();
        json = new Json();
    }

    private void createReceiverListener(){
        mReceiverListener = new TurnBasedMultiplayerReceiverListener() {
            @Override
            public void onInvitationReceived() {

            }

            @Override
            public void onInvitationRemoved(String s) {

            }

            @Override
            public void onTurnBasedMatchReceived(String myId, boolean b, TurnBasedMultiplayerDataPacket packet) {
                //Is this a new game?
                if(packet.gameData.equals("NEW")){

                    //New game, set the game data
                    if(d_onTurnBasedMatchReceived)System.out.println("NEW MATCH BOOOOOM");

                    data = new DataPacket();

                    //Get the total number of players
                    data.playerCount = packet.playerCount;

                    //set the round length
                    data.roundLength = 8;
                    data.roundProgress = 1;

                    //pick any player at random ( 1 through Max player ) to be dealer
                    data.dealer = MathUtils.random(data.playerCount - 1 ) + 1;

                    //small blind is the person next to the dealer. If our dealer is Max player,
                    //then small blind is player 1
                    data.smallBlind =  data.dealer != data.playerCount ?  data.dealer + 1:1;
//                    data.currentTurn = 1;
                    data.currentTurn = data.smallBlind;

                    data.smallBlindAmount = 50;
                    data.bigBlindAmount =100;

                    //big blind is person next to the small blind. If our small blind is Max player,
                    //then big blind is player 1
                    data.bigBlind = data.smallBlind != data.playerCount ? data.smallBlind + 1 : 1;

                    //The game starts with everyone giving their blinds
                    data.gameState = DataPacket.STATE_BLINDS;

                    //create table
                    int[] table = new int[5];
                    table[0] = -1;
                    table[1] = -1;
                    table[2] = -1;
                    table[3] = -1;
                    table[4] = -1;
                    data.table = table;

                    //burn pile
                    int[] burn = new int[3];
                    burn[0] = -1;
                    burn[1] = -1;
                    burn[2] = -1;
                    data.burns = burn;

                    //For now everyone has $1000 starting out
                    int[] wallets = new int[data.playerCount];
                    for(int i = 0; i < wallets.length; i++){ wallets[i] = 1000;}
                    wallets[0] = 100;
                    wallets[1] = 200;
                    wallets[2] = 300;
                    wallets[3] = 400;
                    wallets[4] = 500;
                    wallets[5] = 600;
                    wallets[6] = 700;
                    wallets[7] = 800;
                    data.wallets = wallets;

                    //Nobody has placed any bets at the start
                    int[] bets = new int[data.playerCount];
                    for(int bet: bets){bet = 0;}
                    data.bets = bets;

                    //Set the player states
                    int[] states = new int[data.playerCount];
                    for(int state: states){state = 0;}
                    data.playerState = states;

                    //There is no cash on the table
                    data.cashPiles = new int[data.playerCount];

                    //everyone's cards are initially hidden
                    boolean[] isShowing = new boolean[data.playerCount];
                    data.isShowingCards = isShowing;

                    //everyone has rank of 0
                    int[] ranks = new int[data.playerCount];
                    for(int rank: ranks){rank = 0;}
                    data.ranks = ranks;

                    //Nobody gets any cards until they have put their blinds
                    int[][] hands = new int[data.playerCount][2];
                    for(int i = 0; i < hands.length; i ++){
                        hands[i][0] = -1;
                        hands[i][1] = -1;
                    }
                    data.hands = hands;

                    data.workingPotIndex = 0;

                    //everyone participates on the main pot at the start
                    int[][] mainPotParticipant = new int[data.playerCount][data.playerCount];
                    for(int i = 0; i < data.playerCount; i++){
                        mainPotParticipant[0][i] = i+1;

                        if( i != 0){
                            for(int j = 0; j < data.playerCount; j++){
                                mainPotParticipant[i][j] = 0;
                            }
                        }
                    }
                    data.potParticipants = mainPotParticipant;

                    //At the begining, no one is Allin, so set all to -1
                    int[] allIn = new int[data.playerCount];
                    for(int i = 0; i < data.playerCount; i++){
                        allIn[i] = -1;
                    }
                    data.allIn = allIn;



                    if(d_onTurnBasedMatchReceived){
                        System.out.println("data PlayerCount:" + data.playerCount);
                        System.out.println("data currentTurn:" + data.currentTurn);
                        System.out.println("data dealer:"      + data.dealer);
                        System.out.println("data smallBlind:"  + data.smallBlind);
                        System.out.println("data bigBlind:"    + data.bigBlind);
                        System.out.println("data gameState:"   + data.gameState);
                        System.out.println("data PlayerCount:" + data.playerCount);
                    }

                    gsm.pop();
                    gsm.pop();
                    gsm.push(new PlayState(gsm, "p_" + data.currentTurn));

                }else{ //Not a new game
                    data = json.fromJson(DataPacket.class, packet.gameData);
                    if(gsm.peek()  instanceof PlayState){
                        if(d_onTurnBasedMatchReceived) System.out.println("PlayState Already Instantiated");
                    }else{
                        if(d_onTurnBasedMatchReceived) System.out.println("New PlayState");
                        gsm.pop();
                        gsm.pop();
                        gsm.push(new PlayState(gsm, myId));
                    }
                }

                if(data == null){
                    if(d_onTurnBasedMatchReceived) System.out.println("Something went wrong!");
                    gsm.set(new TitleState(gsm));
                }else{
                    if(d_onTurnBasedMatchReceived)System.out.println("DATA OK");
                    ((PlayState)gsm.peek()).updateData("p_"+data.currentTurn,data, packet.playerImages, packet.iconStrings);
//                    ((PlayState)gsm.peek()).updateData(data, packet.playerImages, packet.iconStrings);
                }
            }

            @Override
            public void onTurnBasedMatchRemoved(String s) {

            }
        };
    }
}
