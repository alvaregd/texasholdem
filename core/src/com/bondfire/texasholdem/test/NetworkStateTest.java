package com.bondfire.texasholdem.test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Json;
import com.bondfire.app.bfUtils.BlurrableTextureAtlas;
import com.bondfire.app.services.TurnBasedMultiplayerDataPacket;
import com.bondfire.texasholdem.TexasHoldem;
import com.bondfire.texasholdem.data.DataPacket;
import com.bondfire.texasholdem.states.GSM;
import com.bondfire.texasholdem.states.State;
import com.bondfire.texasholdem.ui.CenteredCentricTextBox;
import com.bondfire.texasholdem.ui.TextBox;
import com.bondfire.texasholdem.utils.ArrayUtils;

/**
 * Created by alvaregd on 25/07/15.
 * Used for testing network/game data activity
 */

public class NetworkStateTest extends State {

    private final static String Tag = NetworkStateTest.class.getName();
    private final static boolean d_networkStateTest = false;

    private BitmapFont bitmapFont;
    private CenteredCentricTextBox invitationReceived;
    private CenteredCentricTextBox invitationRemoved;
    private CenteredCentricTextBox matchReceived;
    private CenteredCentricTextBox matchRemoved;

    private int eventCount = 0;

    public NetworkStateTest(GSM gsm){
        super(gsm);
        System.out.println("LOADED Network Test State");
        bitmapFont = TexasHoldem.res.getBmpFont();
        invitationReceived = new CenteredCentricTextBox(bitmapFont,"InvIn",50,TexasHoldem.HEIGHT - 25);
        invitationRemoved  = new CenteredCentricTextBox(bitmapFont,"InvOut",150,TexasHoldem.HEIGHT- 25);
        matchReceived      = new CenteredCentricTextBox(bitmapFont,"MaIn",250,100 );
        matchRemoved       = new CenteredCentricTextBox(bitmapFont,"MaOut",350,TexasHoldem.HEIGHT- 25);
    }

    @Override
    public void update(float dt) {
        this.handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        if(d_networkStateTest)System.out.println(Tag + " Render Enter");

        TexasHoldem.blurShader.begin();
        TexasHoldem.blurShader.setUniformf("bias", TexasHoldem.MAX_BLUR * TexasHoldem.blurAmount);
        TexasHoldem.blurShader.end();

        sb.setProjectionMatrix(cam.combined);
        sb.begin();

        invitationReceived.render(sb);
        invitationRemoved.render(sb);
        matchReceived.render(sb);
        matchRemoved.render(sb);

        sb.end();
    }

    @Override
    public void handleInput() {
        if (Gdx.input.justTouched()) {
            mouse.x = Gdx.input.getX();
            mouse.y = Gdx.input.getY();
            cam.unproject(mouse);

            if (invitationReceived.contains(mouse.x, mouse.y)) {
                System.out.println("Simulating Invite Received");
            }

            if (invitationRemoved.contains(mouse.x, mouse.y)) {
                System.out.println("Simulating Invite Removed");
            }
            if (matchReceived.contains(mouse.x, mouse.y)) {
                System.out.println("Simulating Match Received");

                TurnBasedMultiplayerDataPacket packet = new TurnBasedMultiplayerDataPacket();
                packet.turnCounter = 1;
                packet.gameId = 2;
                packet.playerCount = 8;
                packet.gameData = getEvent(eventCount);
                packet.iconStrings = new String[]{
                        "http://lh5.googleusercontent.com/-K6KLPCMjjxw/AAAAAAAAAAI/AAAAAAAAABM/MiaIIUvEabg/s96-ns/",
                        "http://lh4.googleusercontent.com/-0wBUhn8Gy7c/AAAAAAAAAAI/AAAAAAAAABM/YeSh_8ILUvw/s96-ns/",
                        "http://lh5.googleusercontent.com/-8gSLDl3l_9A/AAAAAAAAAAI/AAAAAAAAABE/y5ZF_VIsB7M/s96-ns/",
                        "http://lh3.googleusercontent.com/-d1K6i1-f0zo/AAAAAAAAAAI/AAAAAAAAABM/JE32-Z_SuaI/s96-ns/",
                        "http://lh6.googleusercontent.com/-MTCLBITLbKM/AAAAAAAAAAI/AAAAAAAAABM/JBFnDzvk3PQ/s96-ns/",
                        "http://lh6.googleusercontent.com/-e5-VNShoC2o/AAAAAAAAAAI/AAAAAAAAABM/QQjEtDswLqA/s96-ns/",
                        "http://lh6.googleusercontent.com/-PJEPRbeaPe4/AAAAAAAAAAI/AAAAAAAAABE/o3o2RsoNLf8/s96-ns/",
                        "http://lh5.googleusercontent.com/-aMPI59mpfnk/AAAAAAAAAAI/AAAAAAAAABM/SZ-M57xjfIk/s96-ns/"
                };
                packet.playerImages = new String[]{
                        "http://lh5.googleusercontent.com/-K6KLPCMjjxw/AAAAAAAAAAI/AAAAAAAAABM/MiaIIUvEabg/s96-ns-s376/",
                        "http://lh4.googleusercontent.com/-0wBUhn8Gy7c/AAAAAAAAAAI/AAAAAAAAABM/YeSh_8ILUvw/s96-ns-s376/",
                        "http://lh5.googleusercontent.com/-8gSLDl3l_9A/AAAAAAAAAAI/AAAAAAAAABE/y5ZF_VIsB7M/s96-ns-s376/",
                        "http://lh3.googleusercontent.com/-d1K6i1-f0zo/AAAAAAAAAAI/AAAAAAAAABM/JE32-Z_SuaI/s96-ns-s376/",
                        "http://lh6.googleusercontent.com/-MTCLBITLbKM/AAAAAAAAAAI/AAAAAAAAABM/JBFnDzvk3PQ/s96-ns-s376/",
                        "http://lh6.googleusercontent.com/-e5-VNShoC2o/AAAAAAAAAAI/AAAAAAAAABM/QQjEtDswLqA/s96-ns-s376/",
                        "http://lh6.googleusercontent.com/-PJEPRbeaPe4/AAAAAAAAAAI/AAAAAAAAABE/o3o2RsoNLf8/s96-ns-s376/",
                        "http://lh5.googleusercontent.com/-aMPI59mpfnk/AAAAAAAAAAI/AAAAAAAAABM/SZ-M57xjfIk/s96-ns-s376/"
                };
                eventCount ++;
                if(eventCount ==4){
                    eventCount = 0;
                }
                TexasHoldem.events.mReceiverListener.onTurnBasedMatchReceived("p_2",true, packet);
            }
            if (matchRemoved.contains(mouse.x, mouse.y)) {
                System.out.println("Simulating Match Removed");
            }
        }
    }

    public String getEvent(int event){
        switch (event){
            case 0: return
                    "NEW";
            case 1: return
                    "{\"currentTurn\":2," +
                            "\"playerCount\":8," +
                            "\"dealer\":1," +
                            "\"bigBlind\":3," +
                            "\"smallBlind\":2," +
                            "\"gameState\":0," +
                            "\"hands\":[[1,2],[3,4],[5,6],[7,8],[9,10],[11,12],[13,14],[15,16]]," +
                            "\"wallets\":[10,20,30,40,50,6070,80,80,100]," +
                            "\"bets\":[5,4,4,3,3,4,4,4]," +
                            "\"playerState\":[2,3,3,3,3,3,2,1]," +
                            "\"cashPile\":50}";
            case 2: return
                    "{\"currentTurn\":3," +
                            "\"playerCount\":8," +
                            "\"dealer\":1," +
                            "\"bigBlind\":3," +
                            "\"smallBlind\":2," +
                            "\"gameState\":1," +
                            "\"hands\":[[1,2],[3,4],[5,6],[7,8],[9,10],[11,12],[13,14],[15,16]]," +
                            "\"wallets\":[1000,1000,1000,1000,1000,1000,1000,1000,1000]," +
                            "\"bets\":[10,10,10,10,10,10,10,10]," +
                            "\"playerState\":[2,3,3,3,3,3,2,1]," +
                            "\"cashPile\":100000}";
            case 3: return
                    "{\"currentTurn\":2," +
                            "\"playerCount\":8," +
                            "\"dealer\":1," +
                            "\"bigBlind\":3," +
                            "\"smallBlind\":2," +
                            "\"gameState\":2," +
                            "\"hands\":[[1,2],[3,4],[5,6],[7,8],[9,10],[11,12],[13,14],[15,16]]," +
                            "\"wallets\":[1000,1000,1000,1000,1000,1000,1000,1000,1000]," +
                            "\"bets\":[10,10,10,10,10,10,10,10]," +
                            "\"playerState\":[2,3,3,3,3,3,2,1]," +
                            "\"cashPile\":100000}";
            case 4: return
                    "{\"currentTurn\":2," +
                            "\"playerCount\":8," +
                            "\"dealer\":1," +
                            "\"bigBlind\":3," +
                            "\"smallBlind\":2," +
                            "\"gameState\":3," +
                            "\"hands\":[[1,2],[3,4],[5,6],[7,8],[9,10],[11,12],[13,14],[15,16]]," +
                            "\"wallets\":[1000,1000,1000,1000,1000,1000,1000,1000,1000]," +
                            "\"bets\":[10,10,10,10,10,10,10,10]," +
                            "\"playerState\":[2,3,3,3,3,3,2,1]," +
                            "\"cashPile\":100000}";
            case 5: return
                    "{\"currentTurn\":2," +
                            "\"playerCount\":8," +
                            "\"dealer\":1," +
                            "\"bigBlind\":3," +
                            "\"smallBlind\":2," +
                            "\"gameState\":4," +
                            "\"hands\":[[1,2],[3,4],[5,6],[7,8],[9,10],[11,12],[13,14],[15,16]]," +
                            "\"wallets\":[1000,1000,1000,1000,1000,1000,1000,1000,1000]," +
                            "\"bets\":[10,10,10,10,10,10,10,10]," +
                            "\"playerState\":[2,3,3,3,3,3,2,1]," +
                            "\"cashPile\":100000}";
            default: return  "";
        }
    }

    public void takeTurn(DataPacket packet){
//        System.out.println("taking a turn");

        Json json = new Json();
        String data = json.toJson(packet);
//        System.out.println(json.prettyPrint(data));

        //Alternative printing:
        System.out.println("TAKING TURN =========================================");
        System.out.println("Bets: "     + packet.bets[0] + "," + packet.bets[1] + ","+ packet.bets[2] + ","+ packet.bets[3] + ","+ packet.bets[4] + ","+ packet.bets[5] + "," + packet.bets[6] + ","+ packet.bets[7]);
        System.out.println("Wallets: "  + packet.wallets[0] + "," + packet.wallets[1] + ","+ packet.wallets[2] + ","+ packet.wallets[3] + ","+ packet.wallets[4] + ","+ packet.wallets[5] + "," + packet.wallets[6] + ","+ packet.wallets[7]);
        System.out.println("Ceilings: " + packet.ceilings[0] + "," + packet.ceilings[1] + "," + packet.ceilings[2] + "," + packet.ceilings[3] + "," + packet.ceilings[4] + "," + packet.ceilings[5] + "," + packet.ceilings[6] + "," + packet.ceilings[7]);
        System.out.println("Piles: "    + packet.cashPiles[0] + "," + packet.cashPiles[1] + ","+ packet.cashPiles[2] + ","+ packet.cashPiles[3] + ","+ packet.cashPiles[4] + ","+ packet.cashPiles[5] + "," + packet.cashPiles[6] + ","+ packet.cashPiles[7]);
        System.out.println("States: "    + packet.playerState[0] + "," + packet.playerState[1] + ","+ packet.playerState[2] + ","+ packet.playerState[3] + ","+ packet.playerState[4] + ","+ packet.playerState[5] + "," + packet.playerState[6] + ","+ packet.playerState[7]);

        System.out.println("Sum of Bets:" + ArrayUtils.sum(packet.bets));
        System.out.println("Sum of Piles:" + ArrayUtils.sum(packet.cashPiles));


        TurnBasedMultiplayerDataPacket tbmp = new TurnBasedMultiplayerDataPacket();
        tbmp.turnCounter = 1;
        tbmp.gameId = 2;
        tbmp.playerCount = 8;
        tbmp.gameData = data;
        tbmp.iconStrings = new String[]{
                "http://lh5.googleusercontent.com/-K6KLPCMjjxw/AAAAAAAAAAI/AAAAAAAAABM/MiaIIUvEabg/s96-ns/",
                "http://lh4.googleusercontent.com/-0wBUhn8Gy7c/AAAAAAAAAAI/AAAAAAAAABM/YeSh_8ILUvw/s96-ns/",
                "http://lh5.googleusercontent.com/-8gSLDl3l_9A/AAAAAAAAAAI/AAAAAAAAABE/y5ZF_VIsB7M/s96-ns/",
                "http://lh3.googleusercontent.com/-d1K6i1-f0zo/AAAAAAAAAAI/AAAAAAAAABM/JE32-Z_SuaI/s96-ns/",
                "http://lh6.googleusercontent.com/-MTCLBITLbKM/AAAAAAAAAAI/AAAAAAAAABM/JBFnDzvk3PQ/s96-ns/",
                "http://lh6.googleusercontent.com/-e5-VNShoC2o/AAAAAAAAAAI/AAAAAAAAABM/QQjEtDswLqA/s96-ns/",
                "http://lh6.googleusercontent.com/-PJEPRbeaPe4/AAAAAAAAAAI/AAAAAAAAABE/o3o2RsoNLf8/s96-ns/",
                "http://lh5.googleusercontent.com/-aMPI59mpfnk/AAAAAAAAAAI/AAAAAAAAABM/SZ-M57xjfIk/s96-ns/"
        };
        tbmp.playerImages = new String[]{
                "http://lh5.googleusercontent.com/-K6KLPCMjjxw/AAAAAAAAAAI/AAAAAAAAABM/MiaIIUvEabg/s96-ns-s376/",
                "http://lh4.googleusercontent.com/-0wBUhn8Gy7c/AAAAAAAAAAI/AAAAAAAAABM/YeSh_8ILUvw/s96-ns-s376/",
                "http://lh5.googleusercontent.com/-8gSLDl3l_9A/AAAAAAAAAAI/AAAAAAAAABE/y5ZF_VIsB7M/s96-ns-s376/",
                "http://lh3.googleusercontent.com/-d1K6i1-f0zo/AAAAAAAAAAI/AAAAAAAAABM/JE32-Z_SuaI/s96-ns-s376/",
                "http://lh6.googleusercontent.com/-MTCLBITLbKM/AAAAAAAAAAI/AAAAAAAAABM/JBFnDzvk3PQ/s96-ns-s376/",
                "http://lh6.googleusercontent.com/-e5-VNShoC2o/AAAAAAAAAAI/AAAAAAAAABM/QQjEtDswLqA/s96-ns-s376/",
                "http://lh6.googleusercontent.com/-PJEPRbeaPe4/AAAAAAAAAAI/AAAAAAAAABE/o3o2RsoNLf8/s96-ns-s376/",
                "http://lh5.googleusercontent.com/-aMPI59mpfnk/AAAAAAAAAAI/AAAAAAAAABM/SZ-M57xjfIk/s96-ns-s376/"
        };
        TexasHoldem.events.mReceiverListener.onTurnBasedMatchReceived("p_"+packet.currentTurn, true, tbmp);
    }

}

