package com.bondfire.texasholdem.utils;

import java.util.List;

/**
 * Created by alvaregd on 19/08/15.
 */

public class MathBUtils {

    //Array length must be 2 or greater
    public static int max(int[] array){
        try{
            int ret = array[0];
            for(int i = 1; i < array.length; i++){
                if(array[i] > ret) ret= array[i];
            }
            return ret;
        }catch (NullPointerException e){
            return 0;
        }catch (ArrayIndexOutOfBoundsException e){
            return 0;
        }
    }

    public static void InsertionSort(List<Integer> values) {
        for(int i = 1; i <values.size(); i++){
            int x = values.get(i);
            int j = i;
//            System.out.println("Checking: J-1 :" + values.get(j-1) + " and  J " + values.get(j));
            while(j > 0 && ((Integer)values.get(j-1) >x)){
//                System.out.println("SWAPPING");
                int temp = (Integer) values.get(j);
                values.set(j, values.get(j-1));
                j = j - 1;
            }
            values.set(j, x);
        }
    }
}
