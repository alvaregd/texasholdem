package com.bondfire.texasholdem.utils;

/**
 * Created by alvaregd on 06/08/15.
 */
public class CardUtils {

    /** creates a new deck with all cards inside the deck */
    public static FrenchDeck getNewFrenchDeckInstance(){
        return new FrenchDeck();
    }

    /** creates a new deck and removes indicated cards from the deck */
    public static FrenchDeck restoreDeck(int[] cards){
        return new FrenchDeck(cards);
    }

    public static int PickRandom(FrenchDeck deck){
        if(deck.getRemainingCount() == 0)return -1;
        return deck.pickRandomCard();
    }

    public static int getPokerScore(int one, int two, int three, int four, int five){
        return 0;
    }

    public static String getCardString(int card) {
        switch (card){
            case -1: return "";
            case 0: return "a_s";
            case 1: return "a_h";
            case 2: return "a_d";
            case 3: return "a_c";
            case 4: return "k_s";
            case 5: return "k_h";
            case 6: return "k_d";
            case 7: return "k_c";
            case 8: return "q_s";
            case 9: return "q_h";
            case 10: return "q_d";
            case 11: return "q_c";
            case 12: return "j_s";
            case 13: return "j_h";
            case 14: return "j_d";
            case 15: return "j_c";
            case 16: return "10_s";
            case 17: return "10_h";
            case 18: return "10_d";
            case 19: return "10_c";
            case 20: return "9_s";
            case 21: return "9_h";
            case 22: return "9_d";
            case 23: return "9_c";
            case 24: return "8_s";
            case 25: return "8_h";
            case 26: return "8_d";
            case 27: return "8_c";
            case 28: return "7_s";
            case 29: return "7_h";
            case 30: return "7_d";
            case 31: return "7_c";
            case 32: return "6_s";
            case 33: return "6_h";
            case 34: return "6_d";
            case 35: return "6_c";
            case 36: return "5_s";
            case 37: return "5_h";
            case 38: return "5_d";
            case 39: return "5_c";
            case 40: return "4_s";
            case 41: return "4_h";
            case 42: return "4_d";
            case 43: return "4_c";
            case 44: return "3_s";
            case 45: return "3_h";
            case 46: return "3_d";
            case 47: return "3_c";
            case 48: return "2_s";
            case 49: return "2_h";
            case 50: return "2_d";
            case 51: return "2_c";
            case 52: return "card_back_dark";
            default:{
                System.out.println("DEFAULTED!!!!!!!!!!!!!  !!!!!!!! " + card + " DEFAULTED");
                return "card_back_dark";
            }
        }
    }
}
