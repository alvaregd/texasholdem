package com.bondfire.texasholdem.utils;

import com.badlogic.gdx.math.MathUtils;

/**
 * Created by alvaregd on 06/08/15.
 */
public class FrenchDeck {

    private boolean[] cards;

    public FrenchDeck() {
        cards = new boolean[52];
        for(boolean card: cards)card = false; //they are all not picked
    }

    public FrenchDeck(int [] c) {
        try{
            cards = new boolean[52];
            for(int index :c) {
                cards[index] = true;
            }

        }catch (ArrayIndexOutOfBoundsException exp){
            System.out.println("Got a -1... wtf");
        }

    }

    public int pickRandomCard() {
        int pick;
        do{
            pick  = MathUtils.random(51);
        }while(cards[pick]);

        cards[pick] = true;
        return pick;
    }

    public int getRemainingCount() {
        int count = 0;
        for (int i = 0; i < 52; i++) {
            if (!cards[i]) count++;
        }
        return count;
    }

}
