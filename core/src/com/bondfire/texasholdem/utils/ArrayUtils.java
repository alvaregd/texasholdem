package com.bondfire.texasholdem.utils;

/**
 * Created by alvaregd on 20/08/15.
 */
public class ArrayUtils {

    public static int sum(int[] array){
        int sum =0;
        for(int bit: array){
            sum += bit;
        }
        return  sum;
    }
}
