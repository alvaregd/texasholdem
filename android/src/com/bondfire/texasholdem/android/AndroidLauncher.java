package com.bondfire.texasholdem.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.bondfire.app.callbacks.LibgdxLifeCycleCallback;
import com.bondfire.texasholdem.TexasHoldem;

import java.util.Calendar;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

		Calendar cal = Calendar.getInstance();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);

		initialize(new TexasHoldem(hour * 60 * 60 + minute * 60 + second, new LibgdxLifeCycleCallback() {
			@Override
			public void onCreate() {

			}
		}), config);
	}
}
